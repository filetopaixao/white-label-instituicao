import { createGlobalStyle } from 'styled-components'

const GlobalStyles = createGlobalStyle`
  * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
  }

  body {
    font-family: 'Montserrat', sans-serif;
  }

  html, body, #__next {
      height: 100%;
  }

  .form-erro {

    color: red;

    font-size: 12px;
  }

  input {
    width: -webkit-fill-available;
    color: #5e5e5e;
    border: 2px solid #e1e1e1;
    font-size: 14px;
    font-family: sans-serif;
    outline: none;
    background: #ffffff;
    border-radius: 4px;
    position: relative;
    padding: 15px;
    bottom: 0px;
    ::placeholder {
      color: #acacac;
    }
  }

  select {
    width: -webkit-fill-available;
    color: #5e5e5e;
    border: 2px solid #e1e1e1;
    font-size: 14px;
    font-family: sans-serif;
    outline: none;
    background: #ffffff;
    border-radius: 4px;
    position: relative;
    padding: 15px;
    bottom: 0px;
    ::placeholder {
      color: #acacac;
    }
  }

  .table-container {
      margin: 10px;
      padding: 0;
      border: 1px solid #e5e5e5;
      border-radius: 5px;
    }

    tr.expandable > td {
      box-shadow: inset 0 3px 6px -3px rgba(0, 0, 0, .2);
      padding: 0;
    }

    tr.expandable > td > .inner {
      margin: 15px;
      overflow: hidden;
    }
`
export default GlobalStyles
