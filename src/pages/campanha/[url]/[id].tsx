import Head from 'next/head'
import HeartIcon from 'components/atoms/HeartIcon'
import CampaignHeader from '../main/campaignHeader'
import CampaignDescription from '../main/campaignDescription'
import CampaignComments from '../main/campaignComments'

import Sidebar from '../sidebar/paymentForm'
import Donations from '../sidebar/donations'
import Video from '../sidebar/video'

import {
  getActiveSingleCampaign,
  getSingleDonations
} from '../../../services/pages'

import * as S from './styles'

interface Image {
  id: number
  url: string
}

interface Comment {
  id: number
  user: string
  comment: string
}

interface Campaign {
  id: number
  name: string
  user_id: number
  goal_value: string
  amount_collected: string
  ending_date: string
  category_id: number
  image_id: number
  description: string
  is_anonymous: boolean
  status: string
  image: Image
  comments: Comment[]
}

interface Donation {
  id: number
  campaign: string
  user_id: number
  campaign_id: number
  user_name: string
  value: string
  payment_method: string
  payment_status: string
}

interface CampaignsProps {
  campaign: Campaign
  donations: Donation[]
  url: string
}

const Campaign: React.FC<CampaignsProps> = ({ campaign, donations, url }) => {
  return (
    <S.Container>
      <Head>
        <title>{`Tamo Junto - ${campaign.name}`}</title>
        <meta
          property="og:title"
          content={`Tamo Junto - ${campaign.name}`}
          key="title"
        />
        <meta property="og:image" content={campaign.image.url}></meta>
        <meta property="og:description" content={campaign.description} />
      </Head>
      <HeartIcon />
      <S.Title>{campaign.name}</S.Title>
      <div className="single-campaign">
        <div className="campaign-main">
          <CampaignHeader campaign={campaign} />
          <CampaignDescription campaign={campaign} />
          <div className="donations-mobile">
            <Donations campaign_id={campaign.id} />
          </div>
          <CampaignComments comments={campaign.comments} />
        </div>
        <div className="sidebar">
          <Sidebar />
          <div className="donations-desktop">
            <Donations campaign_id={campaign.id} />
          </div>
          <Video />
        </div>
      </div>
    </S.Container>
  )
}

export async function getStaticPaths() {
  // query Strapi to calculate the total page number
  return {
    paths: [{ params: { id: '8', url: 'campanha-teste-3' } }],
    fallback: 'blocking' // See the "fallback" section in docs
  }
}

export const getStaticProps = async ({ params }: any) => {
  const { id } = params
  const campaign = await getActiveSingleCampaign(id)
  const donations = await getSingleDonations(id)
  return {
    props: {
      campaign: campaign?.campaign?.data,
      donations: donations?.donations?.data
    },
    revalidate: 10
  }
}

export default Campaign
