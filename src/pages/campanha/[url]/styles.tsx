import styled from 'styled-components'

interface StyledCampaignsProps {
  image?: string
}

export const Container = styled.div<StyledCampaignsProps>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  padding: 10px 10%;

  .single-campaign {
    display: flex;

    width: 100%;
    margin-bottom: 35px;

    .campaign-main {
      display: flex;
      flex-direction: column;

      width: 70%;
      padding-right: 40px;
      @media (max-width: 768px) {
        width: 100%;
        padding-right: 0px;
      }
      .donations-mobile {
        display: none;
        @media (max-width: 768px) {
          display: block;
        }
      }
    }

    .sidebar {
      width: 30%;

      display: flex;
      flex-direction: column;

      .donations-desktop {
        display: block;
        @media (max-width: 768px) {
          display: none;
        }
      }

      @media (max-width: 768px) {
        width: 100%;
      }
    }
    @media (max-width: 768px) {
      flex-direction: column;
    }
  }
  @media (max-width: 768px) {
    padding-top: 68px;
  }
`

export const Title = styled.h1`
  display: flex;
  justify-content: center;

  width: 100%;
  padding-bottom: 26px;

  color: #5e5e5e;

  font-size: 50px;
  text-transform: capitalize;
  @media (max-width: 768px) {
    text-align: center;
  }
`

const Default = () => false
export default Default
