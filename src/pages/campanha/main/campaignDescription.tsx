import { useContext, useState } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'
import Donations from '../sidebar/donations'

import * as S from './styles'

interface Image {
  id: number
  url: string
}

interface Campaign {
  id: number
  name: string
  user_id: number
  goal_value: string
  amount_collected: string
  ending_date: string
  category_id: number
  image_id: number
  description: string
  is_anonymous: boolean
  status: string
  image: Image
  created_by?: string
  category?: string
}

interface CampaignsProps {
  campaign: Campaign
}

const CampaignDescription: React.FC<CampaignsProps> = ({ campaign }) => {
  const { primaryColor } = useContext(ThemeContext)
  const [isActiveReadMore, setIsActiveReadMore] = useState(false)

  return (
    <>
      <S.ContainerDescription
        primaryColor={primaryColor}
        isActiveReadMore={isActiveReadMore}
      >
        <h2>Descrição da campanha</h2>
        <p className="created-by">
          Criado por <span>{campaign && campaign.created_by}</span>
        </p>
        <p className="category">
          Categoria: <span>{campaign && campaign.category}</span>
        </p>
        <div className="campaign-description">
          <div className="shadow" />
          {campaign && campaign.description}
        </div>
        <a
          className="all-description"
          onClick={() => setIsActiveReadMore(!isActiveReadMore)}
        >
          ler {isActiveReadMore ? 'menos' : 'mais'} <div />
        </a>
      </S.ContainerDescription>
    </>
  )
}

export default CampaignDescription
