import { useContext, useState } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'

import * as S from './styles'

interface Comment {
  id: number
  user: string
  user_image?: string
  comment: string
}

interface CommentsProps {
  comments: Comment[]
}

const CampaignComments: React.FC<CommentsProps> = ({ comments }) => {
  const { primaryColor } = useContext(ThemeContext)
  const [isActiveSeeMore, setIsActiveSeeMore] = useState(false)
  const [showCommentsNumber, setShowCommentsNumber] = useState(4)

  const handkeSeeMoreComments = () => {
    if (isActiveSeeMore && comments) {
      setShowCommentsNumber(comments.length)
    } else {
      setShowCommentsNumber(4)
    }
    setIsActiveSeeMore(!isActiveSeeMore)
  }

  return (
    <S.ContainerComments
      primaryColor={primaryColor}
      isActiveReadMore={isActiveSeeMore}
    >
      <h2>Comentários</h2>

      <div className="comments">
        {comments && comments.length > 4 && <div className="shadow" />}
        {comments &&
          comments
            .filter((comment, idx) => idx < showCommentsNumber)
            .map((comment, key) => (
              <div className="comment" key={key}>
                <div className="avatar">
                  <S.Avatar image={comment.user_image} />
                </div>
                <div className="author-message-comment">
                  <div className="author-comment">
                    <p>{comment.user}</p>
                  </div>
                  <div className="message-comment">
                    <p>{comment.comment}</p>
                  </div>
                </div>
              </div>
            ))}
      </div>
      {comments && comments.length > 4 && (
        <a className="all-comments" onClick={() => handkeSeeMoreComments()}>
          ver {isActiveSeeMore ? 'menos' : 'mais'} <div />
        </a>
      )}
    </S.ContainerComments>
  )
}

export default CampaignComments
