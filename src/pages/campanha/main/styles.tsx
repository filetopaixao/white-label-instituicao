import styled from 'styled-components'

import GoalIcon from '../../../../public/images/goal.svg'
import AmountIcon from '../../../../public/images/amount.svg'
import Facebook from '../../../../public/images/facebook-colored.svg'

import ArrowDown from '../../../../public/images/arrow-down.svg'

interface StyledCampaignsProps {
  image?: string
  primaryColor?: string
  isActiveReadMore?: boolean
}

export const ContainerHeader = styled.div<StyledCampaignsProps>`
  position: relative;
  .campaign-header {
    box-shadow: 0px 3px 10px #00000029;
    border-radius: 5px;
    .campaign-image {
      width: 100%;
      height: 450px;
      border-radius: 5px 5px 0px 0px;

      background-image: ${(props) => `url(${props.image})`};
      background-position: center center;
      background-size: cover;
      background-repeat: no-repeat;
    }

    .campaign-header-info {
      padding: 0 32px;

      .goals {
        display: flex;

        width: 100%;
        margin-bottom: 26px;

        .goal-amount {
          width: 50%;
        }

        .remaining-days {
          display: flex;
          justify-content: flex-end;
          align-items: flex-end;

          width: 50%;

          color: #acacac;

          font-size: 16px;
        }
      }
    }
    .share-campaign {
      display: flex;

      border-top: 1px solid #acacac;
      padding: 20px 32px 30px 32px;

      .input-copy {
        width: 65%;
      }

      .social-share {
        display: flex;
        justify-content: flex-end;
        align-items: center;

        width: 35%;

        a {
          padding-left: 25px;
        }
      }
      @media (max-width: 768px) {
        display: none;
      }
    }
  }

  .share-campaign-mobile {
    position: fixed;
    bottom: 0;
    left: 0;
    display: none;

    width: 100%;
    padding: 10px 10%;

    background-color: #fff;

    z-index: 9999;

    .short-url-social-share {
      display: flex;

      .input-copy {
        width: 50%;
      }

      .social-share {
        display: flex;
        justify-content: flex-end;
        align-items: center;

        width: 50%;

        a {
          padding-left: 25px;
        }
      }
    }

    @media (max-width: 768px) {
      display: flex;
      flex-direction: column;
    }

    .button {
      margin-top: 15px;

      button {
        border-radius: 5px;
      }
    }
  }
`

export const GoalBarContainer = styled.div`
  padding-top: 26px;
  padding-bottom: 45px;
`

export const Goal = styled.div`
  display: flex;
  align-items: center;

  margin-bottom: 20px;
  div {
    width: 25px;
    height: 25px;
    margin-right: 20px;

    mask-image: url(${GoalIcon});
    mask-size: 25px;
    -webkit-mask-image: url(${GoalIcon});
    mask-repeat: no-repeat;
    background-color: #5e5e5e;
  }
  span {
    color: #5e5e5e;

    font-size: 16px;
    span {
      font-weight: 600;
    }
    @media (max-width: 768px) {
      display: block;
    }
  }
`

export const Amount = styled.div`
  display: flex;
  align-items: center;
  div {
    width: 25px;
    height: 25px;
    margin-right: 20px;

    mask-image: url(${AmountIcon});
    mask-size: 25px;
    -webkit-mask-image: url(${AmountIcon});
    mask-repeat: no-repeat;
    background-color: #5e5e5e;
  }
  span {
    color: #5e5e5e;

    font-size: 16px;
    span {
      font-weight: 600;
    }
    @media (max-width: 768px) {
      display: block;
    }
  }
  @media (max-width: 768px) {
    margin-bottom: 30pt;
  }
`

export const ContainerDescription = styled.div<StyledCampaignsProps>`
  padding-top: 41px;

  h2 {
    padding-bottom: 20px;

    color: #5e5e5e;

    font-size: 24px;
  }

  .created-by {
    padding-bottom: 12px;

    color: #5e5e5e;

    font-size: 16px;

    span {
      font-weight: bold;
    }
  }

  .category {
    padding-bottom: 12px;

    color: #5e5e5e;

    font-size: 16px;

    span {
      font-weight: bold;
    }
  }

  .campaign-description {
    position: relative;

    -webkit-transition: max-height 1s;
    -moz-transition: max-height 1s;
    -ms-transition: max-height 1s;
    -o-transition: max-height 1s;
    transition: max-height 1s;

    max-height: ${(props) => (props.isActiveReadMore ? '100%' : '232px')};

    color: #5e5e5e;

    font-size: 16px;

    overflow: hidden;
    transition: max-height 0.3s;
    .shadow {
      position: absolute;
      bottom: 0;
      display: ${(props) => (props.isActiveReadMore ? 'none' : 'block')};

      width: 100%;
      height: 100%;

      -webkit-box-shadow: inset 0px -38px 36px -14px rgba(255, 255, 255, 1);
      -moz-box-shadow: inset 0px -38px 36px -14px rgba(255, 255, 255, 1);
      box-shadow: inset 0px -38px 36px -14px rgba(255, 255, 255, 1);
    }
  }

  .all-description {
    display: flex;
    align-items: center;

    padding-top: 10px;
    padding-bottom: 41px;

    color: ${(props) => props.primaryColor};

    font-size: 17px;
    font-weight: 600;
    text-decoration: none;
    text-transform: uppercase;

    cursor: pointer;

    div {
      width: 11px;
      height: 11px;
      margin-left: 6px;

      mask-image: url(${ArrowDown});
      mask-size: 11px;
      -webkit-mask-image: url(${ArrowDown});
      mask-repeat: no-repeat;
      background-color: ${(props) => props.primaryColor};

      transform: ${(props) =>
        props.isActiveReadMore ? 'rotate(180deg)' : 'rotate(0deg)'};
    }
  }
`

export const ContainerComments = styled.div<StyledCampaignsProps>`
  padding-top: 41px;

  h2 {
    padding-bottom: 20px;

    color: #5e5e5e;

    font-size: 24px;
  }

  .comments {
    position: relative;

    -webkit-transition: max-height 1s;
    -moz-transition: max-height 1s;
    -ms-transition: max-height 1s;
    -o-transition: max-height 1s;
    transition: max-height 1s;

    max-height: ${(props) => (props.isActiveReadMore ? '100%' : '232px')};
    padding-bottom: 30px;

    color: #5e5e5e;

    font-size: 16px;

    overflow: hidden;
    transition: max-height 0.3s;
    .shadow {
      position: absolute;
      bottom: 0;
      display: ${(props) => (props.isActiveReadMore ? 'none' : 'block')};

      width: 100%;
      height: 100%;

      -webkit-box-shadow: inset 0px -38px 36px -14px rgba(255, 255, 255, 1);
      -moz-box-shadow: inset 0px -38px 36px -14px rgba(255, 255, 255, 1);
      box-shadow: inset 0px -38px 36px -14px rgba(255, 255, 255, 1);
    }

    .comment {
      display: flex;

      padding-bottom: 30px;
    }

    .author-message-comment {
      display: flex;
      flex-direction: column;

      width: 100%;

      .author-comment {
        padding-bottom: 8px;

        color: #5e5e5e;

        font-size: 16px;
        font-weight: 600;
      }

      .message-comment {
        width: 100%;

        padding: 15px;

        border: 1px solid #acacac;
        border-radius: 5px;
      }
    }
  }

  .all-comments {
    display: flex;
    align-items: center;

    padding-bottom: 41px;

    color: ${(props) => props.primaryColor};

    font-size: 17px;
    font-weight: 600;
    text-decoration: none;
    text-transform: uppercase;

    cursor: pointer;

    div {
      width: 11px;
      height: 11px;
      margin-left: 6px;

      mask-image: url(${ArrowDown});
      mask-size: 11px;
      -webkit-mask-image: url(${ArrowDown});
      mask-repeat: no-repeat;
      background-color: ${(props) => props.primaryColor};

      transform: ${(props) =>
        props.isActiveReadMore ? 'rotate(180deg)' : 'rotate(0deg)'};
    }
  }
`

export const Avatar = styled.div<StyledCampaignsProps>`
  width: 80px;
  height: 80px;

  border-radius: 5px;
  margin-right: 25px;

  background: ${(props) => `url(${props.image});`}
  background-position: center center;
  background-size: cover;
  background-repeat: no-repeat;
`

const Default = () => false
export default Default
