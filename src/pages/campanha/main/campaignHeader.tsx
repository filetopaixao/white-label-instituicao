import shortUrl from '../../../utils/shortUrl'
import domainUrl from '../../../utils/domainUrl'
import { useContext, useState } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'

import Image from 'next/image'
import Button from 'components/atoms/Button'
import GoalBar from 'components/atoms/GoalBar'
import InputCopy from 'components/atoms/InputCopy'

import * as S from './styles'

interface Image {
  id: number
  url: string
}

interface Campaign {
  id: number
  name: string
  user_id: number
  goal_value: string
  amount_collected: string
  ending_date: string
  category_id: number
  image_id: number
  description: string
  is_anonymous: boolean
  status: string
  image: Image
}

interface CampaignsProps {
  campaign: Campaign
}

const CampaignHeader: React.FC<CampaignsProps> = ({ campaign }) => {
  const { secondaryColor } = useContext(ThemeContext)

  const remainingDays = Math.round(
    (new Date(campaign && campaign.ending_date).getTime() -
      new Date().getTime()) /
      (1000 * 3600 * 24)
  )

  return (
    <S.ContainerHeader image={campaign && campaign.image.url}>
      <div className="campaign-header">
        <div className="campaign-image" />
        <div className="campaign-header-info">
          <S.GoalBarContainer>
            <GoalBar
              gloalValue={parseFloat(campaign && campaign.goal_value)}
              amountCollected={parseFloat(
                campaign && campaign.amount_collected
              )}
            />
          </S.GoalBarContainer>
          <div className="goals">
            <div className="goal-amount">
              <S.Goal>
                <div />
                <span>
                  Meta:{' '}
                  <span>
                    {parseFloat(campaign && campaign.goal_value).toLocaleString(
                      'pt-br',
                      {
                        style: 'currency',
                        currency: 'BRL'
                      }
                    )}
                  </span>
                </span>
              </S.Goal>
              <S.Amount>
                <div />
                <span>
                  Arrecadado:{' '}
                  <span>
                    {parseFloat(
                      campaign && campaign.amount_collected
                    ).toLocaleString('pt-br', {
                      style: 'currency',
                      currency: 'BRL'
                    })}
                  </span>
                </span>
              </S.Amount>
            </div>
            <div className="remaining-days">
              Restam {remainingDays} dias para doar
            </div>
          </div>
        </div>
        <div className="share-campaign">
          <div className="input-copy">
            <InputCopy
              value={`${shortUrl}/${campaign && campaign.id}`}
              id="input-copy"
            />
          </div>
          <div className="social-share">
            <a
              href={`https://www.facebook.com/sharer/sharer.php?u=${domainUrl}/campanha/${
                campaign && campaign.name.replace(/ /g, '-').toLowerCase()
              }/${campaign && campaign.id}`}
              target="_blank"
            >
              <Image
                src="/images/facebook-colored.svg"
                alt="ícone do facebook"
                width={30}
                height={30}
              />
            </a>
            <a
              href={`https://api.whatsapp.com/send?text=${domainUrl}/campanha/${
                campaign && campaign.name.replace(/ /g, '-').toLowerCase()
              }/${campaign && campaign.id}`}
              target="_blank"
            >
              <Image
                src="/images/whatsapp.svg"
                alt="ícone Whatsapp"
                width={30}
                height={30}
              />
            </a>
            <a
              href={`mailto:?subject=${
                campaign && campaign.name
              }&body=Ajude essa campanha ${domainUrl}/campanha/${
                campaign && campaign.name.replace(/ /g, '-').toLowerCase()
              }/${campaign && campaign.id}`}
              target="_blank"
            >
              <Image
                src="/images/mail.svg"
                alt="íconde do email"
                width={30}
                height={30}
              />
            </a>
          </div>
        </div>
      </div>
      <div className="share-campaign-mobile">
        <div className="short-url-social-share">
          <div className="input-copy">
            <InputCopy
              value={`${shortUrl}/${campaign && campaign.id}`}
              id="input-copy-mobile"
            />
          </div>
          <div className="social-share">
            <a
              href={`https://www.facebook.com/sharer/sharer.php?u=${domainUrl}/campanha/${
                campaign && campaign.name.replace(/ /g, '-').toLowerCase()
              }/${campaign && campaign.id}`}
              target="_blank"
            >
              <Image
                src="/images/facebook-colored.svg"
                alt="ícone do facebook"
                width={30}
                height={30}
              />
            </a>
            <a
              href={`https://api.whatsapp.com/send?text=${domainUrl}/campanha/${
                campaign && campaign.name.replace(/ /g, '-').toLowerCase()
              }/${campaign && campaign.id}`}
              target="_blank"
            >
              <Image
                src="/images/whatsapp.svg"
                alt="ícone do Whatsapp"
                width={30}
                height={30}
              />
            </a>
            <a
              href={`mailto:?subject=${
                campaign && campaign.name
              }&body=Ajude essa campanha ${domainUrl}/campanha/${
                campaign && campaign.name.replace(/ /g, '-').toLowerCase()
              }/${campaign && campaign.id}`}
              target="_blank"
            >
              <Image
                src="/images/mail.svg"
                alt="ícone do email"
                width={30}
                height={30}
              />
            </a>
          </div>
        </div>
        <div className="button">
          <Button onClick={() => false} color={secondaryColor}>
            doar
          </Button>
        </div>
      </div>
    </S.ContainerHeader>
  )
}

export default CampaignHeader
