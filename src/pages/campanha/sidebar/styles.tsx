import styled from 'styled-components'

import LockIcon from '../../../../public/images/lock.svg'
import BackgroundVideo from '../../../../public/images/african-man-and-little-girl-fist-bump-close-up.png'

interface StyledSidebarsProps {
  primaryColor?: string
  tertiaryColor?: string
  selected?: boolean
  valueInput?: string
}

export const Container = styled.div<StyledSidebarsProps>`
  .campaign-sidebar {
    width: 100%;
    border-radius: 5px;

    box-shadow: 0px 3px 10px #00000029;

    .sidebar-header {
      display: flex;
      justify-content: center;
      align-items: center;

      padding: 15px;
      border-radius: 5px 5px 0px 0px;

      background-color: ${(props) => props.primaryColor};

      h2 {
        padding-left: 12px;

        color: #fff;

        font-size: 20px;
        font-weight: bold;
      }
    }

    .donation-form {
      padding: 10px 10%;

      .buttons {
        display: grid;
        grid-template-columns: 1fr 1fr 1fr;

        .button {
          padding: 5px;
        }
      }

      .form-inputs {
        .value {
          display: flex;
          align-items: center;

          height: 60px;

          padding: 15px;
          margin-top: 28px;
          margin-bottom: 28px;

          border: ${(props) =>
            props.valueInput
              ? `2px solid ${props.primaryColor};`
              : `2px solid #E1E1E1;`};
          border-radius: 5px;

          color: #5e5e5e;

          input {
            width: 100%;
            padding: 0 20px;

            border: none;
            outline: none;

            color: ${(props) => props.primaryColor};

            font-size: ${(props) => (props.valueInput ? '36px' : '16px')};

            ::placeholder {
              display: flex;
              align-items: center;

              color: #acacac;

              font-size: 16px;
            }
          }
        }

        .inputs {
          input {
            margin-bottom: 16px;

            border: 1px solid #e1e1e1;
          }

          button {
            margin-bottom: 10px;
          }

          .isAnnymous {
            padding-bottom: 20px;
            input {
              margin-right: 10px;
            }
            span {
              color: #5e5e5e;

              font-size: 16px;
            }
          }

          .message {
            padding-bottom: 20px;
          }
        }
      }
    }
    @media (max-width: 768px) {
      display: none;
    }
  }

  .campaign-donations {
    padding: 10px 10%;

    width: 100%;
    border-radius: 5px;

    box-shadow: 0px 3px 10px #00000029;

    .header-donations {
      display: flex;

      padding-bottom: 10px;
      border-bottom: 1px solid #acacac;

      .title-donations {
        width: 70%;
        h2 {
          color: #5e5e5e;

          font-size: 16px;
          font-weight: 600;
        }
      }

      .qtd-donations {
        display: flex;
        flex-direction: column;
        align-items: flex-end;
        justify-content: space-between;

        width: 30%;

        h2 {
          color: #5e5e5e;

          font-size: 16px;
          font-weight: 600;
        }

        p {
          color: #5e5e5e;

          font-size: 12px;
        }
      }
    }

    .users-donations {
      border-bottom: 1px solid #acacac;
      p {
        padding: 10px 0px;

        color: #5e5e5e;

        font-size: 16px;
      }
    }
    .pagination-donations {
      display: flex;

      padding: 10px 0px;

      .prev {
        width: 50%;

        padding-right: 30px;
      }
      .next {
        width: 50%;

        padding-left: 30px;
      }
    }
  }

  .campaign-sidebar-video {
    width: 100%;
    margin-top: 30px;

    border-radius: 5px;
    box-shadow: 0px 3px 10px #00000029;

    .sidebar-header-video {
      display: flex;
      justify-content: center;
      align-items: center;

      width: 100%;
      height: 250px;

      border-radius: 5px 5px 0px 0px;

      background-image: ${(props) => `linear-gradient(#000000b3, #000000b3)`},
        url(${BackgroundVideo});
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;

      cursor: pointer;
      .play {
        width: 0;
        height: 0;

        border-top: 34px solid transparent;
        border-bottom: 34px solid transparent;
        border-left: 53px solid #ffffff52;
      }
    }

    .video-desc {
      padding: 10px 10%;

      color: #5e5e5e;

      font-size: 16px;
    }
  }

  .recaptcha {
    display: flex;
    align-items: center;

    padding-top: 10px;
    padding-bottom: 30px;
    span {
      float: left;

      width: 12px;
      height: 12px;

      margin-right: 5px;

      mask-image: url(${LockIcon});
      mask-size: 12px;
      -webkit-mask-image: url(${LockIcon});
      mask-repeat: no-repeat;
      background-color: #5e5e5e;
    }

    p {
      color: #acacac;

      font-size: 12px;
    }
    @media (max-width: 768px) {
      display: none;
    }
  }
`

export const Button = styled.button<StyledSidebarsProps>`
  width: 100%;
  padding: 15px 10px;

  border: none;
  border-radius: 5px;
  outline: none;

  background-color: ${(props) =>
    props.selected ? props.primaryColor : '#fff'};
  color: ${(props) => (props.selected ? '#fff' : '#5e5e5e')};

  font-size: 14px;

  box-shadow: 0px 1px 6px #00000040;
  cursor: pointer;
`

export const ButtonDonationsPrev = styled.button<StyledSidebarsProps>`
  width: 100%;
  height: 40px;

  border: none;
  outline: none;
  border-radius: 10px;

  color: ${(props) => (props.disabled ? '#d6d6d6' : '#5e5e5e')};
  background: #f2f2f2 0% 0% no-repeat padding-box;

  cursor: ${(props) => (props.disabled ? 'unset' : 'pointer')};
`

export const ButtonDonationsNext = styled.button<StyledSidebarsProps>`
  width: 100%;
  height: 40px;

  border-radius: 10px;
  border: none;
  outline: none;

  color: ${(props) => (props.disabled ? '#d6d6d6' : '#5e5e5e')};
  background: #f2f2f2 0% 0% no-repeat padding-box;

  cursor: ${(props) => (props.disabled ? 'unset' : 'pointer')};
`

const Default = () => false
export default Default
