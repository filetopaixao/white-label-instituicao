import { ReactNode } from 'react'
import { useContext, useState, useEffect } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'
import Image from 'next/image'
import UserForm from 'components/molecules/Form/Payment/User'

import * as S from './styles'

const Sidebar: React.FC = () => {
  const { primaryColor } = useContext(ThemeContext)
  const [form, setForm] = useState<ReactNode>()
  const [header, setHeader] = useState<ReactNode>()

  useEffect(() => {
    setForm(<UserForm setForm={setForm} setHeader={setHeader} />)
    setHeader(
      <>
        <Image src="/images/lock.svg" alt="cadeado" width={20} height={20} />
        <h2>Doação segura</h2>
      </>
    )
  }, [])

  return (
    <S.Container primaryColor={primaryColor}>
      <div className="campaign-sidebar">
        <div className="sidebar-header">{header}</div>
        {form}
      </div>
      <div className="recaptcha">
        <p>
          <span />
          Pagamento seguro · Este site é protegido pelo reCAPTCHA e a Política
          de privacidade e os Termos de serviço do Google se aplicam.
        </p>
      </div>
    </S.Container>
  )
}

export default Sidebar
