import { useEffect, useState } from 'react'

import { getSingleDonations } from '../../../services/pages'

import * as S from './styles'

interface DonationsProps {
  campaign_id: number
}

interface Donation {
  user_name: string
}

const Donations: React.FC<DonationsProps> = ({ campaign_id }) => {
  const [donations, setDonations] = useState([])
  const [donationsPagination, setDonationsPagination] = useState(5)
  const [prevButtonDisabled, setPrevButtonDisabled] = useState(true)
  const [nextButtonDisabled, setNextButtonDisabled] = useState(false)

  useEffect(() => {
    getSingleDonations(campaign_id).then((res) => {
      setDonations(res?.donations?.data?.data)
      if (res?.donations?.data?.data?.length <= 5) {
        setNextButtonDisabled(true)
      }
    })
  }, [donations])

  const handlePrev = () => {
    setDonationsPagination(donationsPagination - 5)
    setNextButtonDisabled(false)

    if (donationsPagination - 5 === 5) {
      setPrevButtonDisabled(true)
    }
  }

  const handleNext = () => {
    setDonationsPagination(donationsPagination + 5)
    setPrevButtonDisabled(false)
    console.log(donationsPagination)
    if (donations.length <= donationsPagination + 5) {
      setNextButtonDisabled(true)
    } else {
      setNextButtonDisabled(false)
    }
  }

  return (
    <S.Container>
      <div className="campaign-donations">
        <div className="header-donations">
          <div className="title-donations">
            <h2>Todas as doações feitas para essa campanha</h2>
          </div>
          <div className="qtd-donations">
            <h2>{donations.length}</h2>
            <p>doações</p>
          </div>
        </div>
        <div className="users-donations">
          {donations
            .filter(
              (donation, idx) =>
                idx >= donationsPagination - 5 && idx < donationsPagination
            )
            .map((donation: Donation, key) => (
              <p key={key}>{donation.user_name}</p>
            ))}
        </div>
        <div className="pagination-donations">
          <div className="prev">
            <S.ButtonDonationsPrev
              disabled={prevButtonDisabled}
              type="button"
              onClick={() => handlePrev()}
            >
              {'< Anterior'}
            </S.ButtonDonationsPrev>
          </div>
          <div className="next">
            <S.ButtonDonationsNext
              type="button"
              disabled={nextButtonDisabled}
              onClick={() => handleNext()}
            >
              {'Próximo >'}
            </S.ButtonDonationsNext>
          </div>
        </div>
      </div>
    </S.Container>
  )
}

export default Donations
