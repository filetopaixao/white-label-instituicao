import { useContext, useState } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'
import Image from 'next/image'
import Input from 'components/atoms/Input'
import Button from 'components/atoms/Button'
import Checkbox from 'components/atoms/Checkbox'
import TextArea from 'components/atoms/TextArea'
import ModalVideo from 'components/atoms/Modal/video'

import * as S from './styles'

const Video: React.FC = () => {
  const { tertiaryColor } = useContext(ThemeContext)
  const [isVisible, setIsVisible] = useState(false)
  return (
    <S.Container tertiaryColor={tertiaryColor}>
      <div className="campaign-sidebar-video">
        <div
          className="sidebar-header-video"
          onClick={() => setIsVisible(!isVisible)}
        >
          <div className="play" />
        </div>
        <div className="video-desc">
          Nossa missão é ajudar a realizar sonhos a augue insu image pellen tes.
          Aliq is notm Aliq is notm hendr erit a augue insu image pellen tes.
          Aliq is notm Aliq is notm hendr erit a augue insu image pellen tes.
          Aliq is notm
        </div>
      </div>
      <ModalVideo
        id="modal-video"
        isVisible={isVisible}
        variant="md"
        onClose={() => setIsVisible(!isVisible)}
        urlVideo="https://www.youtube.com/embed/4xy3iXJJ0So"
      />
    </S.Container>
  )
}

export default Video
