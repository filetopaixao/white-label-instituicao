import React from 'react'
import Cover from 'components/molecules/Cover'
import CampaignsHome from 'components/organisms/CampaignsHome'
import MetricsHome from 'components/atoms/MetricsHome'
import MissionsHome from 'components/molecules/MissionsHome'
import DetailHome from 'components/molecules/DetailHome'
import NewsHome from 'components/organisms/NewsHome'
import DonateHome from 'components/atoms/DonateHome'

import { getHome, getNews, getStatistcs } from '../services/pages'
import { AxiosResponse } from 'axios'

interface Image {
  id: number
  url: string
}

interface Campaign {
  id: number
  name: string
  user_id: number
  goal_value: string
  amount_collected: string
  ending_date: string
  category_id: number
  image_id: number
  description: string
  is_anonymous: boolean
  status: string
  image: Image
}

interface New {
  id: number
  title: string
  description: string
  button_text: string
  button_redirect_url: string
  image: Image
}

interface DataCampaigns {
  data: Campaign[]
}
interface DataNews {
  data: New[]
}

interface HomeProps {
  campaigns: DataCampaigns
  news: DataNews
}

const Home: React.FC<HomeProps> = ({ campaigns, news, statistics }) => {
  return (
    <>
      <Cover />
      <CampaignsHome campaigns={campaigns} />
      <MetricsHome statistics={statistics} />
      <MissionsHome />
      <DetailHome />
      <NewsHome news={news} />
      <DonateHome />
    </>
  )
}

export const getStaticProps = async () => {
  const campaigns = await getHome()
  const statistics = await getStatistcs()
  const news: AxiosResponse<any> | any = await getNews()
  return {
    props: {
      campaigns: campaigns?.campaigns?.data,
      news: news?.data,
      statistics: statistics?.data
    }, // will be passed to the page component as props
    revalidate: 10
  }
}

export default Home
