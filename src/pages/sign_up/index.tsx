import { useEffect } from 'react'
import HeartIcon from 'components/atoms/HeartIcon'
import SignupForm from 'components/molecules/Form/Signup'
import { useRouter } from 'next/router'
import cookie from 'js-cookie'

import * as S from './styles'

const Signup: React.FC | any = () => {

  return (
    <S.Container>
      <HeartIcon />
      <S.Title>Faça seu cadastro</S.Title>

      <S.Subtitle>*Todos seus dados estão seguros</S.Subtitle>

      <S.ContainerForm>
        <SignupForm />
      </S.ContainerForm>
    </S.Container>
  )
}

// export const getServerSideProps = async (ctx: any) => {
//   console.log(ctx.req.cookies.token)
//   if (ctx.req.cookies.token != undefined) {
//     //campaigns = await getMyCampaigns(page, ctx.req.cookies.token)
//     return {
//       redirect: {
//         permanent: false,
//         destination: "/campanhas/1"
//       }
//     }
//   } else {
//     return {
//       props: {
//         token: ctx.req.cookies.token
//       }
//     }
//   }
// }

export default Signup
