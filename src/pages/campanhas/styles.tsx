import styled from 'styled-components'

import Background from '../../../public/images/metrics-background.png'
import BackgroundVideo from '../../../public/images/african-man-and-little-girl-fist-bump-close-up.png'

interface StyledCampaignsProps {
  tertiaryColor: string
}

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  padding: 10px 10%;

  a {
    text-decoration: none;
  }
  @media (max-width: 768px) {
    padding-top: 68px;
  }
`
export const Title = styled.h1`
  display: flex;
  justify-content: center;

  width: 100%;
  padding-bottom: 26px;

  color: #5e5e5e;

  font-size: 50px;
  text-transform: capitalize;
  @media (max-width: 768px) {
    text-align: center;
  }
`
export const Subtitle = styled.h2`
  display: flex;
  justify-content: center;

  padding-bottom: 26px;

  color: #5e5e5e;

  font-size: 16px;
  @media (max-width: 768px) {
    text-align: center;
  }
`
export const CampaignsContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  @media (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`
export const PaginationContainer = styled.div`
  width: 100%;
  margin-bottom: 70px;
`

export const VideoContainer = styled.div<StyledCampaignsProps>`
  display: flex;
  align-items: center;
  flex: 5;

  width: 100%;
  padding: 160px;

  background-image: ${(props) =>
      `linear-gradient(${props.tertiaryColor}b3, ${props.tertiaryColor}b3)`},
    url(${Background});
  background-size: cover;
  background-repeat: no-repeat;

  .column-text {
    width: 50%;
    padding-right: 35px;

    h1 {
      padding-bottom: 25px;

      color: #ffffff;

      font-size: 3rem;
      font-weight: bold;
    }
    h2 {
      color: #ffffff;

      font-size: 20px;
      font-weight: bold;
      p:first-child {
        margin-bottom: 20px;
      }
    }
    @media (max-width: 768px) {
      align-items: center;

      width: 100%;
      padding-right: 0;
      margin-bottom: 60px;
    }
  }
  .column-video {
    width: 50%;
    height: 300px;
    padding-left: 35px;
    a {
      width: 100%;
      height: 100%;

      cursor: pointer;
    }

    .video {
      display: flex;
      justify-content: center;
      align-items: center;

      width: 100%;
      height: 100%;

      background-image: ${(props) => `linear-gradient(#00000099, #00000099)`},
        url(${BackgroundVideo});
      background-size: cover;
      background-repeat: no-repeat;
      .play {
        width: 0;
        height: 0;

        border-top: 34px solid transparent;
        border-bottom: 34px solid transparent;
        border-left: 53px solid #ffffff52;
      }
    }
    @media (max-width: 768px) {
      width: 100%;
      padding-left: 0;
    }
  }

  @media (max-width: 768px) {
    flex-direction: column;

    padding: 80px 35px;
    h1 {
      width: 100%;
    }
  }
`

const Default = () => false
export default Default
