import { useEffect, useContext, useState } from 'react'
import HeartIcon from 'components/atoms/HeartIcon'
import { useRouter } from 'next/router'
import { ThemeContext } from '../../../contexts/ThemeContext'
import shortUrl from '../../../utils/shortUrl'
import domainUrl from '../../../utils/domainUrl'
import InputCopy from 'components/atoms/InputCopy'
import Image from 'next/image'
import Button from 'components/atoms/Button'

import * as S from './styles'

const Signin: React.FC | any = ({ campaign_id, campaign_name }) => {
  const { tertiaryColor, primaryColor, secondaryColor } = useContext(ThemeContext)
  const router = useRouter()
  const [checkBoxEmail, setCheckBoxEmail] = useState(false)
  const [checkBoxWhatsapp, setCheckBoxWhatsapp] = useState(false)
  const [checkBoxSms, setCheckBoxSms] = useState(false)
  const [checkBoxCorrespondencia, setCheckBoxCorrespondencia] = useState(false)


  const handleSearch = () => {
    let arrayStatus: string[] = []
    if (checkBoxEmail) {
      arrayStatus.push('Email')
    }
    if (checkBoxWhatsapp) {
      arrayStatus.push('Whatsapp')
    }
    if (checkBoxSms) {
      arrayStatus.push('Sms')
    }
    if (checkBoxCorrespondencia) {
      arrayStatus.push('Correspondencia')
    }

    console.log(arrayStatus)
  }



  return (
    <S.Container>
      <S.Top primaryColor={primaryColor} tertiaryColor={tertiaryColor}>
        <h1>Obrigado!</h1>
        <h2>Doação ID {campaign_id} aguardando pagamento</h2>
        <span>*Valor não dedutível no imposto de renda</span>
        <p>Enviamos o boleto bancário em seu e-mail. Sua doação será verificada após 2 dias úteis da confirmação de pagamento. Doar é um gesto de amor! Agradecemos a sua confiança no nosso trabalho de transformar o seu amor em tratamento para quem mais precisa.</p>
      </S.Top>
      <S.Bottom>
        <HeartIcon />
        <S.Title>Você é importante para nós!</S.Title>

        <S.Subtitle>
          Como prefere que seja a nossa comunicação com você:
        </S.Subtitle>

        <S.FormContainer secondaryColor={secondaryColor}>
          <div>
            <div className="column">
              <S.CheckBoxContianer>
                <input type="checkbox" data-id="active" onChange={() => setCheckBoxEmail(!checkBoxEmail)} checked={checkBoxEmail} /> Email
            </S.CheckBoxContianer>
              <S.CheckBoxContianer>
                <input type="checkbox" data-id="inactive" onChange={() => setCheckBoxWhatsapp(!checkBoxWhatsapp)} checked={checkBoxWhatsapp} /> WhatsApp
            </S.CheckBoxContianer>
            </div>
            <div className="column">
              <S.CheckBoxContianer>
                <input type="checkbox" data-id="ended" onChange={() => setCheckBoxSms(!checkBoxSms)} checked={checkBoxSms} /> SMS
            </S.CheckBoxContianer>
              <S.CheckBoxContianer>
                <input type="checkbox" data-id="ended" onChange={() => setCheckBoxCorrespondencia(!checkBoxCorrespondencia)} checked={checkBoxCorrespondencia} /> Correspondência
            </S.CheckBoxContianer>
            </div>
          </div>
          <button onClick={() => handleSearch()}>prosseguir</button>
        </S.FormContainer>
      </S.Bottom>
      <S.Quote>
        <div className="share-campaign">
          <div className="input-copy">
            <InputCopy
              value={`${shortUrl}/${campaign_id}`}
              id="input-copy"
            />
          </div>
          <div className="social-share">
            <a
              href={`https://www.facebook.com/sharer/sharer.php?u=${domainUrl}/campanha/${campaign_name.replace(/ /g, '-').toLowerCase()
                }/${campaign_id}`}
              target="_blank"
            >
              <Image
                src="/images/facebook-colored.svg"
                alt="ícone do facebook"
                width={30}
                height={30}
              />
            </a>
            <a
              href={`https://api.whatsapp.com/send?text=${domainUrl}/campanha/${campaign_name.replace(/ /g, '-').toLowerCase()
                }/${campaign_id}`}
              target="_blank"
            >
              <Image
                src="/images/whatsapp.svg"
                alt="ícone Whatsapp"
                width={30}
                height={30}
              />
            </a>
            <a
              href={`mailto:?subject=${campaign_name}&body=Ajude essa campanha ${domainUrl}/campanha/${campaign_name.replace(/ /g, '-').toLowerCase()
                }/${campaign_id}`}
              target="_blank"
            >
              <Image
                src="/images/mail.svg"
                alt="íconde do email"
                width={30}
                height={30}
              />
            </a>
          </div>
        </div>
        <div className="share-campaign-mobile">
          <div className="short-url-social-share">
            <div className="input-copy">
              <InputCopy
                value={`${shortUrl}/${campaign_id}`}
                id="input-copy-mobile"
              />
            </div>
            <div className="social-share">
              <a
                href={`https://www.facebook.com/sharer/sharer.php?u=${domainUrl}/campanha/${campaign_name.replace(/ /g, '-').toLowerCase()
                  }/${campaign_id}`}
                target="_blank"
              >
                <Image
                  src="/images/facebook-colored.svg"
                  alt="ícone do facebook"
                  width={30}
                  height={30}
                />
              </a>
              <a
                href={`https://api.whatsapp.com/send?text=${domainUrl}/campanha/${campaign_name.replace(/ /g, '-').toLowerCase()
                  }/${campaign_id}`}
                target="_blank"
              >
                <Image
                  src="/images/whatsapp.svg"
                  alt="ícone do Whatsapp"
                  width={30}
                  height={30}
                />
              </a>
              <a
                href={`mailto:?subject=${campaign_name
                  }&body=Ajude essa campanha ${domainUrl}/campanha/${campaign_name.replace(/ /g, '-').toLowerCase()
                  }/${campaign_id}`}
                target="_blank"
              >
                <Image
                  src="/images/mail.svg"
                  alt="ícone do email"
                  width={30}
                  height={30}
                />
              </a>
            </div>
          </div>
        </div>
      </S.Quote>
    </S.Container>
  )
}

export const getServerSideProps = async (ctx: any) => {
  const { campaign_id, campaign_name } = ctx.params

  return {
    props: {
      campaign_id,
      campaign_name
    }
  }
}


export default Signin
