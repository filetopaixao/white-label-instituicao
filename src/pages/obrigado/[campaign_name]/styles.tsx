import styled from 'styled-components'

import Background from '../../../../public/images/obrigado_background.png'

interface StyledObrigadoProps {
  primaryColor?: string
  secondaryColor?: string
  tertiaryColor?: string
}

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  @media (max-width: 768px) {
    padding-top: 70px;
  }
`

export const Title = styled.h1`
  display: flex;
  justify-content: center;
  font-size: 50px;
  color: #5e5e5e;
  padding-bottom: 26px;
`
export const Subtitle = styled.h1`
  display: flex;
  justify-content: center;
  font-size: 16px;
  color: #5e5e5e;
  padding-bottom: 26px;
  a {
    color: #5e5e5e;

    cursor: pointer;
  }
`

export const FormContainer = styled.div<StyledObrigadoProps>`
  display: flex;
  flex-direction: column;
  justify-content: center;

  width: 350px;
  margin-bottom: 80px;

  color: #5E5E5E;

  > div{
    display: flex;
    justify-content: center;
  }

  .column{
    display: flex;
    flex-direction: column;
    justify-content: space-between;

    width:100%;
    margin-bottom: 15px;
  }

  button{
    padding: 15px;
    margin-top: 10px;

    background: ${props => props.secondaryColor};
    border-radius: 5px;

    font-size: 16px;
    color:#fff;
    text-transform: uppercase;

    border: 0;
    outline: 0;
  }
  
  @media (max-width: 768px) {
    padding-top: 70px;
  }
`

export const CheckBoxContianer = styled.div`
  display: flex;
  align-items: center;

  input{
    width: auto;
    margin-right: 5px;
  }
`


export const Top = styled.div<StyledObrigadoProps>`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  flex: 5;

  width: 100%;
  padding: 160px;

  background-image: ${(props) =>
    `linear-gradient(90deg, ${props.primaryColor} 17%, ${props.tertiaryColor}63 100%)`},
    url(${Background});
  background-size: cover;
  background-repeat: no-repeat;

  h1 {
    width: 50%;

    margin-bottom: 20px;

    color: #ffffff;

    font-size: 3rem;
    font-weight: bold;
  }
  
  h2 {
    width: 50%;
    margin-bottom: 5px;

    color: #ffffff;

    font-size: 24px;
    font-weight: bold;
  }

  span{
    margin-bottom: 20px;

    color: #ffffff;

    font-size: 12px;
  }

  p{
    width: 50%;

    color: #ffffff;

    font-size: 16px;
  }

  @media (max-width: 768px) {
    height: 100vh;
    padding: 70px;
    h1 {
      width: 100%;
    }
    background-position: center;
  }
`

export const Bottom = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex: 5;

  width: 100%;
  padding: 50px 10%;

  .bottom-container {
    .metric-container:first-child {
      padding-bottom: 30px;
    }
  }

  .metric-container {
    display: flex;
    align-items: center;
  }

  .metrics {
    display: flex;
    flex-direction: column;

    padding-left: 30px;
  }

  .coins-icon {
    width: 80px;
    height: 80px;

    background-color: #5e5e5e;
  }

  .heart-hand-icon {
    width: 80px;
    height: 80px;

    background-color: #5e5e5e;
  }

  .value {
    color: #5e5e5e;

    font-size: 30px;
    font-weight: bold;
  }

  .metric-description {
    color: #5e5e5e;

    font-size: 14px;
  }
  @media (max-width: 768px) {
    height: 50vh;
    width: 100%;
    padding: 100px 36px;
  }
`

export const Quote = styled.div`
  position: absolute;
  top: 600px;
  left: 160px;

  .share-campaign {
    display: flex;

    width: 860px;
    padding: 20px 32px 20px 32px;

    background: #FFFFFF 0% 0% no-repeat padding-box;
    box-shadow: 0px 3px 10px #00000029;
    border-radius: 5px;

    .input-copy {
      width: 65%;
    }

    .social-share {
      display: flex;
      justify-content: flex-end;
      align-items: center;

      width: 35%;

      a {
        padding-left: 25px;
      }
    }
    @media (max-width: 768px) {
      display: none;
    }
  }

  .share-campaign-mobile {
    position: fixed;
    bottom: 0;
    left: 0;
    display: none;

    width: 100%;
    padding: 10px 10%;

    background-color: #fff;

    z-index: 9999;

    .short-url-social-share {
      display: flex;

      .input-copy {
        width: 50%;
      }

      .social-share {
        display: flex;
        justify-content: flex-end;
        align-items: center;

        width: 50%;

        a {
          padding-left: 25px;
        }
      }
    }

    @media (max-width: 768px) {
      display: flex;
      flex-direction: column;
    }

    .button {
      margin-top: 15px;

      button {
        border-radius: 5px;
      }
    }
  }
`
export default () => false
