import { EffectCallback, useEffect, useState, useContext } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'
import HeartIcon from 'components/atoms/HeartIcon'
import LoginForm from 'components/molecules/Form/Login'
import { useRouter } from 'next/router'
import cookie from 'js-cookie'
import EmMemoria from '../../../public/images/categoria-em-memoria.png'
import GratidaoPelaVida from '../../../public/images/categoria-gratidao-pela-vida.png'
import Comemoracao from '../../../public/images/categoria-comemoracao.png'
import EmpresaAmiga from '../../../public/images/categoria-empresa-amiga.png'
import Eventos from '../../../public/images/categoria-eventos.png'
import AcoesDeAmor from '../../../public/images/categoria-acoes-de-amor.png'
import EditCampaignForm from 'components/molecules/Form/EditCampaign'
import { getActiveSingleCampaign } from '../../../services/pages'

import * as S from './styles'
import CampaignDescription from 'pages/campanha/main/campaignDescription'

const Signin: React.FC | any = ({ user, campaignData }) => {
  const router = useRouter()
  const [loggedUser, setLoggedUser] = useState()
  const [countColor, setCountColor] = useState(0)
  const { primaryColor } = useContext(ThemeContext)
  const [categorySelected, setCategorySelected] = useState('')

  useEffect(() => {
    console.log('user', user)
    console.log('campaignData', campaignData)
    setLoggedUser(user)
  }, [])

  return (
    <S.Container>
      <HeartIcon />
      <S.Title>
        Editar campanha
      </S.Title>
      <S.Subtitle>
        * Ao escolher uma foto sua ou alterar a descrição padrão essa campanha passará por uma análise da nossa equipe.
      </S.Subtitle>
      <S.ContainerForm>
        <S.ContainerForm>
          <EditCampaignForm campaignData={campaignData} />
        </S.ContainerForm>
      </S.ContainerForm>
    </S.Container>
  )
}

export const getServerSideProps = async (ctx: any) => {
  const { campaignId } = ctx.params
  const campaign = await getActiveSingleCampaign(campaignId)
  if (ctx.req.cookies.token) {
    return {
      props: {
        user: ctx.req.cookies.loggedUser,
        campaignData: campaign?.campaign?.data
      }
    }
  } else {
    return {
      redirect: {
        permanent: false,
        destination: "/sign_in"
      }
    }
  }
}

export default Signin
