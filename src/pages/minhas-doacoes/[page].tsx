import { useContext, useState, useEffect } from 'react'
import { ThemeContext } from '../../contexts/ThemeContext'
import { useRouter } from 'next/router'
import HeartIcon from 'components/atoms/HeartIcon'
import Datatable from 'components/molecules/Datatable'
import CardCampaign from 'components/molecules/CardCampaign/edit'
import Pagination from 'components/atoms/Pagination'
import cookie from 'js-cookie'
import SearchIcon from '../../../public/images/search.svg'
import Filter from '../../../public/images/filter.svg'
import ModalFilter from 'components/atoms/Modal/Filter'
import Input from 'components/atoms/Input'

import { getMyCampaigns } from '../../services/user'

import * as S from './styles'

interface Image {
  id: number
  url: string
}

interface Campaign {
  id: number
  name: string
  user_id: number
  goal_value: string
  amount_collected: string
  ending_date: string
  category_id: number
  image_id: number
  description: string
  is_anonymous: boolean
  status: string
  image: Image
}

interface DataCampaigns {
  lastPage: number
  pagination: any
  data: Campaign[]
  length?: number
}

interface CampaignsProps {
  campaigns: DataCampaigns
  page: number
  token: string
}

const MyDonations: React.FC<CampaignsProps> = ({ donations, page, token }) => {
  const router = useRouter()
  const { tertiaryColor } = useContext(ThemeContext)
  const [isVisible, setIsVisible] = useState(false)

  const [inputSearch, setInputSearch] = useState('')

  const [checkBoxStatusActive, setCheckBoxStatusActive] = useState(false)
  const [checkBoxStatusInactive, setCheckBoxStatusInactive] = useState(false)
  const [checkBoxStatusEnded, setCheckBoxStatusEnded] = useState(false)

  const [inputCategorySearch, setInputCategorySearch] = useState('')
  const [dateCategorySearch, setDateCategorySearch] = useState('')

  const categories = ["Teste", "Em memória", "Gratidão pela vida", "Comemoração", "Empresa amiga", "Eventos", "Ações de amor"]

  const dataTableColumns = [
    {
      Header: 'id',
      accessor: 'id'
    },
    {
      Header: 'data doação',
      accessor: 'date',
      Cell: value => {
        return <span>{value.split('-').reverse().join('/')}</span>
      }
    },
    {
      Header: 'campanha',
      accessor: 'campaign'
    },
    {
      Header: 'valor doado',
      accessor: 'value',
      Cell: value => <span>
        {parseFloat(value).toLocaleString(
          'pt-br',
          {
            style: 'currency',
            currency: 'BRL'
          }
        )}
      </span>
    },
    {
      Header: 'forma pagamento',
      accessor: 'payment_method'
    },
    {
      Header: 'status',
      accessor: 'status',
      Cell: value => {
        if (value == 'pending') {
          return <S.StatusPending>Aguardando Pagamento</S.StatusPending>
        }

        if (value == 'recused') {
          return <S.StatusRecused>Pagamento Recusado</S.StatusRecused>
        }

        if (value == 'confirmed') {
          return <S.StatusConfirmed>Pagamento Confirmado</S.StatusConfirmed>
        }
      }
    }
  ]

  const dataTableColumnsMobile = [
    {
      Header: 'id',
      accessor: 'id'
    },
    {
      Header: 'valor doado',
      accessor: 'value',
      Cell: value => <span>
        {parseFloat(value).toLocaleString(
          'pt-br',
          {
            style: 'currency',
            currency: 'BRL'
          }
        )}
      </span>
    },
    {
      Header: 'status',
      accessor: 'status',
      Cell: value => {
        if (value == 'pending') {
          return <S.StatusPending>Aguardando Pagamento</S.StatusPending>
        }

        if (value == 'recused') {
          return <S.StatusRecused>Pagamento Recusado</S.StatusRecused>
        }

        if (value == 'confirmed') {
          return <S.StatusConfirmed>Pagamento Confirmado</S.StatusConfirmed>
        }
      }
    }
  ]

  const dataTableCollapseMobile = [
    {
      Header: 'data doação',
      accessor: 'date',
      Cell: value => {
        return <span>{value.split('-').reverse().join('/')}</span>
      }
    },
    {
      Header: 'campanha',
      accessor: 'campaign'
    }
  ]

  const data = [
    {
      "id": "1234",
      "date": "2020-12-01",
      "campaign": 'Homenagem ao Paulo',
      "name": 'Geraldo Gomes',
      "city-state": 'Goianésia/GO',
      "value": 10.00,
      'type': 'unique',
      'payment_method': 'Cartão de Crédito',
      'status': 'pending'
    },
    {
      "id": "1235",
      "date": "2020-05-15",
      "campaign": 'Ana Venceu o Câncer',
      "name": 'Catarina Brito',
      "city-state": 'Palmas/TO',
      "value": 100.00,
      'type': 'unique',
      'payment_method': 'Boleto',
      'status': 'recused'
    },
    {
      "id": "1236",
      "date": "2021-02-01",
      "campaign": 'Aniversário do Joaquim',
      "name": 'Geraldo Gomes',
      "city-state": 'Goianésia/GO',
      "value": 1500.00,
      'type': 'unique',
      'payment_method': 'Paypal',
      'status': 'confirmed'
    },
    {
      "id": "1237",
      "date": "2020-02-01",
      "campaign": 'Doação Mensal',
      "name": 'Anna Paula',
      "city-state": 'São Paulo/SP',
      "value": 10.00,
      'type': 'monthly',
      'payment_method': 'Cartão de Crédito',
      'status': 'confirmed'
    },

  ]

  const handleSearch = () => {
    let arrayStatus: string[] = []
    if (checkBoxStatusActive) {
      arrayStatus.push('active')
    }
    if (checkBoxStatusInactive) {
      arrayStatus.push('inactive')
    }
    if (checkBoxStatusEnded) {
      arrayStatus.push('ended')
    }

    console.log(inputSearch)
    console.log(arrayStatus)
    console.log(inputCategorySearch)
    console.log(dateCategorySearch)
  }

  return (
    <>
      <S.Container>
        <HeartIcon />
        <S.Title>Minhas Doações</S.Title>

        <S.SearchContainer>
          <S.SearchInputContainer>
            <S.Input
              value={inputSearch}
              onChange={(e) => setInputSearch(e.target.value)}
              type="text"
              placeholder="Digite o ID ou título"
            />
            <button onClick={() => handleSearch()}><img src={SearchIcon} /></button>
          </S.SearchInputContainer>
          <button className="btn-filter" onClick={() => setIsVisible(!isVisible)}>Filtros<img src={Filter} /></button>

          {/* <S.Button type="submit" onClick={() => searchChange()}>
            BUSCAR
          </S.Button> */}
        </S.SearchContainer>
        <S.DonationsContainer>
          <Datatable columnsDesktop={dataTableColumns} data={data} columnsMobile={dataTableColumnsMobile} collapseMobile={dataTableCollapseMobile} />
        </S.DonationsContainer>
        {/* <S.PaginationContainer>
          <Pagination
            pages={campaigns.pagination.lastPage}
            currentPage={page.toString()}
            url="minhas-campanhas"
          />
        </S.PaginationContainer> */}
        <ModalFilter
          id="modal-filter"
          title="Filtrar por"
          isVisible={isVisible}
          variant="md"
          onClose={() => setIsVisible(!isVisible)}
          btnCloser
        >
          <S.ModalSubtitle>status</S.ModalSubtitle>
          <S.CheckBoxContianer>
            <input type="checkbox" data-id="active" onChange={() => setCheckBoxStatusActive(!checkBoxStatusActive)} checked={checkBoxStatusActive} /> Ativo
            </S.CheckBoxContianer>
          <S.CheckBoxContianer>
            <input type="checkbox" data-id="inactive" onChange={() => setCheckBoxStatusInactive(!checkBoxStatusInactive)} checked={checkBoxStatusInactive} /> Inativo
            </S.CheckBoxContianer>
          <S.CheckBoxContianer>
            <input type="checkbox" data-id="ended" onChange={() => setCheckBoxStatusEnded(!checkBoxStatusEnded)} checked={checkBoxStatusEnded} /> Encerrado
            </S.CheckBoxContianer>
          <S.ModalSubtitle>categoria</S.ModalSubtitle>
          {/* {categories.map(category => (
            <S.CheckBoxContianer>
              <input type="checkbox" data-id={category} onChange={(e) => handleCheckBox(e, 'category')} /> {category}
            </S.CheckBoxContianer>
          ))} */}
          <select value={inputCategorySearch} onChange={(e) => setInputCategorySearch(e.target.value)}>
            <option value="" label="Selecione" />
            {categories.map(category => (
              <option value={category}>{category}</option>
            ))}
          </select>
          <S.ModalSubtitle>data</S.ModalSubtitle>
          <input type="date" value={dateCategorySearch} onChange={(e) => setDateCategorySearch(e.target.value)} />
        </ModalFilter>
      </S.Container>
    </>
  )
}

// export async function getStaticPaths() {
//   // query Strapi to calculate the total page number
//   return {
//     paths: [
//       {params: { page: '1' } },
//       {params: { page: '2' } },
//       {params: { page: '3' } }
//     ],
//     fallback: 'blocking' // See the "fallback" section in docs
//   }
// }

export const getServerSideProps = async (ctx: any) => {
  const { page } = ctx.params
  let campaigns
  if (ctx.req.cookies.token) {
    //campaigns = await getMyCampaigns(page, ctx.req.cookies.token)
    return {
      props: {
        donations: [],
        page,
        token: ctx.req.cookies.token
      }
    }
  } else {
    return {
      redirect: {
        permanent: false,
        destination: "/sign_in?redirect=/minhas-doacoes/1"
      }
    }
  }
}

export default MyDonations
