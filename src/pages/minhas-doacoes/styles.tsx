import styled from 'styled-components'

interface StyledCampaignsProps {
  tertiaryColor: string
}

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  padding: 10px 5%;

  a {
    text-decoration: none;
  }

  .checkbox {
    display: flex;
    align-items: center;

    padding-top: 50px;

    input {
      width: 15px;

      margin-right: 10px;
    }

    span {
      color: #5e5e5e;
    }

    div {
      width: 15px;
      margin-right: 13px;
    }
  }
  
  @media (max-width: 768px) {
    padding-top: 68px;
  }
`
export const Title = styled.h1`
  display: flex;
  justify-content: center;

  width: 100%;
  padding-bottom: 26px;

  color: #5e5e5e;

  font-size: 50px;
  text-transform: capitalize;
  @media (max-width: 768px) {
    text-align: center;
  }
`
export const Subtitle = styled.h2`
  display: flex;
  justify-content: center;

  padding-bottom: 26px;

  color: #5e5e5e;

  font-size: 16px;
  @media (max-width: 768px) {
    text-align: center;
  }
`
export const DonationsContainer = styled.div`
  width: 100%;
  padding: 10px 10%;

  @media (max-width: 768px) {
  }
`
export const PaginationContainer = styled.div`
  width: 100%;
  margin-bottom: 70px;
`

export const SearchInput = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;

  height: 50px;
  width: 280px;
  margin: 0 20px;
  padding-left: 20px;

  background: #f7f7f7;
  border-radius: 5px;
  border: 1px solid #acacac;
`

export const SearchContainer = styled.div`
  display: flex;
  justify-content: center;

  width:100%;

  .btn-filter{
    display: flex;
    align-items: center;

    padding: 15px;

    border-radius: 5px;
    border: 1px solid #acacac;
    background: #fff;
    color: #5E5E5E;

    cursor: pointer;

    img{
      margin-left: 13px;
      width: 13px;
      height: 13px;
    }
  }
`

export const SearchInputContainer = styled.div`
  display: flex;
  align-items: center;

  margin-right: 30px;

  background: #F7F7F7 0% 0% no-repeat padding-box;
  border: 1px solid #ACACAC;
  border-radius: 5px;

  button{
    width: 20%;
    height: 30px;

    border:0;

    border-left: 2px solid #acacac;

    cursor: pointer;
  }
`

export const Input = styled.input`
  width: 80%;

  border: 0;
  background-color: transparent;

  font-size: 17px;

  ::placeholder {
    color: #acacac;
  }
`
export const Button = styled.button`
  height: 75%;
  width: 15%;
  margin-right: 16px;
  padding-left: 6px;

  background: transparent;
  border: none;
  outline: none;
  border-left: 1px solid #acacac;

  font-size: 17px;

  cursor: pointer;

  img {
    width: 20px;
    height: 20px;
  }
`

export const ModalSubtitle = styled.h2`
  padding-top: 20px;
  padding-bottom: 10px;

  color: #5E5E5E;

  font-size: 16px;
  text-transform: uppercase;
`
export const CheckBoxContianer = styled.div`
  display: flex;
  align-items: center;

  input{
    width: auto;
    margin-right: 16px;
  }
`

export const StatusPending = styled.div`
  padding: 10px;

  border: 1px solid #FFAB00;
  border-radius: 5px;

  color: #FFAB00;
  background-color: #FCEFD5;

  font-size: 12px;
`
export const StatusRecused = styled.div`
  padding: 10px;

  border: 1px solid #FF0000;
  border-radius: 5px;

  color: #FF0000;
  background-color: #FCD4D4;

  font-size: 12px;
`

export const StatusConfirmed = styled.div`
  padding: 10px;

  border: 1px solid #2FDF46;
  border-radius: 5px;

  color: #2FDF46;
  background-color: #D2FFD8;

  font-size: 12px;
`

const Default = () => false
export default Default
