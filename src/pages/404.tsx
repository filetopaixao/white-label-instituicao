import { useEffect, useContext } from 'react'
import { ThemeContext } from '../contexts/ThemeContext'
import HeartIcon from 'components/atoms/HeartIcon'
import { useRouter } from 'next/router'

const Signin: React.FC | any = () => {
  const router = useRouter()
  const { primaryColor, secondaryColor } = useContext(ThemeContext)

  return (
    <div style={{
      width: '100%', height: '100%;', marginBottom: 100, display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'
    }}>
      <HeartIcon />
      <h1 style={{ fontSize: 300, fontWeight: 'bold', textAlign: 'center', color: primaryColor }}>404</h1>
      <h2 style={{ fontSize: 50, fontWeight: 'bold', textAlign: 'center', color: primaryColor }}>Página não encontrada</h2>
      <button onClick={() => router.push('/')} style={{ cursor: 'pointer', marginTop: 65, width: 300, padding: 15, fontSize: 16, backgroundColor: secondaryColor, color: '#fff', border: 0, outline: 0, textTransform: 'uppercase', borderRadius: 5 }}>voltar para home</button>
    </div >
  )
}

export default Signin
