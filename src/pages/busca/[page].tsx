import { useContext, useState } from 'react'
import { ThemeContext } from '../../contexts/ThemeContext'
import HeartIcon from 'components/atoms/HeartIcon'
import CardCampaign from 'components/molecules/CardCampaign'
import Pagination from 'components/atoms/Pagination'
import ModalVideo from 'components/atoms/Modal/video'

import { getActiveCampaigns, getHome } from '../../services/pages'

import * as S from './styles'

interface Image {
  id: number
  url: string
}

interface Campaign {
  id: number
  name: string
  user_id: number
  goal_value: string
  amount_collected: string
  ending_date: string
  category_id: number
  image_id: number
  description: string
  is_anonymous: boolean
  status: string
  image: Image
}

interface DataCampaigns {
  lastPage: number
  data: Campaign[]
}

interface CampaignsProps {
  campaigns: DataCampaigns
  page: number
}

const Signin: React.FC<CampaignsProps> = ({ campaigns, page }) => {
  const { tertiaryColor } = useContext(ThemeContext)
  const [isVisible, setIsVisible] = useState(false)
  return (
    <>
      <S.Container>
        <HeartIcon />
        <S.Title>Campanhas Ativas</S.Title>

        <S.Subtitle>Já somos milhares unidos contra o câncer</S.Subtitle>
        <S.CampaignsContainer>
          {campaigns.data.map((campaign, key) => (
            <CardCampaign
              key={key}
              id={campaign.id}
              image={campaign.image.url}
              name={campaign.name}
              description={campaign.description}
              goal_value={campaign.goal_value}
              amount_collected={campaign.amount_collected}
            />
          ))}
        </S.CampaignsContainer>
        <S.PaginationContainer>
          <Pagination
            pages={campaigns.lastPage}
            currentPage={page.toString()}
            url="campanhas"
          />
        </S.PaginationContainer>
      </S.Container>
      <S.VideoContainer tertiaryColor={tertiaryColor}>
        <div className="column-text">
          <h1>Mais de 50 anos de história...</h1>
          <h2>
            <p>
              ...empenhados para oferecer atendimento altamente qualificado,
              humanizado, gratuito e cheio de amor.
            </p>
            “Vencer o câncer é mais fácil quando se está cercado de amor”
            Assista o vídeo e saiba mais.
          </h2>
        </div>
        <div className="column-video">
          <a onClick={() => setIsVisible(!isVisible)}>
            <div className="video">
              <div className="play" />
            </div>
          </a>
        </div>
      </S.VideoContainer>
      <ModalVideo
        id="modal-video"
        isVisible={isVisible}
        variant="md"
        onClose={() => setIsVisible(!isVisible)}
        urlVideo="https://www.youtube.com/embed/4xy3iXJJ0So"
      />
    </>
  )
}

export async function getStaticPaths() {
  // query Strapi to calculate the total page number
  return {
    paths: [
      { params: { page: '6' } },
      { params: { page: '7' } },
      { params: { page: '8' } }
    ],
    fallback: 'blocking' // See the "fallback" section in docs
  }
}

export const getStaticProps = async (ctx: any) => {
  const { page } = ctx.params
  //const campaigns = await getHome(page)
  const campaigns = await getActiveCampaigns(page)
  console.log('aeeee', campaigns)
  return {
    props: {
      campaigns: campaigns?.campaigns?.data,
      page
    },
    revalidate: 10
  }
}

export default Signin
