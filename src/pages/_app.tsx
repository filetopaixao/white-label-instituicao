import React from 'react'
import Header from 'components/organisms/Header'
import Footer from 'components/molecules/Footer'
import { ThemeProvider } from '../contexts/ThemeContext'
import { PagesProvider } from '../contexts/PagesContext'
import type { AppProps } from 'next/app'
import 'regenerator-runtime/runtime';

import GlobalStyles from 'styles/global'

const MyApp: React.FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <PagesProvider>
      <ThemeProvider>
        <GlobalStyles />
        <Header />
        <Component {...pageProps} />
        <Footer />
      </ThemeProvider>
    </PagesProvider>
  )
}

export default MyApp
