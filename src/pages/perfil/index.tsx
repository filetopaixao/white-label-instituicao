import { useEffect, useState } from 'react'
import HeartIcon from 'components/atoms/HeartIcon'
import Tabs from 'components/molecules/Tabs'
import PersonalData from './PersonalData'
import Contacts from './Contacts'
import Address from './Address'
//import BankData from './BankData'
import { useRouter } from 'next/router'
import cookie from 'js-cookie'

import * as S from './styles'

const Perfil: React.FC | any = () => {
  const [loggedUser, setLoggedUser] = useState()

  useEffect(() => {
    setLoggedUser(JSON.parse(`${cookie.get('loggedUser')}`))
  }, [])

  return (
    <S.Container>
      <HeartIcon />
      <S.Title>minha conta</S.Title>
      {loggedUser && (
        <Tabs
          buttonTxt1="Dados Pessoais"
          content1={<PersonalData loggedUser={loggedUser} />}
          buttonTxt2="contatos"
          content2={<Contacts loggedUser={loggedUser} />}
          buttonTxt3="endereço"
          content3={<Address loggedUser={loggedUser} />}
        // buttonTxt4="contas bancárias"
        // content4={<BankData loggedUser={loggedUser} />}
        />
      )}
    </S.Container>
  )
}

export const getServerSideProps = async (ctx: any) => {
  if (ctx.req.cookies.token) {
    //campaigns = await getMyCampaigns(page, ctx.req.cookies.token)
    return {
      props: {
        token: ctx.req.cookies.token
      }
    }
  } else {
    return {
      redirect: {
        permanent: false,
        destination: "/sign_in?redirect=/perfil"
      }
    }
  }
}

export default Perfil
