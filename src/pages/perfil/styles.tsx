import styled from 'styled-components'

export const Container = styled.div`
  padding: 10px 10% 90px 10%;
  @media (max-width: 768px) {
    padding: 68px 10% 90px 10%;
  }
`

export const Title = styled.h1`
  display: flex;
  justify-content: center;
  font-size: 50px;
  color: #5e5e5e;
  text-transform: capitalize;
  padding-bottom: 26px;
`

export default () => false
