import ContactsForm from 'components/molecules/Form/Profile/Contacts'

import * as S from './styles'

interface ContactsProps {
  loggedUser: any
}

const Contacts: React.FC<ContactsProps> = ({ loggedUser }) => {
  return (
    <S.Container>
      <div className="form">
        <ContactsForm loggedUser={loggedUser} />
      </div>
    </S.Container>
  )
}

export default Contacts
