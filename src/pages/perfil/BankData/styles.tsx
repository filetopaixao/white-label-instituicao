import styled from 'styled-components'

interface StyledProps {
  secondaryColor?: string
  avatar?: string
}

export const Container = styled.div<StyledProps>`
  display: flex;

  width: 100%;

  padding: 30px;
  .form {
    width: 100%;
    .button-add-bank {
      width: 300px;
      margin-bottom: 48px;
      button {
        font-size: 16px;
      }
      @media (max-width: 768px) {
        width: 100%;
      }
    }
  }
  @media (max-width: 768px) {
    flex-direction: column;
  }
`
export const HeaderBanks = styled.div<StyledProps>`
  display: flex;

  width: 100%;
  padding: 10px 20px;

  color: #5e5e5e;

  font-size: 16px;
  font-weight: bold;
  text-transform: uppercase;

  div {
    width: 280px;
  }

  .banco-id {
    width: 180px;
  }
`
export const Alert = styled.div<StyledProps>`
  margin-bottom: 30px;

  color: #f4366a;

  font-size: 12px;
`

export default () => false
