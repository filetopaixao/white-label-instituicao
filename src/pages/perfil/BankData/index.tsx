import { useContext, useState } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'
import CardEdit from 'components/atoms/CardEdit'
import Button from 'components/atoms/Button'
import Modal from 'components/atoms/Modal'
import BankDataForm from 'components/molecules/Form/Profile/BankData'

import * as S from './styles'

interface BankDataProps {
  loggedUser: any
}

const BankData: React.FC<BankDataProps> = ({ loggedUser }) => {
  const { secondaryColor } = useContext(ThemeContext)
  const [isVisible, setIsVisible] = useState(false)

  const bank = [
    {
      id: 1,
      titular: {
        nome: 'João Silva',
        cpf: '123.456.789-99',
        telefone: '(63) 9 9999-0001'
      },
      conta: {
        banco: 'Banco Itaú',
        agencia: '0000-0',
        conta: '00000-0',
        tipo: 'Conta Corrente'
      },
      status: 'approved'
    },
    {
      id: 2,
      titular: {
        nome: 'João Silva',
        cpf: '123.456.789-99',
        telefone: '(63) 9 9999-0001'
      },
      conta: {
        banco: 'Banco Itaú',
        agencia: '0000-0',
        conta: '00000-0',
        tipo: 'Conta Corrente'
      },
      status: 'approved'
    }
  ]

  return (
    <>
      <S.Container>
        <div className="form">
          <div className="button-add-bank">
            <Button
              color={secondaryColor}
              borderRadius={5}
              onClick={() => setIsVisible(!isVisible)}
            >
              adicionar conta bancária
            </Button>
          </div>
          <S.HeaderBanks>
            <div className="banco-id">id</div>
            <div>banco</div>
            <div>titular</div>
          </S.HeaderBanks>
          {bank.map((bank) => (
            <CardEdit bank={bank} />
          ))}
        </div>
      </S.Container>
      <Modal
        id="modal-add-bank"
        isVisible={isVisible}
        variant="md"
        onClose={() => setIsVisible(!isVisible)}
        title="Conta Bancária"
        btnCloser
      >
        <S.Alert>
          <b>ATENÇÃO!</b> Não é permitido cadastrar uma conta salário. Confira
          todas as informações antes de cadastrar a conta. Não nos
          responsabilizamos casos sejam informados dados incorretos.
        </S.Alert>
        <BankDataForm loggedUser={loggedUser} />
      </Modal>
    </>
  )
}

export default BankData
