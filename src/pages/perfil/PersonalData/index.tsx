import { useContext, useState } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'
import PersonDataForm from 'components/molecules/Form/Profile/PersonalData'
import Button from 'components/atoms/Button'
import Modal from 'components/atoms/Modal/ChangePass'
import ModalConfirm from 'components/atoms/Modal/Confirm/confirm'

import * as S from './styles'

interface PersonalDataProps {
  loggedUser: any
}

const PersonalData: React.FC<PersonalDataProps> = ({ loggedUser }) => {
  const { secondaryColor } = useContext(ThemeContext)
  const [editAvatar, setEditAvatar] = useState('')
  const [isVisible, setIsVisible] = useState(false)
  const [isVisibleConfirm, setIsVisibleConfirm] = useState(false)

  const handleAvatar = (e: any) => {
    //setEditAvatar(e.target.files[0])
    const preview = URL.createObjectURL(e.target.files[0])
    setEditAvatar(preview)
    //console.log(preview)
  }

  const handleConfirmDeleteAccount = () => {
    console.log('excluido')
  }

  return (
    <>
      <S.Container
        secondaryColor={secondaryColor}
        avatar={
          editAvatar ? editAvatar : loggedUser && loggedUser.image_profile
        }
      >
        <div className="avatar">
          <div className="image-profile">
            <div className="photo" />
            <label htmlFor="avatar-upload" />
            <input
              type="file"
              id="avatar-upload"
              accept="image/png, image/jpeg"
              onChange={(e: any) => handleAvatar(e)}
            />
          </div>
          <p className="user-firstname">
            {loggedUser && loggedUser.first_name}
          </p>
          <Button
            color="#fff"
            borderRadius={5}
            onClick={() => setIsVisible(!isVisible)}
          >
            alterar senha
          </Button>
          <div className="delete-account">
            <button onClick={() => setIsVisibleConfirm(!isVisibleConfirm)}>
              excluir conta
            </button>
          </div>
        </div>
        <div className="form">
          <PersonDataForm loggedUser={loggedUser} editAvatar={editAvatar} />
        </div>
      </S.Container>
      <Modal
        id="modal-change-pass"
        isVisible={isVisible}
        variant="md"
        onClose={() => setIsVisible(!isVisible)}
        title="Alterar senha"
        btnCloser
      />
      <ModalConfirm
        id="modal-change-pass"
        isVisible={isVisibleConfirm}
        variant="sm"
        onClose={() => setIsVisibleConfirm(!isVisibleConfirm)}
        title="Tem certeza que deseja excluir?"
        message="Ao excluir sua conta todos seus dados e históricos de ações serão apagados, tem certeza que deseja prosseguir?"
        btnCloser={false}
        handleConfirm={() => handleConfirmDeleteAccount()}
      />
    </>
  )
}

export default PersonalData
