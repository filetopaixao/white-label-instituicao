import styled from 'styled-components'
import IconCamera from '../../../../public/images/icon_camera.svg'

interface StyledProps {
  secondaryColor?: string
  avatar?: string
}

export const Container = styled.div<StyledProps>`
  display: flex;

  width: 100%;

  padding: 30px;

  .avatar {
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;

    width: 30%;

    padding: 30px;

    border-radius: 5px;

    background-color: #f2f2f2;

    .image-profile {
      position: relative;

      padding-bottom: 25px;
      .photo {
        width: 200px;
        height: 200px;

        border-radius: 50%;

        background: ${(props) => `url(${props.avatar})`};
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover;
      }

      label {
        position: absolute;
        bottom: 33px;
        right: 4px;

        width: 47px;
        height: 47px;

        border-radius: 50%;

        background: url(${IconCamera}) center center no-repeat #fff;

        cursor: pointer;
      }

      #avatar-upload {
        display: none;
      }
    }

    .user-firstname {
      margin-bottom: 80px;

      color: #404040;

      font-size: 24px;
      font-weight: 600;
    }

    button {
      margin-bottom: 16px;

      border: ${(props) => `1px solid ${props.secondaryColor}`};

      color: ${(props) => props.secondaryColor};

      font-size: 16px;
    }

    .delete-account {
      button {
        border: none;
        outline: none;

        background-color: transparent;

        text-decoration: underline;
        text-transform: capitalize;

        cursor: pointer;
      }
    }

    @media (max-width: 768px) {
      width: 100%;
    }
  }

  .form {
    width: 70%;
    padding-left: 90px;

    @media (max-width: 768px) {
      width: 100%;
      padding-left: 0;
    }
  }
  @media (max-width: 768px) {
    flex-direction: column;
  }
`

export default () => false
