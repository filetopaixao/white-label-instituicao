import AddressForm from 'components/molecules/Form/Profile/Address'

import * as S from './styles'

interface AddressProps {
  loggedUser: any
}

const Address: React.FC<AddressProps> = ({ loggedUser }) => {
  return (
    <S.Container>
      <div className="form">
        <AddressForm loggedUser={loggedUser} />
      </div>
    </S.Container>
  )
}

export default Address
