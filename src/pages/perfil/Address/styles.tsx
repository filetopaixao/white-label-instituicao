import styled from 'styled-components'
import IconCamera from '../../../../public/images/icon_camera.svg'

interface StyledProps {
  secondaryColor?: string
  avatar?: string
}

export const Container = styled.div<StyledProps>`
  display: flex;

  width: 100%;

  padding: 30px;
  .form {
    width: 100%;
  }
  @media (max-width: 768px) {
    flex-direction: column;
  }
`

export default () => false
