import { useEffect } from 'react'
import HeartIcon from 'components/atoms/HeartIcon'
import LoginForm from 'components/molecules/Form/Login'
import { useRouter } from 'next/router'
import cookie from 'js-cookie'

import * as S from './styles'

const Signin: React.FC | any = () => {
  const router = useRouter()

  useEffect(() => {
    const foo = async () => {
      if (cookie.get('loggedUser')) {
        return await router.push('/minhas-campanhas')
      }
    }
    foo()
  }, [])

  return (
    <S.Container>
      <HeartIcon />
      <S.Title>login</S.Title>

      <S.Subtitle>
        <a onClick={() => router.push('/sign_up')}>
          Não tem conta? Cadastre-se
        </a>
      </S.Subtitle>

      <S.ContainerForm>
        <LoginForm />
      </S.ContainerForm>
    </S.Container>
  )
}

export default Signin
