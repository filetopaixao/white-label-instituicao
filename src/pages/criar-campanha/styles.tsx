import styled from 'styled-components'

interface CriarCampanhaProps {
  image?: any
  background?: string
  primaryColor?: string
}

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 10px 10% 90px 10%;
  @media (max-width: 768px) {
    padding: 68px 10% 90px 10%;
  }
`
export const Title = styled.h1`
  display: flex;
  justify-content: center;
  font-size: 50px;
  color: #5e5e5e;
  text-transform: capitalize;
  padding-bottom: 26px;
  text-align: center;
`
export const Subtitle = styled.h1`
  display: flex;
  justify-content: center;
  font-size: 16px;
  color: #5e5e5e;
  padding-bottom: 26px;
`
export const CatgorySelected = styled.span<CriarCampanhaProps>`
  font-size: 16px;
  color: #5e5e5e;
  padding-bottom: 26px;
  font-weight: normal;
  text-align: center;
  button {
    background: none;
    border: none;
    outline: none;
    color: ${(props) => props.primaryColor};
    font-size: 16px;
    text-decoration: underline;
    cursor: pointer;
  }
`
export const ContainerForm = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`
export const Row = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;

  width: 100%;
  .category {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    padding: 20px;

    .card-category {
      width: 100%;

      border-radius: 5px;

      cursor: pointer;
    }
  }
  @media (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`
export const CategoryImage = styled.div<CriarCampanhaProps>`
  height: 120px;

  border-radius: 5px 5px 0 0;

  background: ${(props) => `url(${props.image})`};
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`
export const CategoryDesc = styled.div<CriarCampanhaProps>`
  height: 240px;

  background-color: ${(props) => props.background};
  border-radius: 0 0 5px 5px;

  padding: 32px;

  h1 {
    margin-bottom: 12px;

    color: #fff;

    font-size: 25px;
    font-weight: bold;
  }
  p {
    color: #fff;

    font-size: 16px;
  }
`
export default () => false
