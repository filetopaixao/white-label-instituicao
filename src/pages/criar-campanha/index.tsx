import { EffectCallback, useEffect, useState, useContext } from 'react'
import { ThemeContext } from '../../contexts/ThemeContext'
import HeartIcon from 'components/atoms/HeartIcon'
import LoginForm from 'components/molecules/Form/Login'
import { useRouter } from 'next/router'
import cookie from 'js-cookie'
import EmMemoria from '../../../public/images/categoria-em-memoria.png'
import GratidaoPelaVida from '../../../public/images/categoria-gratidao-pela-vida.png'
import Comemoracao from '../../../public/images/categoria-comemoracao.png'
import EmpresaAmiga from '../../../public/images/categoria-empresa-amiga.png'
import Eventos from '../../../public/images/categoria-eventos.png'
import AcoesDeAmor from '../../../public/images/categoria-acoes-de-amor.png'
import CreateCampaignForm from 'components/molecules/Form/CreateCampaign'

import * as S from './styles'
import CampaignDescription from 'pages/campanha/main/campaignDescription'

const Signin: React.FC | any = ({ user }) => {
  const router = useRouter()
  const [loggedUser, setLoggedUser] = useState()
  const [countColor, setCountColor] = useState(0)
  const { primaryColor } = useContext(ThemeContext)
  const [categorySelected, setCategorySelected] = useState('')
  const [categorySelectedId, setCategorySelectedId] = useState('')

  useEffect(() => {
    console.log('user', user)
    setLoggedUser(user)
  }, [])

  const handleCategory = (category: string, categoryId: number) => {
    console.log(category)
    setCategorySelected(category)
    setCategorySelectedId(categoryId)
  }

  let iColor = 0

  const colors = [
    '#9AACB4',
    '#F46036',
    '#29AF8A',
    '#F4366A',
    '#2971AF',
    '#F8BA00'
  ]

  const categoriesTeste = [
    {
      id: 1,
      name: 'Em memória',
      description: 'Aquela pessoa especial que não está mais entre nós merece ser lembrada, e quão feliz ela ficaria em saber que sua memória ajudou a salvar outras vidas?',
      image: EmMemoria
    },
    {
      id: 2,
      name: 'Gratidão pela vida',
      description: 'Depois de uma vitória contra o câncer ou qualquer situação difícil que você passou, que tal praticar gratidão incentivando doações em prol dos que ainda estão na luta?',
      image: GratidaoPelaVida
    },
    {
      id: 3,
      name: 'Comemoração',
      description: 'Em momentos de celebrações nos damos conta de quão abençoados somos e talvez os presentes são meros detalhes em nossas vidas, mas podem fazer toda a diferença em outras!',
      image: Comemoracao
    },
    {
      id: 4,
      name: 'Empresa Amiga',
      description: 'A liderança conduz os sentimentos das pessoas envolvidas no projeto e a prática da gratidão é muito poderosa. Traga sua empresa para fazer parte dessa missão de salvar vidas com a gente.',
      image: EmpresaAmiga
    },
    {
      id: 5,
      name: 'Eventos',
      description: 'Que tal incentivar doações através do seu evento? Conduzir pessoas é sempre mágico e fazer isso com um propósito se torna ainda mais especial. Crie sua campanha e divulgue seu evento através da nossa plataforma.',
      image: Eventos
    },
    {
      id: 6,
      name: 'Ações de Amor',
      description: 'Aqui você é livre para criar a sua ação de amor! Como você pode contribuir nessa luta contra o câncer? Com uma live? Um show? Um desafio? Um movimento? Ouse e ajude!',
      image: AcoesDeAmor
    }
  ]

  return (
    <S.Container>
      <HeartIcon />
      <S.Title>
        {categorySelected ? 'Detalhes campanha' : 'Escolha o tema da campanha'}
      </S.Title>

      {categorySelected ? (
        <S.CatgorySelected primaryColor={primaryColor}>
          Categoria: <b>{categorySelected}</b>{' '}
          <button onClick={() => setCategorySelected('')}>Alterar</button>
        </S.CatgorySelected>
      ) : (
        <S.Subtitle>
          O melhor remédio contra o câncer é a solidariedade!
        </S.Subtitle>
      )}

      <S.ContainerForm>
        {categorySelected ? (
          <S.ContainerForm>
            <CreateCampaignForm categorySelectedName={categorySelected} categorySelectedId={categorySelectedId} />
          </S.ContainerForm>
        ) : (
          <>
            <S.Row>
              {categoriesTeste.map((category, key) => {
                if (key % 6 == 0) {
                  iColor = key / 6
                }
                return (
                  <div className="category">
                    <div
                      className="card-category"
                      onClick={() => handleCategory(category.name, category.id)}
                    >
                      <S.CategoryImage image={category.image} />
                      <S.CategoryDesc background={colors[key - (6 * iColor)]}>
                        <h1>{category.name}</h1>
                        <p>{category.description}</p>
                      </S.CategoryDesc>
                    </div>
                  </div>
                )
              })}
            </S.Row>
          </>
        )}
      </S.ContainerForm>
    </S.Container>
  )
}

export const getServerSideProps = async (ctx: any) => {
  if (ctx.req.cookies.token) {
    //aqui rodará o código para pegar as categorias cadastradas no banco de dados
    return {
      props: {
        user: ctx.req.cookies.loggedUser
      }
    }
  } else {
    return {
      redirect: {
        permanent: false,
        destination: "/sign_in?redirect=/criar-campanha"
      }
    }
  }
}

export default Signin
