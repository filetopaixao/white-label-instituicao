const banks = [
  { nome: 'BANCO DO BRASIL S.A.', value: '001' },
  { nome: 'BANCO BRADESCO S.A.', value: '237' },
  { nome: 'BANCO ITAÚ S.A.', value: '341' },
  { nome: 'CAIXA ECONOMICA FEDERAL', value: '104' },
  { nome: 'BANCO SANTANDER BANESPA S.A.', value: '033' },
  { nome: 'HSBC BANK BRASIL S.A.', value: '399' },
  { nome: 'BANCO NOSSA CAIXA S.A.', value: '151' },
  { nome: 'BANCO CITIBANK S.A.', value: '745' },
  { nome: 'BANCO DO ESTADO DO RIO GRANDE DO SUL S.A.', value: '041' }
]

export default banks
