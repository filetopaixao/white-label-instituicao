import { ReactNode, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { createContext } from 'react'
import { logout } from '../services/auth'
import { getCampaignCategories } from '../services/pages'

interface Item {
  itemName: string
  itemLink: string
}

interface MenuProps {
  itemName: string
  itemLink: string | Item[]
}

interface MenuContextData {
  menu: MenuProps[]
  menuUser: MenuProps[]
}

interface MenuProviderProps {
  children: ReactNode
}

export const MenuContext = createContext({} as MenuContextData)

export const MenuProvider = ({ children }: MenuProviderProps) => {
  const [menu, setMenu] = useState([] as any)
  const [menuUser, setMenuUser] = useState([] as any)
  const [menuCategories, setMenuCategories] = useState([] as any)
  const router = useRouter()

  useEffect(async () => {
    const categories = await getCampaignCategories()

    let array = categories.data.data.map((categorie, key) => (
      {
        itemName: categorie.name,
        itemLink: `/campanhas-categoria/${key + 1}`
      }
    ))

    //console.log(categories.data.data)

    setMenu([
      {
        itemName: 'Home',
        itemLink: '/'
      },
      {
        itemName: 'Campanhas',
        // itemLink: async () => {
        //   let arrayMenuCategories = [
        //     {
        //       itemName: 'Geral',
        //       itemLink: '/campanhas/1'
        //     }
        //   ]

        //   const campaignsCategories = await getCampaignCategories()
        //   console.log(campaignsCategories)

        // }

        itemLink: [
          {
            itemName: 'Geral',
            itemLink: '/campanhas/1'
          },
          ...array
        ]
      },
      {
        itemName: 'Quem Somos',
        itemLink: '#'
      },
      {
        itemName: 'Cadastre-se',
        itemLink: '/sign_up'
      },
      {
        itemName: 'Login',
        itemLink: '/sign_in'
      }
    ])
    setMenuUser([
      {
        itemName: 'perfil',
        itemLink: [
          {
            itemName: 'Minha Conta',
            itemLink: () => router.push('/perfil')
          },
          {
            itemName: 'Minhas Campanhas',
            itemLink: () => router.push('/minhas-campanhas/1')
          },
          {
            itemName: 'Minhas Doações',
            itemLink: () => false
          },
          {
            itemName: 'Sair',
            itemLink: async () => {
              await logout()
              router.push('/sign_in')
            }
          }
        ]
      }
    ])
  }, [])

  return (
    <MenuContext.Provider value={{ menu, menuUser }}>
      {children}
    </MenuContext.Provider>
  )
}
