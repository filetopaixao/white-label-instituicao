import { ReactNode, useEffect } from 'react'
import { createContext, useState } from 'react'

interface PagesContextData {
  redirectUrl: string
  setRedirectUrl: any
  nameQuery: string
  setNameQuery: any
  lastnameQuery: string
  setLastnameQuery: any
  emailQuery: string
  setEmailQuery: any
}

interface PagesProviderProps {
  children: ReactNode
}

export const PagesContext = createContext({} as PagesContextData)

export const PagesProvider = ({ children }: PagesProviderProps) => {
  const [nameQuery, setNameQuery] = useState('')
  const [lastnameQuery, setLastnameQuery] = useState('')
  const [emailQuery, setEmailQuery] = useState('')
  const [redirectUrl, setRedirectUrl] = useState('')

  return (
    <PagesContext.Provider
      value={{
        redirectUrl,
        setRedirectUrl,
        nameQuery,
        setNameQuery,
        lastnameQuery,
        setLastnameQuery,
        emailQuery,
        setEmailQuery,
      }}
    >
      {children}
    </PagesContext.Provider>
  )
}
