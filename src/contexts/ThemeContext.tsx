import { ReactNode, useEffect } from 'react'
import { createContext, useState } from 'react'
import Icon from '../../public/images/icon.svg'

interface ThemeContextData {
  logo: string
  icon: string
  logoWhite: string
  coverImage: string
  primaryColor: string
  secondaryColor: string
  tertiaryColor: string
}

interface ThemeProviderProps {
  children: ReactNode
}

export const ThemeContext = createContext({} as ThemeContextData)

export const ThemeProvider = ({ children }: ThemeProviderProps) => {
  const [logo, setLogo] = useState('')
  const [icon, setIcon] = useState('')
  const [logoWhite, setLogoWhite] = useState('')
  const [coverImage, setCoverImage] = useState('')
  const [primaryColor, setPrimaryColor] = useState('')
  const [secondaryColor, setSecondaryColor] = useState('')
  const [tertiaryColor, setTertiaryColor] = useState('')

  useEffect(() => {
    setLogo('/images/logo.svg')
    setIcon(Icon)
    setLogoWhite('/images/logo-white.svg')
    setCoverImage('/images/cover.png')
    setPrimaryColor('#29AF8A')
    setSecondaryColor('#F46036')
    setTertiaryColor('#9AACB4')
  }, [])

  return (
    <ThemeContext.Provider
      value={{
        logo,
        icon,
        logoWhite,
        coverImage,
        primaryColor,
        secondaryColor,
        tertiaryColor
      }}
    >
      {children}
    </ThemeContext.Provider>
  )
}
