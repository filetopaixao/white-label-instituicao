import { api } from './api'

export const createSingleDonation = async (credentials) => {
  try {
    const response = await api.post('/api/create-single-donation', credentials)
    return response
  } catch (e) {
    // // window.location.href = "/login"
    return {}
  }
}
