import { api } from './api'
import { getUser } from './user'
import cookie from 'js-cookie'

export async function authentication(credentials) {
  try {
    const { email, password, keepConnected } = credentials
    const response = await api.post('/api/sign-in', { email, password })
    if (keepConnected) {
      cookie.set('token', response.data.token)
    } else {
      cookie.set('token', response.data.token, { expires: 1 })
    }
    const userData = await getUser()
    if (keepConnected) {
      cookie.set('loggedUser', userData.data)
    } else {
      cookie.set('loggedUser', userData.data, { expires: 1 })
    }
    return '/campanhas/1'
  } catch (e) {
    const response =
      e.response.data.meta.message === 'InvalidParams'
        ? 'Senha inválida'
        : 'Email inválido'
    return response
  }
}

export async function socialAuthentication(credentials) {
  try {
    const response = await api.post('/api/sign-in-social', credentials)
    cookie.set('token', response.data.token, { expires: 1 })

    const userData = await getUser()
    cookie.set('loggedUser', userData.data, { expires: 1 })

    return '/campanhas/1'
  } catch (e) {
    return 'Email não cadastrado'
  }
}

export function isAuthenticated({ token }) {
  return token
}

export function logout() {
  cookie.remove('token')
  cookie.remove('loggedUser')
}

export async function signUp(credentials) {
  try {
    const response = await api.post('/api/sign-up', credentials)
    await authentication({
      email: credentials.email,
      password: credentials.password,
      keepConnected: false
    })
    return '/perfil'
  } catch (e) {
    if (e.response.data.details[0].field === 'email') {
      return 'Email já cadastrado'
    } else if (e.response.data.details[0].field === 'natural_person_data.cpf') {
      return 'CPF já cadastrado'
    }
  }
}

// export async function signinFacebook(credentials) {
//   try {
//     const response = await api.post('/api/sign-in/facebook', credentials)
//     if (response.status === 200) {
//       return response
//     }
//     return '/cadastro'
//   } catch (e) {
//     return '/cadastro'
//     // console.log(e);
//   }
// }
