import { api, config } from './api'
import { isAuthenticated } from './auth'
import cookie from 'js-cookie'

export const getUser = async () => {
  try {
    if (!isAuthenticated({ token: cookie.get('token') })) {
      throw 'Not Authenticated'
    }
    const response = await api.get(
      '/api/user-data',
      config(cookie.get('token'))
    )
    cookie.set('loggedUser', response.data)
    return response
  } catch (e) {
    // // window.location.href = "/login"
    return {}
  }
}

export const getMyCampaigns = async (page, token) => {
  try {
    if (!isAuthenticated({ token })) {
      throw 'Not Authenticated'
    }

    const response = await api.get('/api/my-campaigns', {
      headers: {
        Authorization: `Bearer ${token}`
      },
      params: {
        page,
        limit: 2,
        Authorization: `Bearer ${token}`
      }
    })
    //console.log('responsee', response)
    return response
  } catch (e) {
    console.log(e)
    // // window.location.href = "/login"
    return {}
  }
}

export const editCampaign = async (campaignId, body, token) => {
  try {
    if (!isAuthenticated({ token })) {
      throw 'Not Authenticated'
    }

    const response = await api.put(`/api/update-campaigns/${campaignId}`, {
      headers: {
        Authorization: `Bearer ${token}`
      },
      params: {
        Authorization: `Bearer ${token}`
      },
      body
    })
    //console.log('responsee', response)
    return response
  } catch (e) {
    console.log(e)
    // // window.location.href = "/login"
    return {}
  }
}

export const inactiveCampaign = async (campaignId, token) => {
  try {
    if (!isAuthenticated({ token })) {
      throw 'Not Authenticated'
    }

    const response = await api.put(`/api/update-campaigns/${campaignId}`,
      {
        "status": "inactive"
      },
      {
        headers: {
          Authorization: `Bearer ${token}`
        },
        params: {
          Authorization: `Bearer ${token}`
        }
      })
    //console.log('responsee', response)
    return response
  } catch (e) {
    console.log(e)
    // // window.location.href = "/login"
    return {}
  }
}

export const activeCampaign = async (campaignId, token) => {
  try {
    if (!isAuthenticated({ token })) {
      throw 'Not Authenticated'
    }

    const response = await api.put(`/api/update-campaigns/${campaignId}`,
      {
        "status": "active"
      },
      {
        headers: {
          Authorization: `Bearer ${token}`
        },
        params: {
          Authorization: `Bearer ${token}`
        }
      })
    //console.log('responsee', response)
    return response
  } catch (e) {
    console.log(e)
    // // window.location.href = "/login"
    return {}
  }
}

export const deleteCampaign = async (campaignId, token) => {
  try {
    if (!isAuthenticated({ token })) {
      throw 'Not Authenticated'
    }

    const response = await api.put(`/api/update-campaigns/${campaignId}`,
      {
        "status": "delete"
      },
      {
        headers: {
          Authorization: `Bearer ${token}`
        },
        params: {
          Authorization: `Bearer ${token}`
        }
      })
    //console.log('responsee', response)
    return response
  } catch (e) {
    console.log(e)
    // // window.location.href = "/login"
    return {}
  }
}

// export const getMyCampaignsWithoutPage = async (page, token) => {
//   try {
//     if (!isAuthenticated({ token })) {
//       throw 'Not Authenticated'
//     }

//     const response = await api.get('/api/my-campaigns', {
//       headers: {
//         Authorization: `Bearer ${token}`
//       },
//       params: {
//         Authorization: `Bearer ${token}`
//       }
//     })
//     //console.log('responsee', response)
//     return response
//   } catch (e) {
//     console.log(e)
//     // // window.location.href = "/login"
//     return {}
//   }
// }
