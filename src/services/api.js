import axios from 'axios'

export const api = axios.create({
  //baseURL: 'http://40.71.7.44/',
  baseURL: 'http://3.138.185.146/',
  timeout: 0,
  headers: { 'Content-Type': 'application/json' }
})

export const config = (token) => {
  return {
    headers: {
      Authorization: `Bearer ${token}`
    },
    params: {
      Authorization: `Bearer ${token}`
    }
  }
}

export default api
