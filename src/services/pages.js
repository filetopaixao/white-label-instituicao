import { api } from './api'

export async function getHome() {
  try {
    const responseCampaign = await api.get(`/api/home`)
    //const responseNews = await api.get(`/api/news`, config(token))
    return { campaigns: responseCampaign }
  } catch (e) {
    console.error(e)
    //window.location.replace('/404')
    return {}
  }
}

export async function getActiveCampaigns(page) {
  try {
    //alterar depois para /api/campaigns e tirar requerimento de token para essa rota
    const response = await api.get(`/api/campaigns`, {
      params: {
        page,
        limit: 3
      }
    })
    return { campaigns: response }
  } catch (e) {
    console.error(e)
    //window.location.replace('/404')
    return {}
  }
}

export async function getActiveSingleCampaign(id) {
  try {
    const response = await api.get(`/api/campaigns/${id}`)
    return { campaign: response }
  } catch (e) {
    console.error(e)
    //window.location.replace('/404')
    return {}
  }
}

export async function getSingleDonations(id) {
  try {
    const response = await api.get(`/api/single-donations/${id}`)
    return { donations: response }
  } catch (e) {
    console.error(e)
    //window.location.replace('/404')
    return {}
  }
}


export async function getNews() {
  try {
    const response = await api.get(`/api/news`)
    return response
  } catch (e) {
    console.error(e)
    //window.location.replace('/404')
    return {}
  }
}

export async function getCampaignCategories() {
  try {
    const response = await api.get(`/api/categories`)
    return response
  } catch (e) {
    console.error(e)
    //window.location.replace('/404')
    return {}
  }
}

export async function getStatistcs() {
  try {
    const response = await api.get(`/api/get-statistics`)
    return response
  } catch (e) {
    console.error(e)
    //window.location.replace('/404')
    return {}
  }
}

export async function createDonorCampaign(campaign) {
  try {
    const response = await api.post(
      'https://virtserver.swaggerhub.com/arthur-gehrke/conecta-backend-dev/1.0/api/create-campaigns',
      campaign
    )
    return response
  } catch (e) {
    console.error(e)
    window.location.replace('/404')
    return {}
  }
}

export default {
  getHome,
  getActiveCampaigns,
  getCampaignCategories,
  createDonorCampaign,
  getNews,
  getStatistcs
}
