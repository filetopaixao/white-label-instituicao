import styled from 'styled-components'

interface StyledButtonProps {
  backgroundColor: string
  textColor: string
  borderRadius?: number
  type?: string
}

export const Button = styled.button<StyledButtonProps>`
  display: flex;
  -webkit-box-align: center;
  align-items: center;
  -webkit-box-pack: center;
  justify-content: center;
  text-transform: uppercase;
  font-size: 18px;
  font-weight: 0;
  //border-radius: 5px;
  border: none;
  border-radius: ${(props) =>
    props.borderRadius ? `${props.borderRadius}px` : 0};
  outline: none;
  height: 50px;
  width: 100%;
  background: ${(props) => props.backgroundColor};
  color: ${(props) => props.textColor};
  cursor: pointer;
`
