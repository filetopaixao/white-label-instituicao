import { ReactNode } from 'react'
import * as S from './styles'

interface ButtonProps {
  children: ReactNode
  color: string
  type?: any
  textColor?: string
  onClick: () => void
  borderRadius?: number
}

const Button: React.FC<ButtonProps> = (props) => (
  <S.Button
    backgroundColor={props.color}
    textColor={props.textColor ? props.textColor : '#fff'}
    onClick={props.onClick}
    borderRadius={props.borderRadius}
    type={props.type}
  >
    {props.children}
  </S.Button>
)

export default Button
