import { useContext } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'

import * as S from './styles'

const MetricsHome: React.FC = ({ statistics }) => {
  const { secondaryColor, tertiaryColor } = useContext(ThemeContext)
  return (
    <S.Container>
      <S.Top tertiaryColor={tertiaryColor}>
        <h1>O sentido da vida é fazer sentido a outras vidas</h1>
      </S.Top>
      <S.Bottom>
        <div className="bottom-container">
          <div className="metric-container">
            <div className="coins-icon" />
            <div className="metrics">
              <p className="value">{statistics.amountCollected ? parseFloat(statistics.amountCollected).toLocaleString('pt-br', {
                style: 'currency',
                currency: 'BRL'
              }) : 'R$ 0,00'}</p>
              <p className="metric-description">
                Arrecadados nos últimos 30 dias
              </p>
            </div>
          </div>
          <div className="metric-container">
            <div className="heart-hand-icon" />
            <div className="metrics">
              <p className="value">{statistics.createdUsers ? statistics.createdUsers : '0'}</p>
              <p className="metric-description">Novos doadores esse mês</p>
            </div>
          </div>
        </div>
      </S.Bottom>
      <S.Quote secondaryColor={secondaryColor}>
        <div />
        <p>
          Eu sei que o meu trabalho é uma gota no oceano, mas sem ele o oceano
          seria menor.
        </p>
        <p>Madre Teresa de Calcutá</p>
      </S.Quote>
    </S.Container>
  )
}

export default MetricsHome
