import styled from 'styled-components'

import Background from '../../../../public/images/metrics-background.png'
import QuoteImage from '../../../../public/images/quote.svg'
import CoinsIcon from '../../../../public/images/coins.svg'
import HeartHandIcon from '../../../../public/images/heart-hand.svg'

interface StyledMetricsHomeProps {
  tertiaryColor?: string
  secondaryColor?: string
}

export const Container = styled.section`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;

  min-height: 757px;
  @media (max-width: 768px) {
    height: auto;
  }
`

export const Top = styled.div<StyledMetricsHomeProps>`
  display: flex;
  align-items: center;
  flex: 5;

  width: 100%;
  padding: 160px;

  background-image: ${(props) =>
      `linear-gradient(${props.tertiaryColor}cc, ${props.tertiaryColor}cc)`},
    url(${Background});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;

  h1 {
    width: 50%;

    color: #ffffff;

    font-size: 3rem;
    font-weight: bold;
  }

  @media (max-width: 768px) {
    height: 100vh;
    padding: 70px;
    h1 {
      width: 100%;
    }
  }
`

export const Bottom = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex: 5;

  width: 50%;
  padding: 100px;

  .bottom-container {
    .metric-container:first-child {
      padding-bottom: 30px;
    }
  }

  .metric-container {
    display: flex;
    align-items: center;
  }

  .metrics {
    display: flex;
    flex-direction: column;

    padding-left: 30px;
  }

  .coins-icon {
    width: 80px;
    height: 80px;

    mask-image: url(${CoinsIcon});
    mask-size: 80px;
    -webkit-mask-image: url(${CoinsIcon});
    mask-repeat: no-repeat;
    background-color: #5e5e5e;
  }

  .heart-hand-icon {
    width: 80px;
    height: 80px;

    mask-image: url(${HeartHandIcon});
    mask-size: 80px;
    -webkit-mask-image: url(${HeartHandIcon});
    mask-repeat: no-repeat;
    background-color: #5e5e5e;
  }

  .value {
    color: #5e5e5e;

    font-size: 30px;
    font-weight: bold;
  }

  .metric-description {
    color: #5e5e5e;

    font-size: 14px;
  }
  @media (max-width: 768px) {
    height: 50vh;
    width: 100%;
    padding: 100px 36px;
  }
`

export const Quote = styled.div<StyledMetricsHomeProps>`
  position: absolute;
  right: 0;
  bottom: 0;

  width: 50%;
  padding: 70px;
  background-color: ${(props) => props.secondaryColor};
  div {
    width: 60px;
    height: 60px;

    mask-image: url(${QuoteImage});
    mask-size: 60px;
    -webkit-mask-image: url(${QuoteImage});
    mask-repeat: no-repeat;
    background-color: #fff;
    margin-bottom: 30px;
  }

  > p {
    width: 60%;
    margin-bottom: 50px;

    color: #fff;

    font-size: 28px;
    font-weight: bold;
  }
  p:last-child {
    width: 50%;
    margin-bottom: 0px;

    color: #fff;

    font-size: 10px;
    font-weight: 600;
  }

  @media (max-width: 768px) {
    position: relative;
    display: flex;
    flex-direction: column;
    justify-content: center;

    width: 100%;
    height: 100vh;
    > p {
      width: 100%;
    }
  }
`
