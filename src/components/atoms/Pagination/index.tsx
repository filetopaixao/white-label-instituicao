import { useState, useContext } from 'react'
import { useRouter } from 'next/router'
import { ThemeContext } from '../../../contexts/ThemeContext'

import * as S from './styles'

interface PaginationProps {
  pages: number
  currentPage: string
  url: string
}

const Pagination: React.FC<PaginationProps> = ({ pages, currentPage, url }) => {
  const { primaryColor } = useContext(ThemeContext)
  const router = useRouter()
  const [inputPage, setInputPage] = useState(currentPage)
  const rows: any = []
  console.log(pages, currentPage)
  for (let i = 0; i < pages; i++) {
    rows.push(
      <a href={`/${url}/${i + 1}`} key={`page-${i + 1}`}>
        <S.Number
          currentPage={i + 1 === parseInt(currentPage) ? true : false}
          primaryColor={primaryColor}
        >
          {i + 1}
        </S.Number>
      </a>
    )
  }
  return (
    <S.Container>
      {rows}
      <S.Part>|</S.Part>
      <S.TextPage>Ir para página</S.TextPage>
      <S.Input
        type="number"
        min={1}
        max={pages}
        value={inputPage}
        onChange={(e) => setInputPage(e.target.value)}
      />
      <S.Button
        onClick={() => router.push(`/${url}/${inputPage}`)}
        primaryColor={primaryColor}
      >
        {'>'}
      </S.Button>
    </S.Container>
  )
}

export default Pagination
