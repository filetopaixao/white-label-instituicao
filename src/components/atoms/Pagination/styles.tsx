import styled from 'styled-components'

interface StyledPaginationProps {
  primaryColor?: string
  currentPage?: boolean
}

export const Container = styled.div`
  display: flex;
  float: right;

  color: #7c7c7c;
  a {
    color: #7c7c7c;

    text-decoration: none;
  }
`

export const Number = styled.div<StyledPaginationProps>`
  width: 22px;
  height: 22px;
  margin-right: 7px;
  padding-top: 2px;

  border-radius: ${(props) => (props.currentPage ? '50px' : '')};

  color: ${(props) => (props.currentPage ? '#fff' : '')};
  background-color: ${(props) => (props.currentPage ? props.primaryColor : '')};
  box-shadow: ${(props) => (props.currentPage ? '3px 3px 9px' : '')};

  font-size: 14px;
  text-align: center;
  font-weight: ${(props) => (props.currentPage ? 'bold' : '')};
`

export const Input = styled.input`
  width: 43px;
  height: 23px;
  //margin-top: 2px;
  padding-left: 5px;

  border: solid 1px #bdbdbd;
  border-radius: 7px 0px 0px 7px;
`

export const Button = styled.span<StyledPaginationProps>`
  display: flex;
  align-items: center;

  //height: 27px;
  margin-left: -1px;
  padding: 1px 4px;

  border-radius: 0px 7px 7px 0px;

  color: #ffff;
  background-color: ${(props) => props.primaryColor};

  font-size: 16px;

  cursor: pointer;
`

export const Part = styled.span`
  margin-right: 20px;
  margin-left: 10px;
`

export const TextPage = styled.span`
  margin-right: 17px;
`

export default Container
