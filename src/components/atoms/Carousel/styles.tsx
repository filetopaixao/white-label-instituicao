import styled from 'styled-components'

import ArrowRightIcon from '../../../../public/images/arrow-right.svg'
import ArrowLeftIcon from '../../../../public/images/arrow-left.svg'

export const ArrowRight = styled.div`
  display: 'block'

  width: 25px;
  height: 25px;

  mask-image: url(${ArrowRightIcon});
  mask-size: 25px;
  -webkit-mask-image: url(${ArrowRightIcon});
  mask-repeat: no-repeat;
  background-color: #5e5e5e;
  ::before{
    content: none;
  }
  :hover{
    background-color: #5e5e5e;
  }
`
export const ArrowLeft = styled.div`
  display: 'block'

  width: 25px;
  height: 25px;
  
  mask-image: url(${ArrowLeftIcon});
  mask-size: 25px;
  -webkit-mask-image: url(${ArrowLeftIcon});
  mask-repeat: no-repeat;
  background-color: #5e5e5e;
  ::before{
    content: none;
  }
  :hover{
    background-color: #5e5e5e;
  }
`
