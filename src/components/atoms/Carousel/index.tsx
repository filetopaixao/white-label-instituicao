import { ReactNode } from 'react'
import Slider from 'react-slick'

interface ArrowProps {
  className?: string
  onClick?: () => void
}

import * as S from './styles'

interface CarouselProps {
  children: ReactNode
  len: number
}

const SampleNextArrow: React.FC<ArrowProps> = ({ className, onClick }) => {
  return <S.ArrowRight className={className} onClick={onClick} />
}

const SamplePrevArrow: React.FC<ArrowProps> = ({ className, onClick }) => {
  return <S.ArrowLeft className={className} onClick={onClick} />
}

const Carousel: React.FC<CarouselProps> = ({ children, len }) => {
  const settings = {
    dots: false,
    infinite: true,
    speed: 1000,
    slidesToShow: len < 3 ? len : 3,
    slidesToScroll: 1,
    initialSlide: 0,
    autoplay: true,
    autoplaySpeed: 5000,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 2,
          dots: false
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false
        }
      }
    ]
  }
  return (
    <Slider className="team-members" {...settings} arrows={true}>
      {children}
    </Slider>
  )
}

export default Carousel
