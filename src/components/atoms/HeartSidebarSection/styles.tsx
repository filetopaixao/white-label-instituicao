import styled from 'styled-components'

interface HeartSidebarSectionProps {
  primaryColor?: string
  secondaryColor?: string
  icon?: string
}

export const Container = styled.div<HeartSidebarSectionProps>`
  display: flex;

  padding: 45px 10px;

  .left {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    flex: 1;

    .icon-1 {
      .heart-lg {
        width: 100px;
        height: 100px;

        mask-image: ${(props) => `url(${props.icon})`};
        mask-size: 100px;
        -webkit-mask-image: ${(props) => `url(${props.icon})`};
        mask-repeat: no-repeat;
        //background-color: ${(props) => props.secondaryColor};
        background-color: #d7dee1;
      }
    }

    .icon-2 {
      display: flex;
      justify-content: flex-end;
      .heart-sm {
        width: 60px;
        height: 60px;

        mask-image: ${(props) => `url(${props.icon})`};
        mask-size: 60px;
        -webkit-mask-image: ${(props) => `url(${props.icon})`};
        mask-repeat: no-repeat;
        background-color: #d7dee1;
      }
    }

    .icon-3 {
      display: flex;
      .heart-sm {
        width: 30px;
        height: 30px;

        mask-image: ${(props) => `url(${props.icon})`};
        mask-size: 30px;
        -webkit-mask-image: ${(props) => `url(${props.icon})`};
        mask-repeat: no-repeat;
        background-color: #d7dee1;

        transform: scale(-1, 1);
      }
    }

    .icon-4 {
      display: flex;
      justify-content: flex-end;
      .heart-md {
        width: 80px;
        height: 80px;

        mask-image: ${(props) => `url(${props.icon})`};
        mask-size: 80px;
        -webkit-mask-image: ${(props) => `url(${props.icon})`};
        mask-repeat: no-repeat;
        //background-color: ${(props) => props.primaryColor};
        background-color: #d7dee1;
      }
    }

    .icon-5 {
      display: flex;
      justify-content: flex-end;
      .heart-sm {
        width: 30px;
        height: 30px;

        mask-image: ${(props) => `url(${props.icon})`};
        mask-size: 30px;
        -webkit-mask-image: ${(props) => `url(${props.icon})`};
        mask-repeat: no-repeat;
        background-color: #d7dee1;
      }
    }

    .icon-6 {
      display: flex;
      .heart-sm {
        width: 60px;
        height: 60px;

        mask-image: ${(props) => `url(${props.icon})`};
        mask-size: 60px;
        -webkit-mask-image: ${(props) => `url(${props.icon})`};
        mask-repeat: no-repeat;
        background-color: #d7dee1;

        transform: scale(-1, 1);
      }
    }
    @media (max-width: 768px) {
      display: none;
    }
  }
  .right {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    flex: 1;

    .icon-1 {
      display: flex;
      justify-content: flex-end;

      .heart-lg {
        width: 100px;
        height: 100px;

        mask-image: ${(props) => `url(${props.icon})`};
        mask-size: 100px;
        -webkit-mask-image: ${(props) => `url(${props.icon})`};
        mask-repeat: no-repeat;
        //background-color: ${(props) => props.secondaryColor};
        background-color: #d7dee1;

        transform: scale(-1, 1);
      }
    }

    .icon-2 {
      .heart-sm {
        width: 60px;
        height: 60px;

        mask-image: ${(props) => `url(${props.icon})`};
        mask-size: 60px;
        -webkit-mask-image: ${(props) => `url(${props.icon})`};
        mask-repeat: no-repeat;
        background-color: #d7dee1;

        transform: scale(-1, 1);
      }
    }

    .icon-3 {
      display: flex;
      justify-content: flex-end;
      .heart-sm {
        width: 30px;
        height: 30px;

        mask-image: ${(props) => `url(${props.icon})`};
        mask-size: 30px;
        -webkit-mask-image: ${(props) => `url(${props.icon})`};
        mask-repeat: no-repeat;
        background-color: #d7dee1;
      }
    }

    .icon-4 {
      display: flex;
      .heart-md {
        width: 80px;
        height: 80px;

        mask-image: ${(props) => `url(${props.icon})`};
        mask-size: 80px;
        -webkit-mask-image: ${(props) => `url(${props.icon})`};
        mask-repeat: no-repeat;
        //background-color: ${(props) => props.primaryColor};
        background-color: #d7dee1;

        transform: scale(-1, 1);
      }
    }

    .icon-5 {
      display: flex;
      .heart-sm {
        width: 30px;
        height: 30px;

        mask-image: ${(props) => `url(${props.icon})`};
        mask-size: 30px;
        -webkit-mask-image: ${(props) => `url(${props.icon})`};
        mask-repeat: no-repeat;
        background-color: #d7dee1;

        transform: scale(-1, 1);
      }
    }

    .icon-6 {
      display: flex;
      justify-content: flex-end;
      .heart-sm {
        width: 60px;
        height: 60px;

        mask-image: ${(props) => `url(${props.icon})`};
        mask-size: 60px;
        -webkit-mask-image: ${(props) => `url(${props.icon})`};
        mask-repeat: no-repeat;
        background-color: #d7dee1;
      }
    }

    .heart-lg {
      width: 100px;
      height: 100px;

      mask-image: ${(props) => `url(${props.icon})`};
      mask-size: 100px;
      -webkit-mask-image: ${(props) => `url(${props.icon})`};
      mask-repeat: no-repeat;
      //background-color: ${(props) => props.secondaryColor};
      background-color: #d7dee1;

      transform: scale(-1, 1);
    }
    @media (max-width: 768px) {
      display: none;
    }
  }
  .middle {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    flex: 8;

    padding-bottom: 40px;
    @media (max-width: 768px) {
      width: 100%;
      padding: 0 30px;
    }
  }
  @media (max-width: 768px) {
    height: auto;
  }
`
