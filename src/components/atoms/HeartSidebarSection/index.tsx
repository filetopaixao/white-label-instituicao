import { ReactNode } from 'react'
import { useContext } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'

import * as S from './styles'

interface HeartSidebarSectionProps {
  children: ReactNode
}

const HeartSidebarSection: React.FC<HeartSidebarSectionProps> = ({
  children
}) => {
  const { icon, primaryColor, secondaryColor } = useContext(ThemeContext)
  return (
    <S.Container
      primaryColor={primaryColor}
      secondaryColor={secondaryColor}
      icon={icon}
    >
      <div className="left">
        <div className="icon-1">
          <div className="heart-lg" />
        </div>
        <div className="icon-2">
          <div className="heart-sm" />
        </div>
        <div className="icon-3">
          <div className="heart-sm" />
        </div>
        <div className="icon-4">
          <div className="heart-md" />
        </div>
        <div className="icon-5">
          <div className="heart-sm" />
        </div>
        <div className="icon-6">
          <div className="heart-sm" />
        </div>
      </div>
      <div className="middle">{children}</div>
      <div className="right">
        <div className="icon-1">
          <div className="heart-lg" />
        </div>
        <div className="icon-2">
          <div className="heart-sm" />
        </div>
        <div className="icon-3">
          <div className="heart-sm" />
        </div>
        <div className="icon-4">
          <div className="heart-md" />
        </div>
        <div className="icon-5">
          <div className="heart-sm" />
        </div>
        <div className="icon-6">
          <div className="heart-sm" />
        </div>
      </div>
    </S.Container>
  )
}

export default HeartSidebarSection
