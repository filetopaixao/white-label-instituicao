import { Story, Meta } from '@storybook/react/types-6-0'
import HeartSidebarSection from '.'

export default {
  title: 'HeartSidebarSection',
  component: HeartSidebarSection
} as Meta

export const Basic: Story = () => (
  <HeartSidebarSection>Teste</HeartSidebarSection>
)
