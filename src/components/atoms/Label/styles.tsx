import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  justify-content: flex-start;
  width: 100%;
  padding-bottom: 8px;
  padding-top: 25px;
  label {
    font-size: 16px;
    font-weight: 600;
    color: #5e5e5e;
    text-transform: uppercase;
  }
`
