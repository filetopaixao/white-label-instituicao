import { Story, Meta } from '@storybook/react/types-6-0'
import Label from '.'

export default {
  title: 'Label',
  component: Label
} as Meta

export const Basic: Story = () => <Label htmlFor="input">Label</Label>
