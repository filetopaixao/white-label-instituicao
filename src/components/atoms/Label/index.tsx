import { ReactNode } from 'react'

import * as S from './styles'

interface LabelProps {
  children: ReactNode
  htmlFor?: string
}

const Label: React.FC<LabelProps> = ({ htmlFor, children }) => (
  <S.Container>
    <label htmlFor={htmlFor}>{children}</label>
  </S.Container>
)

export default Label
