import styled from 'styled-components'

export const Input = styled.input`
  width: -webkit-fill-available;
  color: #5e5e5e;
  border: 2px solid #e1e1e1;
  font-size: 14px;
  font-family: sans-serif;
  outline: none;
  background: #ffffff;
  border-radius: 4px;
  position: relative;
  padding: 15px;
  bottom: 0px;
  ::placeholder {
    color: #acacac;
  }
`
