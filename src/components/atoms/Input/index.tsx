import * as S from './styles'

interface InputProps {
  placeholder?: string
  value: any
  name?: string
  type?: string
  disabled?: boolean
  id?: string
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}

const Input: React.FC<InputProps> = (props) => (
  <S.Input
    value={props.value}
    name={props.name}
    type={props.type}
    placeholder={props.placeholder}
    onChange={props.onChange}
    id={props.id}
    disabled={props.disabled}
  />
)

export default Input
