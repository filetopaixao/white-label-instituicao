import { useState, useContext } from 'react'
import { useRouter } from 'next/router'
import { ThemeContext } from '../../../contexts/ThemeContext'
import * as S from './styles'

interface Item {
  itemName: string
  itemLink: string
}

interface ItemProps {
  itemName: string
  itemLink: string | Item[]
  isMobile: boolean
  setShowMenu?: any
}

const Item: React.FC<ItemProps> = ({
  itemName,
  itemLink,
  isMobile,
  setShowMenu
}) => {
  const { primaryColor } = useContext(ThemeContext)
  const router = useRouter()
  const [showDropdown, setShowDropdown] = useState(false)

  const handleDropdown = () => {
    setShowDropdown(!showDropdown)
  }

  //verifica se está em mobile ou desktop
  if (isMobile) {
    //em mobile
    //verifica se o item é dropdown
    if (typeof itemLink === 'string') {
      //não é dropdown
      return (
        <div>
          <S.ItemMobile
            onClick={() => {
              setShowMenu(false)
              router.push(itemLink)
            }}
          >
            {itemName}
          </S.ItemMobile>
        </div>
      )
    } else {
      //é dropdown
      return (
        <S.DropdownItemMobile showDropdown={showDropdown}>
          <button onClick={() => handleDropdown()}>
            {itemName}
            <div className="arrow-icon" />
          </button>
          <S.DropdownContentMobile showDropdown={showDropdown}>
            {itemLink.map((itemDropdown: Item, key: number) => (
              <a key={key} href={itemDropdown.itemLink}>
                {itemDropdown.itemName}
              </a>
            ))}
          </S.DropdownContentMobile>
        </S.DropdownItemMobile>
      )
    }
  } else {
    //em desktop
    //verifica se o item é dropdown
    if (typeof itemLink === 'string') {
      //não é dropdown
      return (
        <S.Item
          onClick={() => router.push(itemLink)}
          primaryColor={primaryColor}
        >
          {itemName}
        </S.Item>
      )
    } else {
      //é dropdown
      return (
        <S.DropdownItem primaryColor={primaryColor}>
          <button>
            {itemName}
            <div className="arrow-icon" />
          </button>
          <S.DropdownContent
            className="dropdown-content"
            primaryColor={primaryColor}
          >
            {itemLink.map((itemDropdown: Item, key: number) => (
              <a key={key} onClick={() => router.push(itemDropdown.itemLink)}>
                {itemDropdown.itemName}
              </a>
            ))}
          </S.DropdownContent>
        </S.DropdownItem>
      )
    }
  }
}

export default Item
