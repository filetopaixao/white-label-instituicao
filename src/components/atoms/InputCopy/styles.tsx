import styled from 'styled-components'

import CopyIcon from '../../../../public/images/copy.svg'

interface StyledInputCopyProps {
  backgroundColor?: string
  primaryColor?: string
  campaign_id?: string
}

export const Container = styled.div<StyledInputCopyProps>`
  display: flex;

  .copy-button-mobile {
    display: none;
  }

  @media (max-width: 768px) {
    align-items: center;

    height: 100%;

    .copy-button-mobile {
      display: flex;

      width: 15px;
      height: 17px;

      mask-image: url(${CopyIcon});
      mask-size: 15px;
      -webkit-mask-image: url(${CopyIcon});
      mask-repeat: no-repeat;
      background-color: ${(props) => props.primaryColor};

      cursor: pointer;
    }
  }
`

export const Input = styled.input<StyledInputCopyProps>`
  width: -webkit-fill-available;
  color: #5e5e5e;
  border: 1px solid #acacac;
  font-size: 14px;
  font-family: sans-serif;
  outline: none;
  background: #ffffff;
  border-radius: 4px 0 0 4px;
  position: relative;
  padding: 15px;
  bottom: 0px;
  ::placeholder {
    color: #acacac;
  }

  @media (max-width: 768px) {
    border: 0;
    padding: 0;
  }
`

export const Button = styled.button<StyledInputCopyProps>`
  display: flex;
  -webkit-box-align: center;
  align-items: center;
  -webkit-box-pack: center;
  justify-content: center;
  text-transform: uppercase;
  font-size: 16px;
  font-weight: 0;
  //border-radius: 5px;
  border: none;
  border-radius: 0 4px 4px 0;
  outline: none;
  height: 50px;
  width: 120px;
  background: ${(props) => props.backgroundColor};
  color: #fff;
  cursor: pointer;
  @media (max-width: 768px) {
    display: none;
  }
`
