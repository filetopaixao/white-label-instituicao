import { useContext } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'

import * as S from './styles'

interface PropsInputCopy {
  value: string
  id: string
}

const InputCopy: React.FC<PropsInputCopy> = ({ value, id }) => {
  const { primaryColor } = useContext(ThemeContext)

  const handleCopy = (id: string) => {
    const element = document.getElementById(id) as HTMLInputElement
    element.focus()
    element.select()
    element.setSelectionRange(0, 99999)
    document.execCommand('Copy')
  }

  return (
    <S.Container primaryColor={primaryColor}>
      <S.Input value={value} id={id} readOnly={true} />
      <div className="copy-button-mobile" onClick={() => handleCopy(id)} />
      <S.Button backgroundColor={primaryColor} onClick={() => handleCopy(id)}>
        copiar
      </S.Button>
    </S.Container>
  )
}

export default InputCopy
