import { Story, Meta } from '@storybook/react/types-6-0'
import InputCopy from '.'

export default {
  title: 'InputCopy',
  component: InputCopy
} as Meta

export const Basic: Story = () => <InputCopy value="teste" id="teste" />
