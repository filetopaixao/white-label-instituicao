import styled from 'styled-components'
import Arrow from '../../../../public/images/arrow.svg'

export const Container = styled.div``

interface StyledItemProps {
  primaryColor?: string
  showDropdown?: boolean
  avatarProfile?: string
}

export const Item = styled.a<StyledItemProps>`
  float: left;
  display: flex;
  align-items: center;

  height: 110px;
  margin: 0;
  padding: 0px 20px;

  background-color: inherit;
  color: #5e5e5e;

  font-family: inherit;
  text-align: center;
  text-decoration: none;
  cursor: pointer;
  :hover {
    color: ${(props) => props.primaryColor};
    font-weight: 0;
  }
`
export const DropdownItem = styled.div<StyledItemProps>`
  float: left;
  display: flex;
  align-items: center;

  height: 110px;
  padding: 0px 20px;
  overflow: hidden;

  color: white;
  border: none;

  outline: none;

  cursor: pointer;

  .avatar-profile {
    width: 70px;
    height: 70px;

    border-radius: 50%;

    background-image: ${(props) => `url(${props.avatarProfile})`};
    background-size: cover;
    background-repeat: no-repeat;
    background-position-x: center;
  }

  button {
    display: flex;
    align-items: center;

    height: 110px;
    margin: 0;

    color: #5e5e5e;
    background-color: inherit;
    border: none;
    outline: none;

    font-size: 16px;
    font-family: inherit;

    cursor: pointer;

    .arrow-icon {
      float: right;

      width: 11px;
      height: 6px;
      margin-left: 5px;

      mask-image: url(${Arrow});
      mask-size: 11px;
      -webkit-mask-image: url(${Arrow});
      mask-repeat: no-repeat;
      background-color: #5e5e5e;

      transform: rotate(180deg);
    }
  }

  :hover {
    .dropdown-content {
      display: block;
    }
    button {
      color: ${(props) => props.primaryColor};

      font-weight: 0;
    }
    .arrow-icon {
      background-color: ${(props) => props.primaryColor};
    }
  }
`

export const DropdownContent = styled.div<StyledItemProps>`
  position: absolute;
  display: none;

  min-width: 160px;
  top: 110px;

  background-color: #f9f9f9;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);

  z-index: 1;
  a {
    float: none;
    display: block;

    padding: 12px 16px;

    color: #5e5e5e;

    text-align: left;
    text-decoration: none;

    border-top: solid 1px #e1e1e1;
    :hover {
      //@ts-ignore
      background-color: ${(props) => `${props.primaryColor}0d`};
      color: ${(props) => props.primaryColor};

      font-weight: 0;
    }
  }
`
export const ItemMobile = styled.a`
  display: flex;
  align-items: center;

  width: 100%;
  height: 55px;
  padding: 0 32px 0 32px;

  color: #5e5e5e;
  border-bottom: 1pt solid #00000029;

  font-size: 15px;
  text-decoration: none;
`

export const DropdownItemMobile = styled.div<StyledItemProps>`
  transition: height 0.5s;
  button {
    display: flex;
    align-items: center;
    justify-content: space-between;

    width: 100%;
    height: 55px;
    padding: 0 32px;

    color: #5e5e5e;
    background-color: inherit;
    border: 0;
    border-bottom: 1pt solid #00000029;
    outline: none;

    font-size: 15px;
    font-family: inherit;
    text-align: left;

    cursor: pointer;

    .arrow-icon {
      float: right;

      width: 11px;
      height: 6px;

      mask-image: url(${Arrow});
      mask-size: 11px;
      -webkit-mask-image: url(${Arrow});
      mask-repeat: no-repeat;
      background-color: #5e5e5e;

      transform: ${(props) =>
        props.showDropdown ? 'rotate(0deg);' : 'rotate(180deg);'};
    }
  }
`

export const DropdownContentMobile = styled.div<StyledItemProps>`
  display: flex;
  flex-direction: column;

  height: auto;

  overflow-y: hidden;

  a {
    display: flex;
    align-items: center;

    height: 55px;
    padding: 0 32px;

    color: #5e5e5e;
    border-bottom: 1pt solid #00000029;

    font-size: 15px;
    font-family: inherit;
    text-decoration: none;
  }
`
