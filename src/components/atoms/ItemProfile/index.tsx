import { useState, useContext, useEffect } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'
import cookie from 'js-cookie'

import * as S from './styles'

interface Item {
  itemName: string
  itemLink: () => void
}

interface ItemProps {
  itemName: string
  itemLink: any
  isMobile: boolean
  setShowMenu?: any
}

const ItemProfile: React.FC<ItemProps> = ({
  itemName,
  itemLink,
  isMobile,
  setShowMenu
}) => {
  const { primaryColor } = useContext(ThemeContext)
  const [showDropdown, setShowDropdown] = useState(false)

  // const handleDropdown = () => {
  //   setShowDropdown(!showDropdown)
  // }

  const loggedUser = cookie.get('loggedUser')
    ? JSON.parse(`${cookie.get('loggedUser')}`)
    : {}

  //verifica se está em mobile ou desktop
  if (isMobile) {
    //em mobile
    //verifica se o item é dropdown
    if (typeof itemLink !== 'string') {
      //é dropdown
      return (
        <S.DropdownContentMobile showDropdown={showDropdown}>
          {itemLink.map((itemDropdown: Item, key: number) => (
            <a
              key={key}
              onClick={() => {
                setShowMenu(false)
                itemDropdown.itemLink()
              }}
            >
              {itemDropdown.itemName}
            </a>
          ))}
        </S.DropdownContentMobile>
      )
    } else {
      return <></>
    }
  } else {
    //em desktop
    //verifica se o item é dropdown

    //é dropdown
    if (typeof itemLink !== 'string') {
      return (
        <S.DropdownItem
          primaryColor={primaryColor}
          showDropdown={showDropdown}
          avatarProfile={loggedUser.image_profile}
        >
          <button>
            <div className="avatar-profile" />
            <div className="arrow-icon" />
          </button>
          <S.DropdownContent
            className="dropdown-content"
            primaryColor={primaryColor}
          >
            {itemLink.map((itemDropdown: Item, key: number) => (
              <a key={key} onClick={itemDropdown.itemLink}>
                {itemDropdown.itemName}
              </a>
            ))}
          </S.DropdownContent>
        </S.DropdownItem>
      )
    } else {
      return <></>
    }
  }
}

export default ItemProfile
