import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import Item from '.'

export default {
  title: 'Item',
  component: Item
} as Meta

export const Basic: Story = () => (
  <Item
    itemName="Teste"
    itemLink={[
      {
        itemName: 'Minha Conta',
        itemLink: () => false
      },
      {
        itemName: 'Minhas Campanhas',
        itemLink: () => false
      },
      {
        itemName: 'Minhas Doações',
        itemLink: () => false
      },
      {
        itemName: 'Sair',
        itemLink: () => false
      }
    ]}
    isMobile={false}
  />
)
