import { AnyAaaaRecord } from 'node:dns'
import * as S from './styles'

interface CardProps {
  bank: any
}

const CardEdit: React.FC<CardProps> = ({ bank }) => {
  return (
    <S.CardAddress key={bank.id}>
      <div className="div-content-card">
        <div className="banco-id">{bank.id}</div>
        <div className="banco-conta">
          <p>{bank.conta.banco}</p>
          <p>Ag: {bank.conta.agencia}</p>
          <p>Conta: {bank.conta.conta}</p>
          <p>{bank.conta.tipo}</p>
        </div>
        <div className="banco-titular">
          <p>{bank.titular.nome}</p>
          <p>{bank.titular.cpf}</p>
          <p>{bank.titular.telefone}</p>
        </div>
      </div>
      <S.RemoveAddress onClick={() => false} id={bank.id}>
        <S.RemoveIcon />
      </S.RemoveAddress>
    </S.CardAddress>
  )
}

export default CardEdit
