import styled from 'styled-components'

import IconAlert from '../../../../public/images/alert.svg'
import IconTrash from '../../../../public/images/trash.svg'

export const CardAddress = styled.div`
  display: flex;

  width: 100%;
  height: auto;

  margin-bottom: 10px;

  border-radius: 10px;

  border: 1px solid #cecece;

  background: #fbfbfb;

  overflow: hidden;
  .div-content-card {
    display: flex;

    font-size: 16px;

    width: 100%;

    padding: 10px 20px;

    color: #5e5e5e;

    div {
      width: 280px;
    }

    .banco-id {
      width: 180px;
    }

    .banco-status {
      display: flex;
      align-items: center;
    }
  }
`

export const RemoveAddress = styled.div`
  display: flex;
  justify-content: center;
  align-content: center;
  flex-direction: column;
  width: 40px;

  background: #cecece;

  cursor: pointer;
`

export const RemoveIcon = styled.div`
  -webkit-mask-image: url(${IconTrash});
  mask-image: url(${IconTrash});
  mask-repeat: no-repeat;
  mask-size: 20px;
  mask-position: center;
  background-color: #5e5e5e;
  width: 100%;
  height: 100%;
`

export const AlertIcon = styled.div`
  padding: 0px 20px;

  -webkit-mask-image: url(${IconAlert});
  mask-image: url(${IconAlert});
  mask-repeat: no-repeat;
  mask-size: 23px;
  background-color: #222933;
  width: 23px;
  height: 23px;
`
