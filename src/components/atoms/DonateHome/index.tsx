import { useContext } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'
import Button from 'components/atoms/Button'

import * as S from './styles'

const DonateHome: React.FC = () => {
  const { icon, secondaryColor, tertiaryColor } = useContext(ThemeContext)
  return (
    <S.Container tertiaryColor={tertiaryColor} icon={icon}>
      <div className="title">
        <div className="heart-icon" />
        <h1>Seja um parceiro pela vida!</h1>
      </div>
      <div className="buttons">
        <div>
          <Button color="#fff" textColor={secondaryColor} onClick={() => false}>
            doação única
          </Button>
        </div>
        <div>
          <Button color={secondaryColor} textColor="#fff" onClick={() => false}>
            doação mensal
          </Button>
        </div>
      </div>
    </S.Container>
  )
}

export default DonateHome
