import { Story, Meta } from '@storybook/react/types-6-0'
import DonateHome from '.'

export default {
title: 'DonateHome',
component: DonateHome
} as Meta

export const Basic: Story = () =>
<DonateHome />