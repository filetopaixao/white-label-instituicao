import styled from 'styled-components'

interface StyledDonateHomeProps {
  tertiaryColor?: string
  icon?: string
}

export const Container = styled.section<StyledDonateHomeProps>`
  display: flex;
  justify-content: center;
  align-items: center;

  height: 160px;
  padding: 53px 133px;
  margin-top: 80px;

  background-color: ${(props) => props.tertiaryColor};

  .title {
    display: flex;
    justify-content: center;
    align-items: center;

    width: 100%;

    .heart-icon {
      width: 35px;
      height: 35px;
      margin-right: 30px;

      mask-image: ${(props) => `url(${props.icon})`};
      mask-size: 35px;
      -webkit-mask-image: ${(props) => `url(${props.icon})`};
      mask-repeat: no-repeat;
      background-color: #fff;
      @media (max-width: 768px) {
        min-width: 35px;
      }
    }
    h1 {
      color: #fff;

      font-size: 34px;
    }
    @media (max-width: 768px) {
      margin-bottom: 30px;
    }
  }
  .buttons {
    display: flex;
    justify-content: center;
    align-items: center;

    width: 100%;

    div:first-child {
      width: 250px;
      padding: 0 32px;
      @media (max-width: 768px) {
        width: 100%;
        margin-bottom: 30px;
        padding: 0 85px;
      }
    }
    div:last-child {
      width: 250px;
      padding: 0 32px;
      @media (max-width: 768px) {
        width: 100%;
        padding: 0 85px;
      }
    }

    button {
      font-size: 16px;
      @media (max-width: 768px) {
        height: 60px;

        font-size: 18px;
      }
    }
    @media (max-width: 768px) {
      flex-direction: column;
    }
  }
  @media (max-width: 768px) {
    flex-direction: column;

    height: auto;
    padding: 53px 40px;
  }
`
