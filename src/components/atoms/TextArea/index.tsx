import * as S from './styles'

interface TextAreaProps {
  placeholder: string
  value: string
  id?: string
  onChange: (e: React.ChangeEvent<HTMLTextAreaElement>) => void
}

const TextArea: React.FC<TextAreaProps> = (props) => (
  <S.TextArea
    value={props.value}
    onChange={props.onChange}
    placeholder={props.placeholder}
  />
)

export default TextArea
