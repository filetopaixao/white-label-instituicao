import styled from 'styled-components'

export const TextArea = styled.textarea`
  width: -webkit-fill-available;

  border: 2px solid #e1e1e1;
  color: #5e5e5e;
  font-size: 14px;
  font-family: sans-serif;
  outline: none;
  background: #ffffff;
  border-radius: 4px;
  position: relative;
  padding: 15px;
  resize: none;
  bottom: 0px;
  ::placeholder {
    color: #acacac;
  }
`
