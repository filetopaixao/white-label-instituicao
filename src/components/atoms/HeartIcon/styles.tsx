import styled from 'styled-components'

interface StyledHeartIconProps {
  secondaryColor?: string
  icon?: string
}

export const Container = styled.div<StyledHeartIconProps>`
  display: flex;
  justify-content: center;
  padding: 35px;
  div {
    width: 35px;
    height: 35px;

    mask-image: ${(props) => `url(${props.icon})`};
    mask-size: 35px;
    -webkit-mask-image: ${(props) => `url(${props.icon})`};
    mask-repeat: no-repeat;
    background-color: ${(props) => props.secondaryColor};
  }
`
