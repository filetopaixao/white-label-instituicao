import { useContext } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'

import * as S from './styles'

const HeartIcon: React.FC = () => {
  const { icon, secondaryColor } = useContext(ThemeContext)
  return (
    <S.Container secondaryColor={secondaryColor} icon={icon}>
      <div />
    </S.Container>
  )
}

export default HeartIcon
