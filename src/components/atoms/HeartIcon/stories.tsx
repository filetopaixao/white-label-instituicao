import { Story, Meta } from '@storybook/react/types-6-0'
import HeartIcon from '.'

export default {
  title: 'HeartIcon',
  component: HeartIcon
} as Meta

export const Basic: Story = () => <HeartIcon />
