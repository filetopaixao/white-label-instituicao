import Image from 'next/image'
import { useContext } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'

interface LogoProps {
  width: number
  height: number
}

const Logo: React.FC<LogoProps> = (props) => {
  const { logo } = useContext(ThemeContext)
  return (
    <a href="/">
      {logo ? (
        <Image
          src={logo ? logo : ''}
          alt="Hospital de amor"
          width={props.width}
          height={props.height}
        />
      ) : null}
    </a>
  )
}
export default Logo
