import { ReactNode, useContext } from 'react'
import { ThemeContext } from '../../../../contexts/ThemeContext'
import ChangePassForm from 'components/molecules/Form/Profile/ChangePass'

import * as S from './styles'

interface ModalProps {
  title: string
  onClose: () => void
  id: string
  isVisible: boolean
  variant: string
  btnCloser: boolean
}

const Modal: React.FC<ModalProps> = ({
  title,
  onClose,
  id,
  isVisible,
  variant,
  btnCloser
}) => {
  const handleOutSideClick = (e: any) => {
    if (e.target.id === id) onClose()
  }
  const { primaryColor, icon } = useContext(ThemeContext)

  return (
    <>
      {isVisible ? (
        <S.Modal id={id} onClick={handleOutSideClick}>
          <S.ModalContent
            variant={variant}
            primaryColor={primaryColor}
            icon={icon}
          >
            <S.ButtonCloserModal>
              <button
                className="btnMdCloser"
                type="button"
                onClick={() => onClose()}
              >
                <div />
              </button>
            </S.ButtonCloserModal>
            <div className="left">
              <div className="hand-1" />
              <div className="hand-2" />
              <div className="hand-3" />
              <div className="hand-4" />
              <div className="hand-5" />
              <div className="hand-6" />
            </div>
            <div className="right">
              {title ? (
                <S.ModalHeader>
                  <h3>{title}</h3>
                </S.ModalHeader>
              ) : null}
              <S.ModalBody>
                <ChangePassForm />
              </S.ModalBody>
            </div>
          </S.ModalContent>
        </S.Modal>
      ) : null}
    </>
  )
}

export default Modal
