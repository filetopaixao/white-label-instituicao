import styled, { css } from 'styled-components'

import CloseIcon from '../../../../../public/images/close.svg'

interface StyledModalProps {
  variant?: string
  primaryColor?: string
  icon?: string
}

const variants: any = {
  lg: () => css`
    width: 85%;
  `,
  md: () => css`
    width: 50%;
  `,
  sm: () => css`
    width: 30%;
  `
}

export const Modal = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  left: 0;
  top: 0;

  width: 100%;
  height: 100vh;

  box-shadow: 0 3px 26px rgba(0, 0, 0, 0.3);
  background: #ffffff;
  background-color: rgba(0, 0, 0, 0.6);

  overflow: auto;
  z-index: 999;
`

export const ModalContent = styled.div<StyledModalProps>`
  display: flex;
  position: relative;

  ${(props) => props.variant && variants[props.variant]}
  height: auto;

  margin: 20px 0 20px 0;
  padding: 20px;

  border-radius: 5px;
  z-index: 2;

  background:#FFFFFF};

  overflow: hidden;

  span {
    margin-left: 10px;
  }

  .left{
    position: absolute;
    left: 0;
    top: 0;

    display: flex;
    flex-direction: column;
    justify-content: space-between;

    width:30%;
    height: 100%;
    padding: 15px;

    background-color:${(props) => props.primaryColor};

    .hand-1{
      display: flex;
      justify-content: flex-end;

      width: 100%;
      height: 100px;

      mask-image: ${(props) => `url(${props.icon})`};
      mask-size: 100px;
      -webkit-mask-image: ${(props) => `url(${props.icon})`};
      mask-repeat: no-repeat;
      background-color: #fff;
      mask-position: right;
    }
    .hand-2{
      display: flex;

      width: 100%;
      height: 50px;

      mask-image: ${(props) => `url(${props.icon})`};
      mask-size: 50px;
      -webkit-mask-image: ${(props) => `url(${props.icon})`};
      mask-repeat: no-repeat;
      background-color: #fff;
      mask-position: left;
    }
    .hand-3{
      display: flex;

      width: 100%;
      height: 25px;

      mask-image: ${(props) => `url(${props.icon})`};
      mask-size: 25px;
      -webkit-mask-image: ${(props) => `url(${props.icon})`};
      mask-repeat: no-repeat;
      background-color: #fff;
      mask-position: right;
    }
    .hand-4{
      display: flex;

      width: 100%;
      height: 80px;

      mask-image: ${(props) => `url(${props.icon})`};
      mask-size: 80px;
      -webkit-mask-image: ${(props) => `url(${props.icon})`};
      mask-repeat: no-repeat;
      background-color: #fff;
      mask-position: center;
    }
    .hand-5{
      display: flex;

      width: 100%;
      height: 25px;

      mask-image: ${(props) => `url(${props.icon})`};
      mask-size: 25px;
      -webkit-mask-image: ${(props) => `url(${props.icon})`};
      mask-repeat: no-repeat;
      background-color: #fff;
      mask-position: left;
    }

    .hand-6{
      display: flex;

      width: 100%;
      height: 50px;

      mask-image: ${(props) => `url(${props.icon})`};
      mask-size: 50px;
      -webkit-mask-image: ${(props) => `url(${props.icon})`};
      mask-repeat: no-repeat;
      background-color: #fff;
      mask-position: right;
    }
    @media (max-width: 768px) {
      display: none;
    }
  }

  .right{
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    width: 100%;
    padding-left: 30%;
    @media (max-width: 768px) {
      padding-left: 0;
    }
  }

  @media (max-width: 768px) {
    width: 90%;
    padding: 15px 15px;
  }
`

export const ModalBody = styled.div`
  height: auto;
  margin-top: 10px;

  iframe {
    width: 100%;
    height: 350px;

    border: none;
  }

  @media (min-width: 1400px) {
    margin: 23px 0px 40px 0px;
  }

  @media (max-width: 992px) {
    margin: 10px 0px 10px 0px;
  }
  @media (max-width: 768px) {
    padding: 15px;
  }
`

export const ModalHeader = styled.header`
  h3 {
    width: 100%;

    font-size: 14px;
    font-weight: 600;
    text-align: center;
    color: #5e5e5e;
    @media (min-width: 768px) {
      font-size: 18px;
    }
  }
`

export const ButtonCloserModal = styled.div`
  display: flex;
  justify-content: flex-end;

  &.closerSidebar {
    display: none;
    @media (max-width: 600px) {
      display: flex;

      padding-right: 10px;
    }
  }

  &.closerContent {
    display: flex;

    padding-right: 10px;
    @media (max-width: 600px) {
      display: none;
    }
  }

  button {
    position: absolute;
    right: 10px;
    top: 10px;

    font-size: 14px;

    border: none;
    outline: none;

    color: #5e5e5e;
    background: transparent;
    opacity: 1;
    box-shadow: none;

    text-transform: none;
    letter-spacing: 0px;

    cursor: pointer;

    &:focus {
      outline: thin dotted;
      outline: 0px auto -webkit-focus-ring-color;
      outline-offset: 0px;
    }

    div {
      width: 12px;
      height: 12px;

      mask-image: url(${CloseIcon});
      mask-size: 12px;
      -webkit-mask-image: url(${CloseIcon});
      mask-repeat: no-repeat;
      background-color: #5e5e5e;
    }
  }
`
