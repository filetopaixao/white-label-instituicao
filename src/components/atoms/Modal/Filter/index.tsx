import { ReactNode } from 'react'

import * as S from './styles'

interface ModalProps {
  children: ReactNode
  title: string
  onClose: () => void
  id: string
  isVisible: boolean
  variant: string
  btnCloser: boolean
}

const ModalFilter: React.FC<ModalProps> = ({
  children,
  title,
  onClose,
  id,
  isVisible,
  variant,
  btnCloser
}) => {
  const handleOutSideClick = (e: any) => {
    if (e.target.id === id) onClose()
  }

  return (
    <>
      {isVisible ? (
        <S.Modal id={id} onClick={handleOutSideClick}>
          <S.ModalContent variant={variant}>
            <S.ButtonCloserModal>
              <button
                className="btnMdCloser"
                type="button"
                onClick={() => onClose()}
              >
                <div />
              </button>
            </S.ButtonCloserModal>
            {title ? (
              <S.ModalHeader>
                <h3>{title}</h3>
              </S.ModalHeader>
            ) : null}
            <S.ModalBody>{children}</S.ModalBody>
          </S.ModalContent>
        </S.Modal>
      ) : null}
    </>
  )
}

export default ModalFilter
