import styled, { css } from 'styled-components'

import CloseIcon from '../../../../../public/images/close.svg'

interface StyledModalProps {
  variant: string
}

const variants: any = {
  lg: () => css`
    width: 85%;
  `,
  md: () => css`
    width: 50%;
  `,
  sm: () => css`
    width: 30%;
  `
}

export const Modal = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  left: 0;
  top: 0px;

  width: 100%;
  height: 100vh;

  box-shadow: 0 3px 26px rgba(0, 0, 0, 0.3);
  background: #ffffff;
  background-color: rgba(0, 0, 0, 0.6);

  overflow: auto;
  z-index: 999;
  @media (max-width: 768px) {
    padding-top: 250px;
  }
`

export const ModalContent = styled.div<StyledModalProps>`
  display: inline-table;
  position: absolute;
  right: 0;

  width: 25%;
  height: 100%;

  margin: 20px 0 20px 0;
  padding: 20px;

  z-index: 2;

  background:#FFFFFF};

  overflow: hidden;

  span {
    margin-left: 10px;
  }

  @media (max-width: 500px) {
    width: 90%;
    padding: 15px 15px;
    top: 48px;
  }
`

export const ModalBody = styled.div`
  height: auto;
  margin-top: 10px;

  iframe {
    width: 100%;
    height: 350px;

    border: none;
  }

  @media (min-width: 1400px) {
    margin: 23px 0px 40px 0px;
  }

  @media (max-width: 992px) {
    margin: 10px 0px 10px 0px;
  }
`

export const ModalHeader = styled.header`
  h3 {
    width: 100%;

    font-size: 14px;
    font-weight: 600;
    text-align: center;
    color: #5e5e5e;
    @media (min-width: 768px) {
      font-size: 18px;
    }
  }
`

export const ButtonCloserModal = styled.div`
  display: flex;
  justify-content: flex-end;

  &.closerSidebar {
    display: none;
    @media (max-width: 600px) {
      display: flex;

      padding-right: 10px;
    }
  }

  &.closerContent {
    display: flex;

    padding-right: 10px;
    @media (max-width: 600px) {
      display: none;
    }
  }

  button {
    position: absolute;
    right: 10px;
    top: 10px;

    font-size: 14px;

    border: none;
    outline: none;

    color: #5e5e5e;
    background: transparent;
    opacity: 1;
    box-shadow: none;

    text-transform: none;
    letter-spacing: 0px;

    cursor: pointer;

    &:focus {
      outline: thin dotted;
      outline: 0px auto -webkit-focus-ring-color;
      outline-offset: 0px;
    }

    div {
      width: 12px;
      height: 12px;

      mask-image: url(${CloseIcon});
      mask-size: 12px;
      -webkit-mask-image: url(${CloseIcon});
      mask-repeat: no-repeat;
      background-color: #5e5e5e;
    }
  }
`
