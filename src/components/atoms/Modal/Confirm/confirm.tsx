import { ReactNode, useContext } from 'react'
import { ThemeContext } from '../../../../contexts/ThemeContext'
import Button from 'components/atoms/Button'

import * as S from './styles'

interface ModalProps {
  message: string
  title: string
  onClose: () => void
  id: string
  isVisible: boolean
  variant: string
  btnCloser: boolean
  handleConfirm: () => void
}

const ModalConfirm: React.FC<ModalProps> = ({
  message,
  title,
  onClose,
  id,
  isVisible,
  variant,
  btnCloser,
  handleConfirm
}) => {
  const { primaryColor } = useContext(ThemeContext)
  const handleOutSideClick = (e: any) => {
    if (e.target.id === id) onClose()
  }

  return (
    <>
      {isVisible ? (
        <S.Modal id={id} onClick={handleOutSideClick}>
          <S.ModalContent variant={variant}>
            {btnCloser && (
              <S.ButtonCloserModal>
                <button
                  className="btnMdCloser"
                  type="button"
                  onClick={() => onClose()}
                >
                  <div />
                </button>
              </S.ButtonCloserModal>
            )}
            {title ? (
              <S.ModalHeader>
                <h3>{title}</h3>
              </S.ModalHeader>
            ) : null}
            <S.ModalBody>
              <S.ModalBodyConfirm>
                <div className="confirm-message">{message}</div>
                <div className="confirm-buttons">
                  <div className="btn-cancel">
                    <Button
                      color="#ACACAC"
                      borderRadius={5}
                      onClick={handleConfirm}
                    >
                      SIM
                    </Button>
                  </div>
                  <div className="btn-confirm">
                    <Button
                      color={primaryColor}
                      borderRadius={5}
                      onClick={() => onClose()}
                    >
                      NÃO
                    </Button>
                  </div>
                </div>
              </S.ModalBodyConfirm>
            </S.ModalBody>
          </S.ModalContent>
        </S.Modal>
      ) : null}
    </>
  )
}

export default ModalConfirm
