import * as S from './styles'

interface ModalProps {
  onClose: () => void
  id: string
  isVisible: boolean
  variant: string
  urlVideo: string
}

const ModalVideo: React.FC<ModalProps> = ({
  id,
  onClose,
  isVisible,
  variant,
  urlVideo
}) => {
  const handleOutSideClick = (e: any) => {
    if (e.target.id === id) onClose()
  }

  return (
    <>
      {isVisible ? (
        <S.Modal id="modal-video" onClick={handleOutSideClick}>
          <S.ModalContent variant={variant}>
            <S.ButtonCloserModal>
              <button
                className="btnMdCloser"
                type="button"
                onClick={() => onClose()}
              >
                <div />
              </button>
            </S.ButtonCloserModal>
            <S.ModalBody>
              <iframe
                title="tutorial"
                className="embed-responsive-item"
                src={urlVideo}
                allowFullScreen
              />
            </S.ModalBody>
          </S.ModalContent>
        </S.Modal>
      ) : null}
    </>
  )
}

export default ModalVideo
