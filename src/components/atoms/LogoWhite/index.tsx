import Image from 'next/image'
import { useContext } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'

interface LogoWhiteProps {
  width: number
  height: number
}

const LogoWhite: React.FC<LogoWhiteProps> = (props) => {
  const { logoWhite } = useContext(ThemeContext)
  return (
    <a href="/">
      {logoWhite ? (
        <Image
          src={logoWhite ? logoWhite : ''}
          alt="Hospital de amor"
          width={props.width}
          height={props.height}
        />
      ) : null}
    </a>
  )
}
export default LogoWhite
