import { Story, Meta } from '@storybook/react/types-6-0'
import GoalBar from '.'

export default {
  title: 'GoalBar',
  component: GoalBar
} as Meta

export const Basic: Story = () => (
  <GoalBar gloalValue={5000} amountCollected={250} />
)
