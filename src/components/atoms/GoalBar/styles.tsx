import styled from 'styled-components'

interface StyledGoalBarProps {
  percent?: number
  primaryColor?: string
  status?: string
}

export const Container = styled.div`
  display: flex;
  align-items: center;

  width: 100%;
  padding-top: 18px;
  > div {
    position: relative;
    flex: 1;

    height: 10px;

    background: #acacac33;
  }
`
export const GoalBar = styled.div<StyledGoalBarProps>`
  width: ${(props) => `${props.percent}%;`};
  height: 10px;

  background: ${props => props.status == 'inactive' || props.status == 'ended' ? '#ACACAC' : props.primaryColor};
`
export const PercentText = styled.span<StyledGoalBarProps>`
  position: absolute;
  top: -18px;
  left: ${(props) => `${props.percent}%;`}

  font-size: 16px;
  color: #5E5E5E;
  font-weight: 600;

  transform: translateX(-50%);
`
