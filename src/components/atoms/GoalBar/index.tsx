import { useContext } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'

import * as S from './styles'

interface GoalBar {
  gloalValue: number
  amountCollected: number
  status: string
}

const GoalBar: React.FC<GoalBar> = ({ gloalValue, amountCollected, status }) => {
  const { primaryColor } = useContext(ThemeContext)
  const percent = Math.round((amountCollected * 100) / gloalValue)
  return (
    <S.Container>
      <div>
        <S.PercentText percent={percent < 100 ? percent : 100}>
          {percent}%
        </S.PercentText>
        <S.GoalBar
          percent={percent < 100 ? percent : 100}
          primaryColor={primaryColor}
          status={status}
        />
      </div>
    </S.Container>
  )
}

export default GoalBar
