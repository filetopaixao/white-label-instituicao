import { Story, Meta } from '@storybook/react/types-6-0'
import Checkbox from '.'

export default {
  title: 'Checkbox',
  component: Checkbox
} as Meta

export const Basic: Story = () => (
  <Checkbox name="teste" value="teste" onChange={(e) => e.target.value} />
)
