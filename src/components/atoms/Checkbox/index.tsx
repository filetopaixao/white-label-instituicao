import * as S from './styles'

interface CheckboxProps {
  name: string
  value: any
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}

const Checkbox: React.FC<CheckboxProps> = (props) => {
  return (
    <S.Checkbox
      type="checkbox"
      value={props.value}
      name={props.name}
      onChange={props.onChange}
    />
  )
}

export default Checkbox
