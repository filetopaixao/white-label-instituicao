import { Story, Meta } from '@storybook/react/types-6-0'
import NewsWithButton from './NewsWithButton'

export default {
  title: 'NewsWithButton',
  component: NewsWithButton
} as Meta

export const Basic: Story = () => (
  <NewsWithButton
    key={1}
    image="teste"
    title="name"
    description="description"
    button_text="teste"
    button_redirect_url="#"
  />
)
