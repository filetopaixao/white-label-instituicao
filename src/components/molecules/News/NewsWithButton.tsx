import { useContext } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'
import Button from 'components/atoms/Button'

import * as S from './styles'

interface NewsWithButtonProps {
  key?: number
  image?: string
  title?: string
  description?: string
  button_text?: string
  button_redirect_url?: string
}

const NewsWithButton: React.FC<NewsWithButtonProps> = ({
  key,
  image,
  title,
  description,
  button_text,
  button_redirect_url
}) => {
  const { secondaryColor } = useContext(ThemeContext)

  return (
    <S.CardContainer key={key}>
      <S.CardNew>
        <S.ImageNew image={image} />
        <S.InfoNew>
          <div>
            <h1>{title}</h1>
            <p>
              {description && description.length > 120
                ? description?.substr(0, 119) + '...'
                : description}
            </p>
          </div>
          <div>
            <Button color={secondaryColor} onClick={() => false}>
              ler mais
            </Button>
          </div>
        </S.InfoNew>
      </S.CardNew>
    </S.CardContainer>
  )
}

export default NewsWithButton
