import { useContext } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'

import * as S from './styles'

interface NewsWithoutButtonProps {
  key?: number
  image?: string
  title?: string
  description?: string
  button_text?: string
  button_redirect_url?: string
}

const NewsWithoutButton: React.FC<NewsWithoutButtonProps> = ({
  key,
  image,
  title,
  description,
  button_redirect_url
}) => {
  const { tertiaryColor } = useContext(ThemeContext)

  return (
    <S.CardContainerWhitoutButton key={key}>
      <S.CardNewWithoutButton tertiaryColor={tertiaryColor} image={image}>
        <S.InfoNewWithoutButton>
          <div>
            <h1>{title}</h1>
            <p>
              {description && description.length > 120
                ? description.substr(0, 119) + '...'
                : description}
            </p>
          </div>
        </S.InfoNewWithoutButton>
      </S.CardNewWithoutButton>
    </S.CardContainerWhitoutButton>
  )
}

export default NewsWithoutButton
