import styled from 'styled-components'

interface StyledCardNewProps {
  image?: string
  tertiaryColor?: string
}

export const CardContainer = styled.div`
  width: 30%;
  padding: 20px;
  @media (max-width: 768px) {
    width: 100%;
    padding: 0px;
    padding-bottom: 30px;
  }
`
export const CardContainerWhitoutButton = styled.div`
  width: 70%;
  padding: 20px;
  @media (max-width: 768px) {
    width: 100%;
    padding: 0px;
    padding-bottom: 30px;
  }
`
export const CardNew = styled.div`
  width: 100%;
  height: 520px;

  box-shadow: 0px 3px 10px #00000029;
  @media (max-width: 768px) {
    width: 100%;
  }
`
export const CardNewWithoutButton = styled.div<StyledCardNewProps>`
  width: 100%;
  height: 520px;
  //max-height: 520px;

  background-image: ${(props) =>
      `linear-gradient(${props.tertiaryColor}b3, ${props.tertiaryColor}b3)`},
    ${(props) => `url(${props.image})`};
  background-position: center center;
  background-size: cover;
  background-repeat: no-repeat;

  box-shadow: 0px 3px 10px #00000029;
  @media (max-width: 768px) {
    width: 100%;
  }
`
export const ImageNew = styled.div<StyledCardNewProps>`
  width: 100%;
  height: 255px;

  background: ${(props) => `url(${props.image});`}
  background-position: center center;
  background-size: cover;
  background-repeat: no-repeat;
`
export const InfoNew = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  height: calc(100% - 255px);
  padding: 32px;
  h1 {
    padding-bottom: 15px;

    color: #5e5e5e;

    font-size: 24px;
    text-transform: capitalize;
  }
  p {
    padding-bottom: 32px;

    color: #acacac;

    font-size: 16px;
  }
  @media (max-width: 768px) {
    padding: 20pt;
  }
`
export const InfoNewWithoutButton = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;

  width: 50%;
  height: 100%;
  padding: 32px;
  h1 {
    padding-bottom: 15px;

    color: #fff;

    font-size: 24px;
    text-transform: capitalize;
  }
  p {
    padding-bottom: 32px;

    color: #fff;

    font-size: 16px;
  }
  > div {
    position: relative;
    top: 75px;
  }
  @media (max-width: 768px) {
    width: 100%;
    padding: 20pt;
  }
`
