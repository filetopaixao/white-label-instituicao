import { ReactNode } from 'react'
import * as S from './styles'

interface MenuProps {
    children: ReactNode
    showDropdown: boolean
    collapseTarget: string
}

const CollapseButton: React.FC<MenuProps> = ({ children, showDropdown, collapseTarget }) => {
    return (
        <>
            <S.DropdownContentMobile showDropdown={showDropdown}>
                {children}
            </S.DropdownContentMobile>
        </>
    )
}

export default CollapseButton
