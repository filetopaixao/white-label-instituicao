import { useContext, useEffect, useState } from 'react'
import dynamic from 'next/dynamic'
import { ThemeContext } from '../../../contexts/ThemeContext'
import { useRouter } from 'next/router'
import GoalBar from 'components/atoms/GoalBar'
import Button from 'components/atoms/Button'
import CollapseButton from './CollapseButton'
import CollapseContent from './CollapseContent'

import Datatable2 from 'components/molecules/Datatable2'

import * as S from './styles'

interface CardCampaignProps {
  columnsDesktop: any
  columnsMobile: any
  data: any
  collapseMobile: any
}
const Datatable: React.FC<CardCampaignProps> = ({
  columnsDesktop,
  columnsMobile,
  collapseMobile,
  data
}) => {
  const { secondaryColor } = useContext(ThemeContext)
  const router = useRouter()
  const [isMobile, setIsmobile] = useState(false)
  const [showDropdown, setShowDropdown] = useState(false)

  useEffect(() => {
    if (process.browser) {
      if (window.innerWidth <= 768) {
        setIsmobile(true)
      } else {
        setIsmobile(false)
      }

    }
  }, [])

  const handleCollapse = (id) => {
    document.getElementById(id).style.display = document.getElementById(id).style.display == 'block' ? 'none' : 'block'
  }

  const handleDropdown = () => {
    setShowDropdown(!showDropdown)
  }

  return (
    <S.Container>
      {isMobile ? (
        <Datatable2 columnsMobile={columnsMobile} data={data} collapseMobile={collapseMobile} />
        // <table>
        //   <S.ThHeader>
        //     {columnsMobile.map(column => <th>{column.Header}</th>)}
        //     <th></th>
        //   </S.ThHeader>
        //   {data.map((row, key) => {
        //     let contentCollapse
        //     return (
        //       <>
        //         <tr>
        //           {columnsMobile.map((header) => {
        //             contentCollapse = row['name']
        //             return (
        //               <>
        //                 <td>{header.Cell ? header.Cell(row[header.accessor]) : row[header.accessor]}</td>
        //               </>
        //             )
        //           })}
        //           <td>
        //             <CollapseButton
        //               collapseTarget={`RowCollapse${key}`}
        //               key={key}
        //               showDropdown={showDropdown}
        //               handleDropdown={handleCollapse}
        //             />
        //           </td>
        //         </tr>
        //         <div id={`RowCollapse${key}`} style={{ display: 'none' }}>
        //           <CollapseContent
        //             key={key}
        //             showDropdown={showDropdown}
        //             collapseTarget={`RowCollapse${key}`}
        //           >
        //             {contentCollapse}dsfsdfsdf
        //           </CollapseContent>
        //         </div>
        //       </>
        //     )
        //   }
        //   )}
        // </table>
      ) : (
        <table>
          <thead>
            <S.ThHeader>
              {columnsDesktop.map(column => <th>{column.Header}</th>)}
            </S.ThHeader>
          </thead>
          <tbody>
            {data.map(row => {
              return (
                <tr>
                  {columnsDesktop.map(header => (

                    <>
                      <td>{header.Cell ? header.Cell(row[header.accessor]) : row[header.accessor]}</td>
                    </>

                  ))}
                </tr>
              )
            }
            )}
          </tbody>
        </table>
      )}
    </S.Container>
  )
}

export default Datatable
