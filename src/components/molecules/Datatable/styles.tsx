import styled from 'styled-components'

import Arrow from '../../../../public/images/arrow.svg'

interface StyledCardCampaignProps {
}

export const Container = styled.div`
  display: flex;

  width: 100%;
  padding: 20px;

  border: 1px solid #E1E1E1;
  border-radius: 5px;

  table{
    border-collapse: collapse;
    width: 100%;

    tr {
      width: 100%;
    }

    td, th {
      border-bottom: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }
  }

  @media (max-width: 768px) {
  }
`

export const ThHeader = styled.tr`

  color: #5E5E5E;
  
  font-weight: bold;
  text-transform: uppercase;

  @media (max-width: 768px) {
  }
`

export const DropdownItemMobile = styled.div<StyledItemProps>`
  transition: height 0.5s;
  button {
    display: flex;
    align-items: center;
    justify-content: space-between;

    width: 100%;
    height: 55px;

    color: #5e5e5e;
    background-color: inherit;
    border: 0;
    outline: none;

    font-size: 15px;
    font-family: inherit;
    text-align: left;

    cursor: pointer;

    .arrow-icon {
      float: right;

      width: 11px;
      height: 6px;

      mask-image: url(${Arrow});
      mask-size: 11px;
      -webkit-mask-image: url(${Arrow});
      mask-repeat: no-repeat;
      background-color: #5e5e5e;

      transform: ${(props) =>
    props.showDropdown ? 'rotate(0deg);' : 'rotate(180deg);'};
    }
  }
`

export const DropdownContentMobile = styled.div<StyledItemProps>`
  display: flex;
  flex-direction: column;
  height: auto;

  overflow-y: hidden;

  a {
    display: flex;
    align-items: center;

    height: 55px;
    padding: 0 64px;

    color: #5e5e5e;
    border-bottom: none;

    font-size: 15px;
    font-family: inherit;
    text-decoration: none;
  }

  td{
    width: 100%;
  }
`