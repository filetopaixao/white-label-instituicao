import { useContext } from 'react'
import { MenuContext } from '../../../contexts/MenuContext'
import { ThemeContext } from '../../../contexts/ThemeContext'
import Item from 'components/atoms/Item'
import Search from 'components/molecules/Form/Search'
import Button from 'components/atoms/Button'
import cookie from 'js-cookie'

import * as S from './styles'

interface MenuProps {
  isMobile: boolean
  showMenu: boolean
  setShowMenu?: any
}

const Menu: React.FC<MenuProps> = ({ isMobile, showMenu, setShowMenu }) => {
  const { menu } = useContext(MenuContext)
  const { secondaryColor } = useContext(ThemeContext)
  return (
    <>
      <S.Container>
        {menu.map((item, key) => {
          if (
            (item.itemName === 'Login' || item.itemName === 'Cadastre-se') &&
            cookie.get('loggedUser')
          )
            return
          return (
            <Item
              key={key}
              itemLink={item.itemLink}
              itemName={item.itemName}
              isMobile={isMobile}
              setShowMenu={setShowMenu}
            />
          )
        })}
      </S.Container>
      <S.ContainerMobile showMenu={showMenu}>
        {menu.map((item, key) => {
          if (
            (item.itemName === 'Login' || item.itemName === 'Cadastre-se') &&
            cookie.get('loggedUser')
          )
            return
          return (
            <Item
              key={key}
              itemLink={item.itemLink}
              itemName={item.itemName}
              isMobile={isMobile}
              setShowMenu={setShowMenu}
            />
          )
        })}
        <S.MenuBottomMobile>
          <Search />
          <Button color={secondaryColor} onClick={() => false}>
            criar campanha
          </Button>
        </S.MenuBottomMobile>
      </S.ContainerMobile>
    </>
  )
}

export default Menu
