import styled from 'styled-components'

interface StyledMenuProps {
  showMenu?: boolean
}

export const Container = styled.nav`
  display: flex;

  color: #5e5e5e;

  font-size: 16px;

  @media (max-width: 768px) {
    display: none;
  }
`

export const ContainerMobile = styled.div<StyledMenuProps>`
  position: absolute; /* Stay in place */
  top: 65px;
  left: 0;
  display: none;

  height: 100vh; /* 100% Full-height */
  width: ${(props) => (props.showMenu ? '80%;' : '0;')}
  
  background-color: #fff; /* Black*/

  overflow-x: hidden; /* Disable horizontal scroll */
  z-index: 1; /* Stay on top */
  transition: 0.5s;

  @media (max-width: 768px) {
    display: flex;
    flex-direction: column;

    a {
      transition: 0.3s;
    }
  }
`

export const MenuBottomMobile = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  padding: 32px;

  > button {
    margin-top: 35px;
  }
`
