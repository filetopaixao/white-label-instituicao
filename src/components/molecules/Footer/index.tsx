import { useContext } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'
import LogoWhite from 'components/atoms/LogoWhite'

import * as S from './styles'

const Footer: React.FC = () => {
  const { primaryColor } = useContext(ThemeContext)
  return (
    <S.Container primaryColor={primaryColor}>
      <div className="row-top">
        <div className="logo">
          <LogoWhite width={300} height={140} />
        </div>
        <div className="social-media">
          <h1>nossas redes sociais:</h1>
          <div className="social-icons">
            <a href="https://www.facebook.com/ohospitaldeamor/" target="_blank">
              <div className="icon-facebook" />
            </a>
            <a href="https://www.instagram.com/hospitaldeamor/" target="_blank">
              <div className="icon-instagram" />
            </a>
            <a href="https://twitter.com/hospitaldeamor" target="_blank">
              <div className="icon-twitter" />
            </a>
            <a href="https://www.youtube.com/hospitaldeamor" target="_blank">
              <div className="icon-youtube" />
            </a>
          </div>
        </div>
      </div>
      <div className="row-bottom">
        <div className="footer-menu">
          <div className="footer-menu-left">
            <h1>conheça-nos</h1>
            <a href="#">Notícias</a>
            <a href="#">Site Institucional</a>
            <a href="#">História do Hospital</a>
            <a href="#">Fale Conosco</a>
          </div>
          <div className="footer-menu-right">
            <h1>como ajudar</h1>
            <a href="#">Doação Única</a>
            <a href="#">Doação Mensal</a>
            <a href="#">Imposto de Renda</a>
            <a href="#">Criar Campanha</a>
          </div>
        </div>
        <div className="bottom-right">
          <div className="privacy-policy">
            <a href="#">Política de Privacidade</a>
          </div>
          <div className="copy">
            <p>© 1999 - 2020 Hospital de Amor</p>
            <p>
              <span>Powered by </span>
              <a href="#" target="_blank">
                Yeap Soluções Tecnológicas
              </a>
            </p>
          </div>
        </div>
      </div>
    </S.Container>
  )
}
export default Footer
