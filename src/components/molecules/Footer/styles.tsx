import styled from 'styled-components'

import FacebookIcon from '../../../../public/images/facebook.svg'
import InstagramIcon from '../../../../public/images/instagram.svg'
import TwitterIcon from '../../../../public/images/twitter.svg'
import YoutubeIcon from '../../../../public/images/youtube.svg'

interface StyledFooterProps {
  primaryColor: string
}

export const Container = styled.footer<StyledFooterProps>`
  display: flex;
  flex-direction: column;

  height: auto;
  width: 100%;
  padding: 45px 250px;

  background-color: ${(props) => props.primaryColor};

  .row-top {
    display: flex;
    justify-content: space-between;
    align-items: center;

    width: 100%;
    height: 100%;
    margin-bottom: 90px;
    .logo {
      width: 50%;
      @media (max-width: 768px) {
        width: 100%;
        padding-bottom: 50px;
      }
    }
    .social-media {
      display: flex;
      flex-direction: column;
      align-items: flex-end;

      width: 50%;
      h1 {
        padding-bottom: 24px;

        color: #fff;

        font-size: 16px;
        text-transform: uppercase;
      }
      .social-icons {
        display: flex;
        .icon-facebook {
          width: 38px;
          height: 38px;
          margin-right: 30px;

          mask-image: url(${FacebookIcon});
          mask-size: 38px;
          -webkit-mask-image: url(${FacebookIcon});
          mask-repeat: no-repeat;
          background-color: #fff;
        }
        .icon-instagram {
          width: 38px;
          height: 38px;
          margin-right: 30px;

          mask-image: url(${InstagramIcon});
          mask-size: 38px;
          -webkit-mask-image: url(${InstagramIcon});
          mask-repeat: no-repeat;
          background-color: #fff;
        }
        .icon-twitter {
          width: 38px;
          height: 38px;
          margin-right: 30px;

          mask-image: url(${TwitterIcon});
          mask-size: 38px;
          -webkit-mask-image: url(${TwitterIcon});
          mask-repeat: no-repeat;
          background-color: #fff;
        }
        .icon-youtube {
          width: 38px;
          height: 38px;

          mask-image: url(${YoutubeIcon});
          mask-size: 38px;
          -webkit-mask-image: url(${YoutubeIcon});
          mask-repeat: no-repeat;
          background-color: #fff;
        }
      }
      @media (max-width: 768px) {
        align-items: center;

        width: 100%;
      }
    }
    @media (max-width: 768px) {
      flex-direction: column;

      width: 100%;
    }
  }
  .row-bottom {
    display: flex;
    justify-content: space-between;
    align-items: flex-end;

    width: 100%;
    height: 180px;

    .footer-menu {
      display: flex;

      width: 50%;
      .footer-menu-left {
        display: flex;
        flex-direction: column;

        width: 100%;
        h1 {
          padding-bottom: 24px;

          color: #fff;

          font-size: 16px;
          text-transform: uppercase;
        }
        a {
          padding-bottom: 16px;

          color: #fff;

          font-size: 16px;
          text-decoration: none;
        }
        @media (max-width: 768px) {
          align-items: center;

          padding-bottom: 48px;
        }
      }
      .footer-menu-right {
        display: flex;
        flex-direction: column;

        width: 100%;
        h1 {
          padding-bottom: 24px;

          color: #fff;

          font-size: 16px;
          text-transform: uppercase;
        }

        a {
          padding-bottom: 16px;

          color: #fff;

          font-size: 16px;
          text-decoration: none;
        }
        @media (max-width: 768px) {
          align-items: center;

          padding-bottom: 48px;
        }
      }
      @media (max-width: 768px) {
        flex-direction: column;

        width: 100%;
      }
    }

    .bottom-right {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      align-items: flex-end;

      width: 50%;
      height: 100%;
      .privacy-policy {
        a {
          color: #fff;

          font-size: 16px;
          text-decoration: none;
        }
        @media (max-width: 768px) {
          padding-bottom: 35px;
        }
      }
      .copy {
        display: flex;
        flex-direction: column;
        align-items: flex-end;

        p {
          padding-bottom: 12px;

          color: #fff;

          font-size: 16px;
          font-weight: 600;

          span {
            font-weight: normal;
          }

          a {
            color: #fff;

            font-size: 16px;
            text-decoration: none;
          }
          @media (max-width: 768px) {
            padding-bottom: 15px;

            text-align: center;
          }
        }
        @media (max-width: 768px) {
          align-items: center;
        }
      }
      @media (max-width: 768px) {
        align-items: center;

        width: 100%;
      }
    }
    @media (max-width: 768px) {
      flex-direction: column;

      width: 100%;
    }
    @media (max-width: 768px) {
      height: auto;
    }
  }
  @media (max-width: 768px) {
    padding: 30px 100px;
  }
`
