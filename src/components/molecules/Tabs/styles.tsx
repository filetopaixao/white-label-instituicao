import styled from 'styled-components'
import { Button as Bt } from 'components/atoms/Button/styles'

interface TabsProps {
  active?: boolean
  primaryColor?: string
}
interface TabsPropsQtd {
  qtd: number
}

export const Container = styled.div``

export const GridButtons = styled.div<TabsPropsQtd>`
  @media (min-width: 992px) {
    display: grid;

    width: 100%;

    grid-template-columns: ${(props) => {
      const width = 100 / props.qtd
      let grid = ''
      for (let i = 0; i < props.qtd; i += 1) {
        grid += `${width}% `
      }
      return `${grid}`
    }};
  }
`

export const Content = styled.main``

export const ContentCointainer = styled.main`
  border: 1px solid #eaeaea;

  background: #ffffff;
  border-radius: 20px;

  box-shadow: 0px 2px 10px #00000029;
`

export const Column = styled.div`
  padding: 10px;
`

export const Button = styled(Bt)<TabsProps>`
  width: 100%;

  border-radius: 30px;

  box-shadow: 0px 2px 10px #00000029;

  font-size: 13px;

  background: #fff;

  color: #5e5e5e;

  ${(props) => {
    if (props.active) {
      return `background: ${props.primaryColor}; color: #fff;`
    }
  }}
`

export const Welcome = styled.div`
  margin: 10px 0px;
  @media (min-width: 992px) {
    margin: 70px 0px;
    width: 100%;
  }
`
