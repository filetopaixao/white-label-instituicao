import React, {
  useState,
  useEffect,
  useCallback,
  ReactNode,
  useContext
} from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'

import * as S from './styles'

interface TabsProps {
  content1: ReactNode
  content2: ReactNode
  content3: ReactNode
  content4: ReactNode
  buttonTxt1: string
  buttonTxt2: string
  buttonTxt3: string
  buttonTxt4: string
}

const Tab: React.FC<TabsProps> = ({
  content1,
  content2,
  content3,
  content4,
  buttonTxt1,
  buttonTxt2,
  buttonTxt3,
  buttonTxt4
}) => {
  const { primaryColor } = useContext(ThemeContext)
  const [activeBtn1, setActiveBtn1] = useState(true)
  const [activeBtn2, setActiveBtn2] = useState(false)
  const [activeBtn3, setActiveBtn3] = useState(false)
  const [activeBtn4, setActiveBtn4] = useState(false)
  const [content, setContent] = useState(content1)

  const handleContent1 = useCallback(() => {
    setContent(content1)
    setActiveBtn1(true)
    setActiveBtn2(false)
    setActiveBtn3(false)
    setActiveBtn4(false)
  }, [content1])

  const handleContent2 = useCallback(() => {
    setContent(content2)
    setActiveBtn1(false)
    setActiveBtn2(true)
    setActiveBtn3(false)
    setActiveBtn4(false)
  }, [content2])

  const handleContent3 = useCallback(() => {
    setContent(content3)
    setActiveBtn1(false)
    setActiveBtn2(false)
    setActiveBtn3(true)
    setActiveBtn4(false)
  }, [content3])

  const handleContent4 = useCallback(() => {
    setContent(content4)
    setActiveBtn1(false)
    setActiveBtn2(false)
    setActiveBtn3(false)
    setActiveBtn4(true)
  }, [content4])

  const clickButton = (e: any) => {
    if (e.target.id === 'btn1') {
      if (activeBtn1) {
        setActiveBtn1(false)
      } else {
        handleContent1()
      }
    } else if (e.target.id === 'btn2') {
      if (activeBtn2) {
        setActiveBtn2(false)
      } else {
        handleContent2()
      }
    } else if (e.target.id === 'btn3') {
      if (activeBtn3) {
        setActiveBtn3(false)
      } else {
        handleContent3()
      }
    } else if (e.target.id === 'btn4') {
      if (activeBtn4) {
        setActiveBtn4(false)
      } else {
        handleContent4()
      }
    }
  }

  return (
    <S.Container>
      <S.GridButtons qtd={3}>
        <S.Column>
          <S.Button
            backgroundColor=""
            textColor=""
            onClick={clickButton}
            id="btn1"
            active={activeBtn1}
            primaryColor={primaryColor}
          >
            {buttonTxt1}
          </S.Button>
        </S.Column>
        <S.Column>
          <S.Button
            backgroundColor=""
            textColor=""
            onClick={clickButton}
            id="btn2"
            active={activeBtn2}
            primaryColor={primaryColor}
          >
            {buttonTxt2}
          </S.Button>
        </S.Column>
        <S.Column>
          <S.Button
            backgroundColor=""
            textColor=""
            onClick={clickButton}
            id="btn3"
            active={activeBtn3}
            primaryColor={primaryColor}
          >
            {buttonTxt3}
          </S.Button>
        </S.Column>
        {/* <S.Column>
          <S.Button
            backgroundColor=""
            textColor=""
            onClick={clickButton}
            id="btn4"
            active={activeBtn4}
            primaryColor={primaryColor}
          >
            {buttonTxt4}
          </S.Button>
        </S.Column> */}
      </S.GridButtons>
      <S.ContentCointainer>
        <S.Content>{content}</S.Content>
      </S.ContentCointainer>
    </S.Container>
  )
}

export default Tab
