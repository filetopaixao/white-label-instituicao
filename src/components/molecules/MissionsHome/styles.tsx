import styled from 'styled-components'

import MedicinesIcon from '../../../../public/images/medicines.svg'
import DegreeIcon from '../../../../public/images/degree.svg'
import EquipmentsIcon from '../../../../public/images/equipments.svg'
import InstallationsIcon from '../../../../public/images/installations.svg'

export const Container = styled.section`
  display: flex;
  flex: 1;

  min-height: 800px;
  .mission-title {
    padding-bottom: 10px;

    color: #5e5e5e;

    font-size: 50px;
    text-align: center;
  }
  .mission-subtitle {
    padding-bottom: 64px;

    color: #5e5e5e;

    font-size: 24px;
    text-align: center;
  }

  .missions-container {
    display: flex;

    padding: 0 30px;

    .medicine {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;

      padding: 0 30px;

      text-align: center;

      .medicine-icon {
        width: 100px;
        height: 100px;
        margin-bottom: 40px;

        mask-image: url(${MedicinesIcon});
        mask-size: 100px;
        -webkit-mask-image: url(${MedicinesIcon});
        mask-repeat: no-repeat;
        background-color: #5e5e5e;
        @media (max-width: 768px) {
          margin-bottom: 20px;
        }
      }

      h2 {
        margin-bottom: 12px;

        color: #5e5e5e;
      }

      p {
        color: #5e5e5e;

        font-size: 17px;
      }
    }

    .degree {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;

      padding: 0 30px;

      text-align: center;

      .degree-icon {
        width: 100px;
        height: 100px;
        margin-bottom: 40px;

        mask-image: url(${DegreeIcon});
        mask-size: 100px;
        -webkit-mask-image: url(${DegreeIcon});
        mask-repeat: no-repeat;
        background-color: #5e5e5e;
        @media (max-width: 768px) {
          margin-bottom: 20px;
        }
      }

      h2 {
        margin-bottom: 12px;

        color: #5e5e5e;
      }

      p {
        color: #5e5e5e;

        font-size: 17px;
      }
    }

    .equipment {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;

      padding: 0 30px;

      text-align: center;

      .equipment-icon {
        width: 100px;
        height: 100px;
        margin-bottom: 40px;

        mask-image: url(${EquipmentsIcon});
        mask-size: 100px;
        -webkit-mask-image: url(${EquipmentsIcon});
        mask-repeat: no-repeat;
        background-color: #5e5e5e;
        @media (max-width: 768px) {
          margin-bottom: 20px;
        }
      }

      h2 {
        margin-bottom: 12px;

        color: #5e5e5e;
      }

      p {
        color: #5e5e5e;

        font-size: 17px;
      }
    }

    .installation {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;

      padding: 0 30px;

      text-align: center;

      .installation-icon {
        width: 100px;
        height: 100px;
        margin-bottom: 40px;

        mask-image: url(${InstallationsIcon});
        mask-size: 100px;
        -webkit-mask-image: url(${InstallationsIcon});
        mask-repeat: no-repeat;
        background-color: #5e5e5e;
        @media (max-width: 768px) {
          margin-bottom: 20px;
        }
      }

      h2 {
        margin-bottom: 12px;

        color: #5e5e5e;
      }

      p {
        color: #5e5e5e;

        font-size: 17px;
      }
    }
    @media (max-width: 768px) {
      flex-direction: column;

      h2 {
        margin-bottom: 10px;
      }

      p {
        margin-bottom: 40px;
      }
    }
  }
  @media (max-width: 768px) {
  }
`
