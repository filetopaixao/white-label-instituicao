import HeartSidebarSection from 'components/atoms/HeartSidebarSection'
import HeartIcon from 'components/atoms/HeartIcon'

import * as S from './styles'

const MissionsHome: React.FC = () => (
  <S.Container>
    <HeartSidebarSection>
      <HeartIcon />
      <h1 className="mission-title">Nossa missão</h1>
      <h2 className="mission-subtitle">
        Garantir que a filosofia do amor seja maior do que...
      </h2>
      <div className="missions-container">
        <div className="medicine">
          <div className="medicine-icon" />
          <h2>Apoio</h2>
          <p>Temos o comprometimento com a sociedade de oferecer os melhores</p>
        </div>
        <div className="degree">
          <div className="degree-icon" />
          <h2>Proteção</h2>
          <p>Nossos médicos trabalham de forma exclusiva com todo amor</p>
        </div>
        <div className="equipment">
          <div className="equipment-icon" />
          <h2>Transformação</h2>
          <p>Utilizamos equipamentos de ponta em todos os pacientes</p>
        </div>
        <div className="installation">
          <div className="installation-icon" />
          <h2>Realização</h2>
          <p>Pensamos em cada detalhe para se sentirem acolhidos</p>
        </div>
      </div>
    </HeartSidebarSection>
  </S.Container>
)

export default MissionsHome
