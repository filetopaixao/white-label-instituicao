import styled from 'styled-components'

import GoalIcon from '../../../../public/images/goal.svg'
import AmountIcon from '../../../../public/images/amount.svg'

interface StyledCardCampaignProps {
  image?: string
  campaign_id?: number | undefined
  clickedDropDown?: boolean
  primaryColor?: string
  status?: string
}

export const CardContainer = styled.div<StyledCardCampaignProps>`
  width: 370px;
  padding: 40px;
  a {
    cursor: pointer;
  }
  @media (max-width: 768px) {
    padding: 40px 5pt;
    width: auto;
  }
`
export const CardCampaign = styled.div<StyledCardCampaignProps>`
  width: 100%;

  box-shadow: 0px 3px 10px #00000029;

  background: ${props => props.status == 'inactive' || props.status == 'ended' ? '#F8F8F8' : ''};
`
export const ImageCampaign = styled.div<StyledCardCampaignProps>`
  width: 100%;
  height: 320px;

  background: ${(props) => `url(${props.image});`}
  background-position: center center;
  background-size: cover;
  background-repeat: no-repeat;
  ${props => props.status == 'inactive' || props.status == 'ended' ? `filter: grayscale(100%)` : ``};
  @media (max-width: 768px) {
    height: 235pt;
  }
`
export const InfoCampaign = styled.div`
  padding: 32px;
  h1 {
    padding-bottom: 15px;

    color: #5e5e5e;

    font-size: 24px;
    text-transform: capitalize;
  }
  p {
    color: #acacac;

    font-size: 16px;
  }

  button {
    margin-top: 55px;
  }

  @media (max-width: 768px) {
    padding: 20pt;
  }
`
export const GoalBarContainer = styled.div`
  padding-top: 60px;
  padding-bottom: 45px;
  @media (max-width: 768px) {
    padding-top: 21pt;
    padding-bottom: 21pt;
  }
`

export const DropDownCard = styled.div<StyledCardCampaignProps>`
  position: relative;
  cursor: pointer;

  display: flex;
  justify-content: center;
  align-items: center;

  width: 34px;
  height: 34px;

  padding-bottom: 5px;

  ${props => props.clickedDropDown && `
    background-color: #E3F6F1;
    border-radius: 50%;
  `}
    
`

export const DropDownCardContent = styled.div<StyledCardCampaignProps>`
  display: ${props => props.clickedDropDown ? 'block' : 'none'};
  position: absolute;
  top: 95px;
  right: 0;

  padding: 20px;

  
  background: #fff;
  box-shadow: 0px 3px 10px #00000029;
  z-index: 998;
  border-radius: 5px;

  a{
    display: flex;
    color: #5E5E5E;
    font-weight: 600;

    img{
      margin-left: 35px;
    }
  }
`


export const Goal = styled.div`
  display: flex;
  align-items: center;

  margin-bottom: 20px;
  div {
    width: 25px;
    height: 25px;
    margin-right: 20px;

    mask-image: url(${GoalIcon});
    mask-size: 25px;
    -webkit-mask-image: url(${GoalIcon});
    mask-repeat: no-repeat;
    background-color: #5e5e5e;
  }
  span {
    color: #5e5e5e;

    font-size: 16px;
    span {
      font-weight: 600;
    }
    @media (max-width: 768px) {
      display: block;
    }
  }
`

export const Amount = styled.div`
  display: flex;
  align-items: center;
  div {
    width: 25px;
    height: 25px;
    margin-right: 20px;

    mask-image: url(${AmountIcon});
    mask-size: 25px;
    -webkit-mask-image: url(${AmountIcon});
    mask-repeat: no-repeat;
    background-color: #5e5e5e;
  }
  span {
    color: #5e5e5e;

    font-size: 16px;
    span {
      font-weight: 600;
    }
    @media (max-width: 768px) {
      display: block;
    }
  }
  @media (max-width: 768px) {
    margin-bottom: 30pt;
  }
`
export const ContainerEdit = styled.div<StyledCardCampaignProps>`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;

  padding: 30px;

  border-top: 1px solid #DCDCDC;

  color: ${props => props.primaryColor};

  font-weight: 600;

  .btn-edit{
    display: flex;

    a{
      margin-right: 20px;
    }
  }
`
