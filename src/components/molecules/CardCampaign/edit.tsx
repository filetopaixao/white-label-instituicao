import { useContext, useState } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'
import { useRouter } from 'next/router'
import GoalBar from 'components/atoms/GoalBar'
import ModalConfirm from 'components/atoms/Modal/Confirm/confirm'
import { inactiveCampaign, activeCampaign, deleteCampaign } from '../../../services/user'
import TrashIcon from '../../../../public/images/trash.svg'

import * as S from './styles'

interface CardCampaignProps {
  id?: number | undefined
  image: string
  name: string
  description: string
  goal_value: string
  amount_collected: string
  status: string
  token: string
}

const CardCampaign: React.FC<CardCampaignProps> = ({
  id,
  image,
  name,
  description,
  goal_value,
  amount_collected,
  status,
  token
}) => {
  const [clickedDropDown, setClickedDropDown] = useState(false)
  const [isVisibleModalConfirmInactive, setIsVisibleModalConfirmInactive] = useState(false)
  const [isVisibleModalConfirmActive, setIsVisibleModalConfirmActive] = useState(false)
  const [isVisibleModalConfirmDelete, setIsVisibleModalConfirmDelete] = useState(false)
  const { primaryColor } = useContext(ThemeContext)
  const router = useRouter()

  const handleInactive = async () => {
    await inactiveCampaign(id, token)
    setIsVisibleModalConfirmInactive(!isVisibleModalConfirmInactive)
    router.push('/minhas-campanhas/1')
  }

  const handleActive = async () => {
    await activeCampaign(id, token)
    setIsVisibleModalConfirmActive(!isVisibleModalConfirmActive)
    router.push('/minhas-campanhas/1')
  }

  const handleDelete = async () => {
    await deleteCampaign(id, token)
    setIsVisibleModalConfirmDelete(!isVisibleModalConfirmDelete)
    router.push('/minhas-campanhas/1')
  }

  return (
    <S.CardContainer campaign_id={id}>
      <S.CardCampaign status={status}>
        <S.ImageCampaign image={image} status={status} />
        <S.InfoCampaign>
          <h1>{name}</h1>
          <p>
            {description && description.length > 80
              ? description.substr(0, 79) + '...'
              : description}
          </p>
          <S.GoalBarContainer>
            <GoalBar
              gloalValue={parseFloat(goal_value)}
              amountCollected={parseFloat(amount_collected)}
              status={status}
            />
          </S.GoalBarContainer>
          <S.Goal>
            <div />
            <span>
              Meta:{' '}
              <span>
                {parseFloat(goal_value).toLocaleString('pt-br', {
                  style: 'currency',
                  currency: 'BRL'
                })}
              </span>
            </span>
          </S.Goal>
          <S.Amount>
            <div />
            <span>
              Arrecadado:{' '}
              <span>
                {parseFloat(amount_collected).toLocaleString('pt-br', {
                  style: 'currency',
                  currency: 'BRL'
                })}
              </span>
            </span>
          </S.Amount>
        </S.InfoCampaign>
        <S.ContainerEdit primaryColor={primaryColor}>
          <div className="btn-edit">
            <a onClick={() =>
              router.push(
                `/minhas-campanhas/editar/${id}`
              )
            }>EDITAR</a>
            {
              status == 'active' ? (
                <a onClick={() => setIsVisibleModalConfirmInactive(!isVisibleModalConfirmInactive)}>DESATIVAR</a>
              ) : (
                <a onClick={() => setIsVisibleModalConfirmActive(!isVisibleModalConfirmActive)}>ATIVAR</a>
              )
            }
          </div>
          <S.DropDownCard clickedDropDown={clickedDropDown} onClick={() => setClickedDropDown(!clickedDropDown)}>
            <div>...</div>
          </S.DropDownCard>
          <S.DropDownCardContent clickedDropDown={clickedDropDown}>
            <div><a onClick={() => setIsVisibleModalConfirmDelete(!isVisibleModalConfirmDelete)}>Excluir Campanha <img src={TrashIcon} /></a></div>
          </S.DropDownCardContent>
        </S.ContainerEdit>
      </S.CardCampaign>
      <ModalConfirm
        title="Tem certeza que deseja desativar?"
        message="Ao desativar campanha ela ficará indisponível para doações"
        onClose={() => setIsVisibleModalConfirmInactive(!isVisibleModalConfirmInactive)}
        handleConfirm={() => handleInactive()}
        btnCloser={false}
        variant="sm"
        id="modal-confirm-inactive-campaign"
        isVisible={isVisibleModalConfirmInactive}
      />
      <ModalConfirm
        title="Tem certeza que deseja ativar?"
        message=""
        onClose={() => setIsVisibleModalConfirmActive(!isVisibleModalConfirmActive)}
        handleConfirm={() => handleActive()}
        btnCloser={false}
        variant="sm"
        id="modal-confirm-inactive-campaign"
        isVisible={isVisibleModalConfirmActive}
      />
      <ModalConfirm
        title="Tem certeza que deseja excluir?"
        message="Ao excluir os dados dessa campanha não aparecerão na sua conta"
        onClose={() => setIsVisibleModalConfirmDelete(!isVisibleModalConfirmDelete)}
        handleConfirm={() => handleDelete()}
        btnCloser={false}
        variant="sm"
        id="modal-confirm-inactive-campaign"
        isVisible={isVisibleModalConfirmDelete}
      />
    </S.CardContainer>
  )
}

export default CardCampaign
