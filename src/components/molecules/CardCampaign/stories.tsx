import { Story, Meta } from '@storybook/react/types-6-0'
import CardCampaign from '.'

export default {
  title: 'CardCampaign',
  component: CardCampaign
} as Meta

export const Basic: Story = () => (
  <CardCampaign
    key={1}
    image="teste"
    name="name"
    description="description"
    goal_value="500"
    amount_collected="250"
    id={1}
  />
)
