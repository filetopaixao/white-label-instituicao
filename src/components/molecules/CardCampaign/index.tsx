import { useContext } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'
import { useRouter } from 'next/router'
import GoalBar from 'components/atoms/GoalBar'
import Button from 'components/atoms/Button'

import * as S from './styles'

interface CardCampaignProps {
  id?: number | undefined
  image: string
  name: string
  description: string
  goal_value: string
  amount_collected: string
}

const CardCampaign: React.FC<CardCampaignProps> = ({
  id,
  image,
  name,
  description,
  goal_value,
  amount_collected
}) => {
  const { secondaryColor } = useContext(ThemeContext)
  const router = useRouter()
  return (
    <S.CardContainer campaign_id={id}>
      <a
        onClick={() =>
          router.push(
            `/campanha/${name.replace(/ /g, '-').toLowerCase()}/${id}`
          )
        }
      >
        <S.CardCampaign>
          <S.ImageCampaign image={image} />
          <S.InfoCampaign>
            <h1>{name}</h1>
            <p>
              {description && description.length > 80
                ? description.substr(0, 79) + '...'
                : description}
            </p>
            <S.GoalBarContainer>
              <GoalBar
                gloalValue={parseFloat(goal_value)}
                amountCollected={parseFloat(amount_collected)}
              />
            </S.GoalBarContainer>
            <S.Goal>
              <div />
              <span>
                Meta:{' '}
                <span>
                  {parseFloat(goal_value).toLocaleString('pt-br', {
                    style: 'currency',
                    currency: 'BRL'
                  })}
                </span>
              </span>
            </S.Goal>
            <S.Amount>
              <div />
              <span>
                Arrecadado:{' '}
                <span>
                  {parseFloat(amount_collected).toLocaleString('pt-br', {
                    style: 'currency',
                    currency: 'BRL'
                  })}
                </span>
              </span>
            </S.Amount>
            <Button color={secondaryColor} onClick={() => false}>
              doar
            </Button>
          </S.InfoCampaign>
        </S.CardCampaign>
      </a>
    </S.CardContainer>
  )
}

export default CardCampaign
