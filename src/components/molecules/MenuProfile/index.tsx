import { useContext } from 'react'
import { MenuContext } from '../../../contexts/MenuContext'
import ItemProfile from 'components/atoms/ItemProfile'
import cookie from 'js-cookie'

import * as S from './styles'

interface MenuProps {
  isMobile: boolean
  showMenu: boolean
  setShowMenu?: any
}

const MenuProfile: React.FC<MenuProps> = ({
  isMobile,
  showMenu,
  setShowMenu
}) => {
  const { menuUser } = useContext(MenuContext)
  return (
    <>
      <S.Container>
        {menuUser.map((item, key) => {
          if (item.itemName === 'Login' && cookie.get('loggedUser')) return
          return (
            <ItemProfile
              key={key}
              itemLink={item.itemLink}
              itemName={item.itemName}
              isMobile={isMobile}
              setShowMenu={setShowMenu}
            />
          )
        })}
      </S.Container>
      <S.ContainerMobile showMenu={showMenu}>
        {menuUser.map((item, key) => {
          return (
            <ItemProfile
              key={key}
              itemLink={item.itemLink}
              itemName={item.itemName}
              isMobile={isMobile}
              setShowMenu={setShowMenu}
            />
          )
        })}
      </S.ContainerMobile>
    </>
  )
}

export default MenuProfile
