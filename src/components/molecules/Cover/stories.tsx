import { Story, Meta } from '@storybook/react/types-6-0'
import Cover from '.'

export default {
  title: 'Cover',
  component: Cover
} as Meta

export const Basic: Story = () => <Cover />
