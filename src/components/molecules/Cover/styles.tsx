import styled from 'styled-components'

interface StyledCoverProps {
  coverImage: string
  primaryColor: string
  tertiaryColor: string
}

export const Cover = styled.section<StyledCoverProps>`
  display: flex;
  justify-content: flex-start;
  align-items: center;

  min-height: 969px;
  padding-left: 8%;

  color: #ffffff;
  background-image: ${(props) =>
      `linear-gradient(90deg, ${props.primaryColor} 17%, ${props.tertiaryColor}63 100%)`},
    ${(props) => `url(${props.coverImage})`};
  background-size: cover;
  background-repeat: no-repeat;
  background-position-x: center;

  @media (max-width: 768px) {
    padding: 8%;

    background-position: center;
  }
`
export const Container = styled.div`
  display: flex;
  flex-direction: column;

  width: 33%;
  @media (max-width: 768px) {
    width: 100%;
  }
`

export const TitleSmall = styled.h2`
  font-size: 20px;
  font-weight: 700;
`
export const Title = styled.h2`
  margin: 10px 0px 40px 0px;

  font-size: 50px;
  font-weight: 700;
  @media (max-width: 768px) {
    font-size: 40px;
  }
`
export const Form = styled.div`
  .input-email {
    margin-bottom: 25px;
  }
`

export const TwoInputs = styled.div`
  display: flex;
  flex-direction: row;

  margin-bottom: 15px;

  .form-erro {
    width: 100%;
    padding-left: 0 !important;
  }

  div {
    width: 50%;
  }

  div:first-child {
    padding-right: 10px;
  }
  div:last-child {
    padding-left: 10px;
  }

  @media (max-width: 768px) {
    div:first-child {
      padding-right: 5px;
    }
    div:last-child {
      padding-left: 5px;
    }
  }
`
