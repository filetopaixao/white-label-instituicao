import { useContext } from 'react'
import { useRouter } from 'next/router'
import { ThemeContext } from '../../../contexts/ThemeContext'
import { PagesContext } from '../../../contexts/PagesContext'
import Input from 'components/atoms/Input'
import Button from 'components/atoms/Button'
import { Formik } from 'formik'
import * as Yup from 'yup'

import * as S from './styles'

const Schema = Yup.object().shape({
  name: Yup.string().required('Campo obrigatório'),
  lastname: Yup.string().required('Campo obrigatório'),
  email: Yup.string().required('Campo obrigatório')
})

const Cover: React.FC = () => {
  const {
    coverImage,
    primaryColor,
    secondaryColor,
    tertiaryColor
  } = useContext(ThemeContext)

  const { setNameQuery, setLastnameQuery, setEmailQuery } = useContext(PagesContext)
  const router = useRouter()

  return (
    <S.Cover
      coverImage={coverImage}
      primaryColor={primaryColor}
      tertiaryColor={tertiaryColor}
    >
      <S.Container>
        <S.TitleSmall>
          Nunca foi tão fácil nos ajudar a salvar vidas!
        </S.TitleSmall>
        <S.Title>Crie sua própria campanha</S.Title>
        <Formik
          initialValues={{
            name: '',
            lastname: '',
            email: ''
          }}
          validationSchema={Schema}
          onSubmit={(values) => {
            // same shape as initial values
            setNameQuery(values.name)
            setLastnameQuery(values.lastname)
            setEmailQuery(values.email)
            router.push('/sign_up')
          }}
        >
          {({ errors, handleChange, handleSubmit, values }) => (
            <S.Form>
              <S.TwoInputs>
                <div>
                  <Input
                    placeholder="Nome"
                    value={values.name}
                    onChange={handleChange}
                    name="name"
                  />
                  {errors.name && (
                    <div className="form-erro">{errors.name}</div>
                  )}
                </div>
                <div>
                  <Input
                    placeholder="Sobrenome"
                    value={values.lastname}
                    onChange={handleChange}
                    name="lastname"
                  />
                  {errors.lastname && (
                    <div className="form-erro">{errors.lastname}</div>
                  )}
                </div>
              </S.TwoInputs>
              <div className="input-email">
                <Input
                  placeholder="Qual seu e-mail?"
                  value={values.email}
                  onChange={handleChange}
                  name="email"
                  type="email"
                />
                {errors.email && (
                  <div className="form-erro">{errors.email}</div>
                )}
              </div>
              <Button
                type="button"
                color={secondaryColor}
                onClick={handleSubmit}
              >
                criar minha campanha
              </Button>
            </S.Form>
          )}
        </Formik>
      </S.Container>
    </S.Cover>
  )
}

export default Cover
