import styled from 'styled-components'

import Background from '../../../../public/images/detail-background.png'
import TreatmentIcon from '../../../../public/images/treatment.svg'
import SnackIcon from '../../../../public/images/snack.svg'
import StayIcon from '../../../../public/images/stay.svg'
import WishesIcon from '../../../../public/images/wishes.svg'

interface StyledDetailHomePorps {
  secondaryColor?: string
  percent?: number
}

export const Container = styled.section<StyledDetailHomePorps>`
  display: flex;
  justify-content: center;
  align-items: center;

  min-height: 650px;

  background-image: ${(props) =>
      `linear-gradient(${props.secondaryColor}b3, ${props.secondaryColor}b3)`},
    url(${Background});
  background-size: cover;
  background-repeat: no-repeat;

  .detail {
    display: flex;
    align-content: flex-end;
    flex: 5;

    padding: 60px;
    padding-left: 200px;

    .detail-title {
      padding-bottom: 30px;

      color: #fff;

      font-size: 50px;
    }

    .detail-subtitle {
      padding-bottom: 48px;

      color: #fff;

      font-size: 24px;
    }

    .offer-list li {
      padding-bottom: 20px;

      color: #fff;

      font-size: 17px;
    }
    @media (max-width: 768px) {
      padding: 60px 35px;
    }
  }

  .offer {
    display: flex;
    justify-content: flex-end;
    flex: 5;

    padding: 60px;
    padding-right: 200px;

    div {
      display: flex;
      flex: 1;
      justify-content: space-between;
      align-items: flex-end;
      .offer-list {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: flex-end;

        //min-height: 100%;

        .offer-percent {
          margin-bottom: 12px;

          color: #fff;

          font-size: 16px;
          font-weight: 600;
        }

        .treatment-icon {
          width: 73px;
          min-height: 73px;
          margin-top: 16px;

          mask-image: url(${TreatmentIcon});
          mask-size: 73px;
          -webkit-mask-image: url(${TreatmentIcon});
          mask-repeat: no-repeat;
          background-color: #fff;
        }

        .snack-icon {
          width: 73px;
          min-height: 73px;
          margin-top: 16px;

          mask-image: url(${SnackIcon});
          mask-size: 73px;
          -webkit-mask-image: url(${SnackIcon});
          mask-repeat: no-repeat;
          background-color: #fff;
        }

        .stay-icon {
          width: 73px;
          min-height: 73px;
          margin-top: 16px;

          mask-image: url(${StayIcon});
          mask-size: 73px;
          -webkit-mask-image: url(${StayIcon});
          mask-repeat: no-repeat;
          background-color: #fff;
        }

        .wishes-icon {
          width: 73px;
          min-height: 73px;
          margin-top: 16px;

          mask-image: url(${WishesIcon});
          mask-size: 73px;
          -webkit-mask-image: url(${WishesIcon});
          mask-repeat: no-repeat;
          background-color: #fff;
        }
      }
    }
    @media (max-width: 768px) {
      width: 100%;
      padding: 30px 35px 60px 35px;
    }
  }
  @media (max-width: 768px) {
    flex-direction: column;
  }
`
export const OfferName = styled.div<StyledDetailHomePorps>`
  display: flex !important;
  align-items: center !important;

  width: 60px;
  min-height: ${(props) => `calc(120px + ${props.percent}%)`};
  padding-top: 2rem;
  margin-bottom: 1rem;

  background-color: white;
  color: ${(props) => props.secondaryColor};

  font-weight: bold;
  text-transform: uppercase;

  -webkit-writing-mode: vertical-lr;
  transform: scale(-1, -1);
`
