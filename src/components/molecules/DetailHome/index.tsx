import { useContext } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'

import * as S from './styles'

const DetailHome: React.FC = () => {
  const { secondaryColor } = useContext(ThemeContext)
  return (
    <S.Container secondaryColor={secondaryColor}>
      <div className="detail">
        <div>
          <h1 className="detail-title">Sua ajuda faz a diferença!</h1>
          <h2 className="detail-subtitle">
            Coisas que não abrimos mão de oferecer para nossos pacientes e
            familiares:
          </h2>
          <div className="offer-list">
            <li>Atendimento humanizado</li>
            <li>Refeições para todos sem exceções</li>
            <li>Dignidade na estadia das famílias</li>
            <li>Realização dos desejos de pacientes paliativos</li>
          </div>
        </div>
      </div>
      <div className="offer">
        <div>
          <div className="offer-list">
            <div className="offer-percent">32%</div>
            <S.OfferName secondaryColor={secondaryColor} percent={32}>
              ação de amor
            </S.OfferName>
            <div className="treatment-icon" />
          </div>
          <div className="offer-list">
            <div className="offer-percent">24%</div>
            <S.OfferName secondaryColor={secondaryColor} percent={24}>
              comemoração
            </S.OfferName>
            <div className="snack-icon" />
          </div>
          <div className="offer-list">
            <div className="offer-percent">17%</div>
            <S.OfferName secondaryColor={secondaryColor} percent={17}>
              empresa do bem
            </S.OfferName>
            <div className="stay-icon" />
          </div>
          <div className="offer-list">
            <div className="offer-percent">13%</div>
            <S.OfferName secondaryColor={secondaryColor} percent={13}>
              esportes
            </S.OfferName>
            <div className="wishes-icon" />
          </div>
        </div>
      </div>
    </S.Container>
  )
}
export default DetailHome
