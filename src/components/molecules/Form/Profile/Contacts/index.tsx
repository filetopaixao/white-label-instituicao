import { useContext, useState } from 'react'
import { ThemeContext } from '../../../../../contexts/ThemeContext'
import { useRouter } from 'next/router'
import { Formik, Field } from 'formik'
import Image from 'next/image'
import Label from 'components/atoms/Label'
import Button from 'components/atoms/Button'
import Schema from './schema'
import axios from 'axios'

//import cookie from 'cookie-cutter'

import * as S from './styles'

interface ContactsFormProps {
  loggedUser: any
}

const ContactsForm: React.FC<ContactsFormProps> = ({ loggedUser }) => {
  const { primaryColor, secondaryColor } = useContext(ThemeContext)
  const router = useRouter()
  const [isLoading, setIsLoading] = useState(false)
  const [errorSignup, setErrorSignup] = useState('')

  return (
    <>
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={{
          email: loggedUser ? loggedUser.email : '',
          phone: loggedUser ? loggedUser.phone : '',
          whatsapp: loggedUser ? loggedUser.whatsapp : '',
          facebook: loggedUser ? loggedUser.facebook : '',
          instagram: loggedUser ? loggedUser.instagram : '',
          youtube: loggedUser ? loggedUser.youtube : ''
        }}
        validationSchema={Schema}
        onSubmit={async (values) => {
          //setIsLoading(true)
          setErrorSignup('')
          console.log(values)

          //const { firstName, lastName, cpf, birthDate, gender } = await values
        }}
      >
        {({ errors, handleChange, handleSubmit, values, setFieldValue }) => {
          return (
            <>
              <S.Form isLoadingLogin={isLoading}>
                <S.Inputs primaryColor={primaryColor}>
                  <Label>e-mail</Label>
                  <S.IptMask
                    mask=""
                    type="email"
                    value={values.email}
                    name="email"
                    onChange={handleChange}
                    disabled
                  />
                  {errors.email ? (
                    <div className="form-erro">{errors.email}</div>
                  ) : null}
                  <Label>telefone (whatsapp)</Label>
                  <S.IptMask
                    mask="(99) 9999-99999"
                    name="phone"
                    value={values.phone}
                    onChange={handleChange}
                  />
                  {errors.phone ? (
                    <div className="form-erro">{errors.phone}</div>
                  ) : null}
                  <Label>whatsapp:</Label>
                  <S.IptMask
                    mask="(99) 9999-99999"
                    name="whatsapp"
                    value={values.whatsapp}
                    onChange={handleChange}
                  />
                  {errors.whatsapp ? (
                    <div className="form-erro">{errors.whatsapp}</div>
                  ) : null}
                  <Label>facebook:</Label>
                  <S.Input
                    name="facebook"
                    value={values.facebook}
                    onChange={handleChange}
                  />
                  <Label>instagram:</Label>
                  <S.Input
                    name="instagram"
                    value={values.instagram}
                    onChange={handleChange}
                  />
                  <Label>youtube:</Label>
                  <S.Input
                    name="youtube"
                    value={values.youtube}
                    onChange={handleChange}
                  />
                </S.Inputs>
                {errorSignup ||
                JSON.stringify(errors) !== JSON.stringify({}) ? (
                  <S.ErrorsContainer>
                    {errors.email ? <p>{errors.email}</p> : null}
                    {errors.phone ? <p>{errors.phone}</p> : null}
                    <p>{errorSignup}</p>
                  </S.ErrorsContainer>
                ) : null}

                <Button
                  borderRadius={5}
                  type="submit"
                  color={secondaryColor}
                  onClick={handleSubmit}
                >
                  {isLoading ? (
                    <>
                      <span>salvando...</span>
                      <Image
                        src="/images/loading-icon.svg"
                        width={30}
                        height={30}
                      />
                    </>
                  ) : (
                    <>
                      <span>salvar alterações</span>
                    </>
                  )}
                </Button>
              </S.Form>
            </>
          )
        }}
      </Formik>
    </>
  )
}

export default ContactsForm
