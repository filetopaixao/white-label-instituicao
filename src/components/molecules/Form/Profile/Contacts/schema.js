import * as Yup from 'yup'

const Schema = Yup.object().shape({
  email: Yup.string().email('Email inválido').required('Email é obrigatório'),
  phone: Yup.string()
    .transform((value) => value.replace(/[^\d]/g, ''))
    .min(11, 'Telefone inválido')
    .required('Telefone é obrigatório')
})

export default Schema
