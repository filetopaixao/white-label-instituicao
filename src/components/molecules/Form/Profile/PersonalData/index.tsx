import { useContext, useState } from 'react'
import { ThemeContext } from '../../../../../contexts/ThemeContext'
import { useRouter } from 'next/router'
import { Formik, Field } from 'formik'
import Image from 'next/image'
import Label from 'components/atoms/Label'
import Button from 'components/atoms/Button'
import Schema from './schema'
import axios from 'axios'

//import cookie from 'cookie-cutter'

import * as S from './styles'

interface PersonDataFormProps {
  loggedUser: any
  editAvatar: any
}

const PersonDataForm: React.FC<PersonDataFormProps> = ({
  loggedUser,
  editAvatar
}) => {
  const { primaryColor, secondaryColor } = useContext(ThemeContext)
  const router = useRouter()
  const [isLoading, setIsLoading] = useState(false)
  const [socialPicture, setSocialPicture] = useState('')
  const [errorSignup, setErrorSignup] = useState('')

  return (
    <>
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={{
          firstName: loggedUser ? loggedUser.first_name : '',
          lastName: loggedUser ? loggedUser.last_name : '',
          cpf: loggedUser ? loggedUser.cpf : '',
          birthDate: loggedUser ? loggedUser.birthday.split('T')[0] : '',
          gender: loggedUser ? loggedUser.gender : ''
        }}
        validationSchema={Schema}
        onSubmit={async (values) => {
          //setIsLoading(true)
          setErrorSignup('')
          console.log(values)

          if (editAvatar) {
            const formData = new FormData()
            formData.append('file', editAvatar)
            console.log('tem imagem para alterar', formData)
          }

          //const { firstName, lastName, cpf, birthDate, gender } = await values
        }}
      >
        {({ errors, handleChange, handleSubmit, values, setFieldValue }) => {
          return (
            <>
              <S.Form isLoadingLogin={isLoading}>
                <S.Inputs primaryColor={primaryColor}>
                  <div className="row-inputs">
                    <div className="column1">
                      <Label>Nome:</Label>
                      <S.Input
                        name="firstName"
                        value={values.firstName}
                        onChange={handleChange}
                      />
                      {errors.firstName ? (
                        <div className="form-erro">{errors.firstName}</div>
                      ) : null}
                    </div>
                    <div className="column2">
                      <Label>Sobrenome:</Label>
                      <S.Input
                        name="lastName"
                        value={values.lastName}
                        onChange={handleChange}
                      />
                      {errors.lastName ? (
                        <div className="form-erro">{errors.lastName}</div>
                      ) : null}
                    </div>
                  </div>

                  <div className="row-inputs">
                    <div className="column1">
                      <Label>Gênero:</Label>
                      <Field as="select" name="gender">
                        <option value="" label="Selecione" />
                        <option value="male">Masculino</option>
                        <option value="female">Feminino</option>
                      </Field>
                      {errors.gender ? (
                        <div className="form-erro">{errors.gender}</div>
                      ) : null}
                    </div>
                    <div className="column2">
                      <Label>Data de Nascimento:</Label>
                      <S.Input
                        name="birthDate"
                        type="date"
                        value={values.birthDate}
                        onChange={handleChange}
                      />
                      {errors.birthDate ? (
                        <div className="form-erro">{errors.birthDate}</div>
                      ) : null}
                    </div>
                  </div>

                  <Label>CPF</Label>
                  <S.IptMask
                    mask="999.999.999-99"
                    value={values.cpf}
                    name="cpf"
                    onChange={handleChange}
                    disabled
                  />
                  {errors.cpf ? (
                    <div className="form-erro">{errors.cpf}</div>
                  ) : null}
                </S.Inputs>
                {errorSignup ||
                JSON.stringify(errors) !== JSON.stringify({}) ? (
                  <S.ErrorsContainer>
                    {errors.firstName ? <p>{errors.firstName}</p> : null}
                    {errors.lastName ? <p>{errors.lastName}</p> : null}
                    {errors.birthDate ? <p>{errors.birthDate}</p> : null}
                    {errors.gender ? <p>{errors.gender}</p> : null}
                    {errors.cpf ? <p>{errors.cpf}</p> : null}
                    <p>{errorSignup}</p>
                  </S.ErrorsContainer>
                ) : null}

                <Button
                  borderRadius={5}
                  type="submit"
                  color={secondaryColor}
                  onClick={handleSubmit}
                >
                  {isLoading ? (
                    <>
                      <span>salvando...</span>
                      <Image
                        src="/images/loading-icon.svg"
                        width={30}
                        height={30}
                      />
                    </>
                  ) : (
                    <>
                      <span>salvar alterações</span>
                    </>
                  )}
                </Button>
              </S.Form>
            </>
          )
        }}
      </Formik>
    </>
  )
}

export default PersonDataForm
