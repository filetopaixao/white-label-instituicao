import * as Yup from 'yup'

const Schema = Yup.object().shape({
  firstName: Yup.string().required('Nome é obrigatório'),
  lastName: Yup.string().required('Sobrenome é obrigatório'),
  cpf: Yup.string()
    .transform((value) => value.replace(/[^\d]/g, ''))
    .min(11, 'CPF inválido')
    .required('Campo obrigatório'),
  birthDate: Yup.string().required('Data de nascimento é obrigatório'),
  gender: Yup.string().required('Gênero é obrigatório')
})

export default Schema
