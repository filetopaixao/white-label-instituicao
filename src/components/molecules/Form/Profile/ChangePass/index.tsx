import { useContext, useState } from 'react'
import { ThemeContext } from '../../../../../contexts/ThemeContext'
import { useRouter } from 'next/router'
import { Formik, Field } from 'formik'
import Image from 'next/image'
import Label from 'components/atoms/Label'
import Input from 'components/atoms/Input'
import Button from 'components/atoms/Button'
import Schema from './schema'
import InputMask from 'react-input-mask'
import axios from 'axios'

//import cookie from 'cookie-cutter'

import * as S from './styles'

const ChangePassForm: React.FC = () => {
  const { primaryColor, secondaryColor } = useContext(ThemeContext)
  const router = useRouter()
  const [isLoading, setIsLoading] = useState(false)
  const [errorSignup, setErrorSignup] = useState('')

  return (
    <>
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={{
          currentPass: '',
          newPass: '',
          renewPass: ''
        }}
        validationSchema={Schema}
        onSubmit={async (values) => {
          setIsLoading(true)
          setErrorSignup('')
          console.log(values)

          const { currentPass, newPass, renewPass } = await values
        }}
      >
        {({ errors, handleChange, handleSubmit, values, setFieldValue }) => {
          return (
            <>
              <S.Form isLoadingLogin={isLoading}>
                <S.Inputs primaryColor={primaryColor}>
                  <Label>senha atual</Label>
                  <Input
                    name="currentPass"
                    type="password"
                    value={values.currentPass}
                    onChange={handleChange}
                  />
                  {errors.currentPass ? (
                    <div className="form-erro">{errors.currentPass}</div>
                  ) : null}
                  <Label>senha</Label>
                  <Input
                    name="newPass"
                    type="password"
                    value={values.newPass}
                    onChange={handleChange}
                  />
                  {errors.newPass ? (
                    <div className="form-erro">{errors.newPass}</div>
                  ) : null}

                  <Label>confirmação de senha:</Label>
                  <Input
                    name="renewPass"
                    type="password"
                    value={values.renewPass}
                    onChange={handleChange}
                  />
                  {errors.renewPass ? (
                    <div className="form-erro">{errors.renewPass}</div>
                  ) : null}
                </S.Inputs>
                {errorSignup ||
                JSON.stringify(errors) !== JSON.stringify({}) ? (
                  <S.ErrorsContainer>
                    {errors.currentPass ? <p>{errors.currentPass}</p> : null}
                    {errors.newPass ? <p>{errors.newPass}</p> : null}
                    {errors.renewPass ? <p>{errors.renewPass}</p> : null}
                    <p>{errorSignup}</p>
                  </S.ErrorsContainer>
                ) : null}

                <Button
                  borderRadius={5}
                  type="submit"
                  color={secondaryColor}
                  onClick={handleSubmit}
                >
                  {isLoading ? (
                    <>
                      <span>cadastrando...</span>
                      <Image
                        src="/images/loading-icon.svg"
                        width={30}
                        height={30}
                      />
                    </>
                  ) : (
                    <>
                      <span>cadastrar-se</span>
                    </>
                  )}
                </Button>
              </S.Form>
            </>
          )
        }}
      </Formik>
    </>
  )
}

export default ChangePassForm
