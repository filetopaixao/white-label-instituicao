import * as Yup from 'yup'

const Schema = Yup.object().shape({
  currentPass: Yup.string()
    .min(6, 'A senha deve ter pelo menos 6 caracteres.')
    .required('Senha atual é obrigatório'),
  newPass: Yup.string()
    .min(6, 'A senha deve ter pelo menos 6 caracteres.')
    .required('Nova senha é obrigatório'),
  renewPass: Yup.string().oneOf(
    [Yup.ref('newPass'), null],
    'As senhas não são idênticas'
  )
})

export default Schema
