import styled from 'styled-components'
import Ipt from 'components/atoms/Input'
import InputMask from 'react-input-mask'

interface StyledLoginProps {
  primaryColor?: string
  isLoadingLogin?: boolean
  disabled?: false
}

export const Form = styled.div<StyledLoginProps>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 100%;

  button {
    opacity: ${(props) => (props.isLoadingLogin ? '0.5' : '1')};
  }
`
export const Input = styled(Ipt)``

export const IptMask = styled(InputMask)<StyledLoginProps>`
  background: ${(props) => props.disabled && '#E1E1E1'};
`

export const Inputs = styled.div<StyledLoginProps>`
  width: 100%;
  padding-bottom: 20px;

  .person-type {
    display: flex;
    align-items: center;

    padding: 15px;

    background: #f2f2f2 0% 0% no-repeat padding-box;
    border: 1px solid #bdbdbd;
    border-radius: 5px;

    div {
      width: 50%;
      input {
        width: 20px;
      }
    }
  }

    span {
      color: #5e5e5e;
    }

    

    a {
      color: ${(props) => props.primaryColor};

      font-weight: bold;
      text-decoration: none;
    }
  }
  .row-inputs {
    display: flex;

    width: 100%;

    .column1 {
      width: 50%;
      padding-right: 10px;
      div label{
        width: 100%;
      }
      @media (max-width: 768px) {
        width: 100%;
        padding-right: 0;
      }
    }
    .column2 {
      width: 50%;
      padding-left: 10px;
      div label{
        width: 100%;
      }
      @media (max-width: 768px) {
        width: 100%;
        padding-left: 0;
      }
    }
    @media (max-width: 768px) {
      flex-direction: column;
    }
  }
  .row-city-uf {
    display: flex;

    width: 100%;

    .column-city {
      width: 75%;
      padding-right: 10px;
      @media (max-width: 768px) {
        width: 70%;
      }
    }
    .column-uf {
      width: 25%;
      padding-left: 10px;
      @media (max-width: 768px) {
        width: 30%;
      }
    }
  }
`

export const BtnFacebook = styled.button<StyledLoginProps>`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 100%;
  padding: 7px 15px;
  margin-bottom: 15px;

  border-radius: 5px;
  border: none;
  outline: none;

  color: #fff;
  background: #2971af 0% 0% no-repeat padding-box;

  font-family: 'Montserrat', sans-serif;
  text-transform: uppercase;
  cursor: pointer;

  span {
    margin-left: 23px;
  }
`

export const BtnGoogle = styled.button<StyledLoginProps>`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 100%;
  padding: 7px 15px;

  border-radius: 5px;
  border: none;
  outline: none;

  color: #fff;
  background: #f4366a 0% 0% no-repeat padding-box;

  font-family: 'Montserrat', sans-serif;
  text-transform: uppercase;
  cursor: pointer;

  span {
    margin-left: 23px;
  }
`
export const ErrorsContainer = styled.div`
  background-color: rgb(237, 67, 55);
  width: 100%;
  padding: 15px 20px;
  margin-bottom: 20px;
  border-radius: 10px;

  color: #fff;

  p {
    padding-bottom: 5px;
  }
`
