import { useContext, useState } from 'react'
import { ThemeContext } from '../../../../../contexts/ThemeContext'
import { useRouter } from 'next/router'
import { Formik, Field } from 'formik'
import Image from 'next/image'
import Label from 'components/atoms/Label'
import Button from 'components/atoms/Button'
import Schema from './schema'
import UF from '../../../../../utils/UF'
import countries from '../../../../../utils/countries'
import axios from 'axios'

//import cookie from 'cookie-cutter'

import * as S from './styles'

interface AddressForm {
  loggedUser: any
}

const AddressForm: React.FC<AddressForm> = ({ loggedUser }) => {
  const { primaryColor, secondaryColor } = useContext(ThemeContext)
  const router = useRouter()
  const [isLoading, setIsLoading] = useState(false)
  const [socialPicture, setSocialPicture] = useState('')
  const [errorSignup, setErrorSignup] = useState('')

  return (
    <>
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={{
          endereco: loggedUser ? loggedUser.endereco : '',
          cep: loggedUser ? loggedUser.cep : '',
          city: loggedUser ? loggedUser.city : '',
          uf: loggedUser ? loggedUser.uf : '',
          bairro: loggedUser ? loggedUser.bairro : '',
          country: loggedUser ? loggedUser.country : ''
        }}
        validationSchema={Schema}
        onSubmit={async (values) => {
          //setIsLoading(true)
          setErrorSignup('')
          console.log(values)

          //const { firstName, lastName, cpf, birthDate, gender } = await values
        }}
      >
        {({ errors, handleChange, handleSubmit, values, setFieldValue }) => {
          return (
            <>
              <S.Form isLoadingLogin={isLoading}>
                <S.Inputs primaryColor={primaryColor}>
                  <Label>endereço:</Label>
                  <S.Input
                    name="endereco"
                    value={values.endereco}
                    onChange={handleChange}
                  />
                  <div className="row-inputs">
                    <div className="column1">
                      <Label>cep:</Label>
                      <S.IptMask
                        mask="99999-999"
                        value={values.cep}
                        name="cep"
                        onChange={handleChange}
                      />
                    </div>
                    <div className="column2">
                      <div className="row-city-uf">
                        <div className="column-city">
                          <Label>Cidade:</Label>
                          <S.Input
                            name="city"
                            value={values.city}
                            onChange={handleChange}
                          />
                          {errors.city ? (
                            <div className="form-erro">{errors.city}</div>
                          ) : null}
                        </div>
                        <div className="column-uf">
                          <Label>UF:</Label>
                          <Field as="select" name="uf">
                            <option value="" disabled label="UF" />
                            {UF.map((uf) => (
                              <option value={uf.sigla}>{uf.sigla}</option>
                            ))}
                          </Field>
                          {errors.uf ? (
                            <div className="form-erro">{errors.uf}</div>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="row-inputs">
                    <div className="column1">
                      <Label>bairro:</Label>
                      <S.Input
                        name="bairro"
                        value={values.bairro}
                        onChange={handleChange}
                      />
                      {errors.bairro ? (
                        <div className="form-erro">{errors.bairro}</div>
                      ) : null}
                    </div>
                    <div className="column2">
                      <Label>país:</Label>
                      <Field as="select" name="country">
                        <option value="" disabled label="País" />
                        {countries.map((country) => (
                          <option value={country.nome}>{country.nome}</option>
                        ))}
                      </Field>
                      {errors.country ? (
                        <div className="form-erro">{errors.country}</div>
                      ) : null}
                    </div>
                  </div>
                </S.Inputs>
                {errorSignup ||
                JSON.stringify(errors) !== JSON.stringify({}) ? (
                  <S.ErrorsContainer>
                    {errors.city ? <p>{errors.city}</p> : null}
                    {errors.uf ? <p>{errors.uf}</p> : null}
                    {errors.country ? <p>{errors.country}</p> : null}
                    <p>{errorSignup}</p>
                  </S.ErrorsContainer>
                ) : null}

                <Button
                  borderRadius={5}
                  type="submit"
                  color={secondaryColor}
                  onClick={handleSubmit}
                >
                  {isLoading ? (
                    <>
                      <span>salvando...</span>
                      <Image
                        src="/images/loading-icon.svg"
                        width={30}
                        height={30}
                      />
                    </>
                  ) : (
                    <>
                      <span>salvar alterações</span>
                    </>
                  )}
                </Button>
              </S.Form>
            </>
          )
        }}
      </Formik>
    </>
  )
}

export default AddressForm
