import * as Yup from 'yup'

const Schema = Yup.object().shape({
  city: Yup.string().required('Cidade é obrigatório'),
  uf: Yup.string().required('UF é obrigatório'),
  country: Yup.string().required('País é obrigatório')
})

export default Schema
