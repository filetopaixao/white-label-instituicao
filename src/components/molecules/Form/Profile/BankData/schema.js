import * as Yup from 'yup'

const Schema = Yup.object().shape({
  titular: Yup.string().required('Nome do titular é obrigatório'),
  cpf: Yup.string()
    .transform((value) => value.replace(/[^\d]/g, ''))
    .min(11, 'CPF inválido')
    .required('Campo obrigatório'),
  bank: Yup.string().required('Banco é obrigatório'),
  tipoConta: Yup.string().required('Tipo da conta é obrigatório'),
  agencia: Yup.string()
    .min(4, 'A agência deve conter 4 números')
    .required('Agência é obrigatório'),
  agenciaDigito: Yup.string().required('Dígito da agência é obrigatório'),
  conta: Yup.string()
    .min(7, 'A conta corrente deve conter 7 números')
    .max(12, 'A conta corrente deve conter até 12 números')
    .required('Conta é obrigatório'),
  contaDigito: Yup.string().required('Dígito da conta é obrigatório')
})

export default Schema
