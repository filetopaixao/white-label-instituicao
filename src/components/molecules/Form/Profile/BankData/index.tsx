import { useContext, useState } from 'react'
import { ThemeContext } from '../../../../../contexts/ThemeContext'
import { useRouter } from 'next/router'
import { Formik, Field } from 'formik'
import Image from 'next/image'
import Label from 'components/atoms/Label'
import Button from 'components/atoms/Button'
import Schema from './schema'
import banks from '../../../../../utils/banks'
import BankAccountValidator from 'br-bank-account-validator'

//import cookie from 'cookie-cutter'

import * as S from './styles'

interface BankDataFormProps {
  loggedUser: any
}

const BankDataForm: React.FC<BankDataFormProps> = ({ loggedUser }) => {
  const { primaryColor, secondaryColor } = useContext(ThemeContext)
  const [isLoading, setIsLoading] = useState(false)
  const [errorSignup, setErrorSignup] = useState('')

  return (
    <>
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={{
          titular: loggedUser.first_name + ' ' + loggedUser.last_name,
          cpf: loggedUser.cpf,
          bank: '',
          tipoConta: '',
          agencia: '',
          agenciaDigito: '',
          conta: '',
          contaDigito: ''
        }}
        validationSchema={Schema}
        onSubmit={async (values) => {
          //setIsLoading(true)
          setErrorSignup('')
          console.log(values)

          try {
            BankAccountValidator.validate({
              bankNumber: values.bank,
              agencyNumber: values.agencia,
              agencyCheckNumber: values.agenciaDigito,
              accountNumber: values.conta,
              accountCheckNumber: values.contaDigito
            })
            // Se chegou até aqui, a conta bancária é válida
            console.log('Válido')
          } catch (e) {
            // se não for válida, lança uma exceção
            console.log(e.message)
          }
        }}
      >
        {({ errors, handleChange, handleSubmit, values, setFieldValue }) => {
          return (
            <>
              <S.Form isLoadingLogin={isLoading}>
                <S.Inputs primaryColor={primaryColor}>
                  <Label>titular da conta:</Label>
                  <S.Input
                    name="titular"
                    value={values.titular}
                    onChange={handleChange}
                  />
                  {errors.titular ? (
                    <div className="form-erro">{errors.titular}</div>
                  ) : null}

                  <Label>CPF:</Label>
                  <S.IptMask
                    mask="999.999.999-99"
                    value={values.cpf}
                    name="cpf"
                    onChange={handleChange}
                    disabled
                  />
                  {errors.cpf ? (
                    <div className="form-erro">{errors.cpf}</div>
                  ) : null}

                  <div className="row-inputs">
                    <div className="column1">
                      <Label>Banco:</Label>
                      <Field as="select" name="bank">
                        <option value="" disabled label="Selecione" />
                        {banks.map((bank) => (
                          <option value={bank.value}>{bank.nome}</option>
                        ))}
                      </Field>
                      {errors.bank ? (
                        <div className="form-erro">{errors.bank}</div>
                      ) : null}
                    </div>
                    <div className="column2">
                      <Label>tipo de conta:</Label>
                      <Field as="select" name="tipoConta">
                        <option value="" disabled label="Selecione" />
                        <option value="corrente" label="Conta Corrente" />
                        <option value="poupanca" label="Conta Poupança" />
                      </Field>
                      {errors.tipoConta ? (
                        <div className="form-erro">{errors.tipoConta}</div>
                      ) : null}
                    </div>
                  </div>

                  <div className="row-inputs">
                    <div className="column1">
                      <div className="row-city-uf">
                        <div className="column-city">
                          <Label>Agência:</Label>
                          <S.IptMask
                            mask="9999"
                            value={values.agencia}
                            name="agencia"
                            onChange={handleChange}
                          />
                          {errors.agencia ? (
                            <div className="form-erro">{errors.agencia}</div>
                          ) : null}
                        </div>
                        <div className="column-uf">
                          <Label>Digito:</Label>
                          <S.IptMask
                            mask="9"
                            value={values.agenciaDigito}
                            name="agenciaDigito"
                            onChange={handleChange}
                          />
                          {errors.agenciaDigito ? (
                            <div className="form-erro">
                              {errors.agenciaDigito}
                            </div>
                          ) : null}
                        </div>
                      </div>
                    </div>
                    <div className="column2">
                      <div className="row-city-uf">
                        <div className="column-city">
                          <Label>conta:</Label>
                          <S.Input
                            value={values.conta}
                            name="conta"
                            onChange={handleChange}
                          />
                          {errors.conta ? (
                            <div className="form-erro">{errors.conta}</div>
                          ) : null}
                        </div>
                        <div className="column-uf">
                          <Label>Digito:</Label>
                          <S.IptMask
                            mask="9"
                            value={values.contaDigito}
                            name="contaDigito"
                            onChange={handleChange}
                          />
                          {errors.contaDigito ? (
                            <div className="form-erro">
                              {errors.contaDigito}
                            </div>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </div>
                </S.Inputs>
                {errorSignup ||
                JSON.stringify(errors) !== JSON.stringify({}) ? (
                  <S.ErrorsContainer>
                    {errors.titular ? <p>{errors.titular}</p> : null}
                    {errors.cpf ? <p>{errors.cpf}</p> : null}
                    {errors.bank ? <p>{errors.bank}</p> : null}
                    {errors.tipoConta ? <p>{errors.tipoConta}</p> : null}
                    {errors.agencia ? <p>{errors.agencia}</p> : null}
                    {errors.agenciaDigito ? (
                      <p>{errors.agenciaDigito}</p>
                    ) : null}
                    {errors.conta ? <p>{errors.conta}</p> : null}
                    {errors.contaDigito ? <p>{errors.contaDigito}</p> : null}
                    <p>{errorSignup}</p>
                  </S.ErrorsContainer>
                ) : null}

                <Button
                  borderRadius={5}
                  type="submit"
                  color={secondaryColor}
                  onClick={handleSubmit}
                >
                  {isLoading ? (
                    <>
                      <span>salvando...</span>
                      <Image
                        src="/images/loading-icon.svg"
                        width={30}
                        height={30}
                      />
                    </>
                  ) : (
                    <>
                      <span>salvar alterações</span>
                    </>
                  )}
                </Button>
              </S.Form>
            </>
          )
        }}
      </Formik>
    </>
  )
}

export default BankDataForm
