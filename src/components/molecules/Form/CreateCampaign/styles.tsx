import styled from 'styled-components'
import Ipt from 'components/atoms/Input'
import InputMask from 'react-input-mask'
import EmptyImage from '../../../../../public/images/empty-image.svg'

interface StyledLoginProps {
  primaryColor?: string
  isLoadingLogin?: boolean
  campaginImage?: string
  valueInput?: string
}

export const Form = styled.div<StyledLoginProps>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 100%;

  button {
    opacity: ${(props) => (props.isLoadingLogin ? '0.5' : '1')};
  }
`
export const Input = styled(Ipt)``

export const IptMask = styled(InputMask) <StyledLoginProps>`
  background: ${(props) => props.disabled && '#E1E1E1'};
`

export const Inputs = styled.div<StyledLoginProps>`
  width: 100%;
  padding-bottom: 20px;

  .person-type {
    display: flex;
    align-items: center;

    padding: 15px;

    background: #f2f2f2 0% 0% no-repeat padding-box;
    border: 1px solid #bdbdbd;
    border-radius: 5px;

    div {
      width: 50%;
      input {
        width: 20px;
      }
    }
  }

    span {
      color: #5e5e5e;
    }

    

    a {
      color: ${(props) => props.primaryColor};

      font-weight: bold;
      text-decoration: none;
    }
  }
  .row-inputs {
    display: flex;

    width: 100%;
    margin-top: 8px;

    .column1 {
      width: 50%;
      padding-right: 10px;
      > label {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;

        width: 100%;
        height: 50px;

        border-radius: 5px;

        background: ${(props) => props.primaryColor};

        color: #fff;

        cursor: pointer;

        .p1{
          font-size: 14px;
          font-weight: 600;
          text-transform: uppercase;
        }
        .p2{
          font-size: 12px;
        }
      }

      #image-campaign-upload {
        display: none;
      }
      button{
        width: 100%;
      }
    }
    .column2 {
      width: 50%;
      padding-left: 10px;

      > label {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;

        width: 100%;
        height: 50px;

        border-radius: 5px;

        background: ${(props) => props.primaryColor};

        color: #fff;

        cursor: pointer;

        .p1{
          font-size: 14px;
          font-weight: 600;
          text-transform: uppercase;
        }
        .p2{
          font-size: 12px;
        }
      }
      #image-campaign-upload-2 {
        display: none;
      }
    }
  }

  textarea{
    height: 160px;
    width: -webkit-fill-available;

    border: 2px solid #e1e1e1;
    color: #5e5e5e;
    font-size: 14px;
    font-family: sans-serif;
    outline: none;
    background: #ffffff;
    border-radius: 4px;
    position: relative;
    padding: 15px;
    resize: none;
    bottom: 0px;
    ::placeholder {
      color: #acacac;
    }
  }

  .checkbox {
    display: flex;

    padding-top: 25px;

    input {
      width: 15px;

      margin-right: 10px;
      margin-top: 2px;
    }

    span {
      color: #5e5e5e;
    }

    div {
      width: 15px;
      margin-right: 13px;
    }
  }
`

export const BtnFacebook = styled.button<StyledLoginProps>`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 100%;
  padding: 7px 15px;
  margin-bottom: 15px;

  border-radius: 5px;
  border: none;
  outline: none;

  color: #fff;
  background: #2971af 0% 0% no-repeat padding-box;

  font-family: 'Montserrat', sans-serif;
  text-transform: uppercase;
  cursor: pointer;

  span {
    margin-left: 23px;
  }
`

export const BtnGoogle = styled.button<StyledLoginProps>`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 100%;
  padding: 7px 15px;

  border-radius: 5px;
  border: none;
  outline: none;

  color: #fff;
  background: #f4366a 0% 0% no-repeat padding-box;

  font-family: 'Montserrat', sans-serif;
  text-transform: uppercase;
  cursor: pointer;

  span {
    margin-left: 23px;
  }
`
export const ErrorsContainer = styled.div`
  background-color: rgb(237, 67, 55);
  width: 100%;
  padding: 15px 20px;
  margin-bottom: 20px;
  border-radius: 10px;

  color: #fff;

  p {
    padding-bottom: 5px;
  }
`
export const CampaignImage = styled.div<StyledLoginProps>`
  width: 100%;
  height: 320px;
  background-image: ${(props) =>
    props.campaginImage
      ? 'linear-gradient(transparent, transparent)'
      : 'linear-gradient(#acacaccc, #acacaccc)'},
    ${(props) =>
    `url(${props.campaginImage ? props.campaginImage : EmptyImage})`};
  background-size: ${(props) => (props.campaginImage ? 'cover' : null)};
  background-repeat: no-repeat;
  background-position: center;
`

export const ValueDonation = styled.div<StyledLoginProps>`
  display: flex;
  align-items: center;

  height: 60px;

  padding: 15px;

  border: ${(props) =>
    props.valueInput
      ? `2px solid ${props.primaryColor};`
      : `2px solid #E1E1E1;`};
  border-radius: 5px;

  color: #5e5e5e;

  input {
    width: 100%;
    padding: 0 20px;

    border: none;
    outline: none;

    color: ${(props) => props.primaryColor};

    font-size: ${(props) => (props.valueInput ? '36px' : '16px')};

    ::placeholder {
      display: flex;
      align-items: center;

      color: #acacac;

      font-size: 16px;
    }
  }
`

export const Row = styled.div`
  display: flex;

  width: 100%;
  margin-top: 8px;

  .column1 {
    display: flex;
    justify-content: center;
    width: 50%;
    padding-right: 10px;
    img {
      cursor: pointer;
    }
  }
  .column2 {
    display: flex;
    justify-content: center;
    width: 50%;
    padding-left: 10px;
    img {
      cursor: pointer;
    }
  }
`
