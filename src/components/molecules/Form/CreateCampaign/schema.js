import * as Yup from 'yup'

const Schema = Yup.object().shape({
  campaignName: Yup.string().required('Nome da campanha é obrigatório'),
  campaignMeta: Yup.string().required('Meta da campanha é obrigatório'),
  campaignClosingDate: Yup.date()
    .min(new Date(), 'A data não pode ser no passado')
    .required('Data de encerramento da campanha é obrigatório'),
  campaignImage: Yup.string().required('Imagem da campanha é obrigatório'),
  campaignDesc: Yup.string().required('Descrição da campanha é obrigatório'),
  terms: Yup.bool().oneOf([true], 'É preciso aceitar os Termos')
})

export default Schema
