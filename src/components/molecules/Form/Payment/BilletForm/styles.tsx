import styled from 'styled-components'

interface CreditCardFormProps {
  selected?: boolean
  primaryColor?: string
}

export const Container = styled.div`
  padding: 10px 10%;

  .container-buttons {
    padding-bottom: 30px;
    .row {
      display: flex;

      .column-1 {
        width: 50%;

        padding-right: 6px;
      }
      .column-2 {
        width: 50%;

        padding-left: 6px;
      }
    }
  }
`
export const Button = styled.button<CreditCardFormProps>`
  img {
    padding-right: 5px !important;
  }

  display: flex;
  justify-content: center;
  align-items: center;

  width: 100%;
  height: 50px;
  padding: 10px;

  border: none;
  outline: none;

  background: ${(props) =>
    props.selected
      ? `${props.primaryColor} 0% 0% no-repeat padding-box`
      : '#ffffff 0% 0% no-repeat padding-box'};
  box-shadow: 0px 1px 6px #00000040;
  color: ${(props) => (props.selected ? '#ffffff' : '#5E5E5E')};

  font-size: 10px;
  text-transform: uppercase;

  cursor: pointer;
`

export const Header = styled.div`
  display: flex;

  width: 100%;

  img {
    cursor: pointer;
  }

  h2 {
    width: 100%;
    margin-left: -15px;

    text-align: center;
  }
`

export const Form = styled.div`
  .input-group {
    padding-bottom: 16px;

    .row {
      display: flex;
      .column-1 {
        width: 50%;
        padding-right: 6px;
      }
      .column-2 {
        width: 50%;
        padding-left: 6px;
      }
    }
  }
`

const Default = () => false
export default Default
