import { useContext } from 'react'
import { ThemeContext } from '../../../../../contexts/ThemeContext'
import Image from 'next/image'
import PaymentMethod from 'components/molecules/Form/Payment/PaymentMethod'
import CreditCardForm from '../CreditCardForm'
import { Formik } from 'formik'
import Schema from './schema'
import Input from 'components/atoms/Input'
import Button from 'components/atoms/Button'

import * as S from './styles'

interface UserFormProps {
  setForm: any
  setHeader?: any
  values: any
}

const BilletForm: React.FC<UserFormProps> = ({
  setForm,
  setHeader,
  values
}) => {
  const { primaryColor, secondaryColor } = useContext(ThemeContext)

  const header = (
    <S.Header>
      <Image
        src="/images/arrow-left.svg"
        alt="back"
        width={20}
        height={20}
        onClick={() =>
          setForm(
            <PaymentMethod
              setForm={setForm}
              setHeader={setHeader}
              values={values}
            />
          )
        }
      />
      <h2>
        Doar{' '}
        {parseFloat(values.valueDonation).toLocaleString('pt-br', {
          style: 'currency',
          currency: 'BRL'
        })}
      </h2>
    </S.Header>
  )
  setHeader(header)
  return (
    <S.Container>
      <div className="container-buttons">
        <div className="row">
          <div className="column-1">
            <S.Button
              onClick={() =>
                setForm(
                  <CreditCardForm
                    setForm={setForm}
                    setHeader={setHeader}
                    values={values}
                  />
                )
              }
            >
              <Image
                src="/images/credit-card.png"
                alt="back"
                width={24}
                height={24}
              />
              <span>cartão crédito</span>
            </S.Button>
          </div>
          <div className="column-2">
            <S.Button
              selected
              primaryColor={primaryColor}
              onClick={() =>
                setForm(
                  <CreditCardForm
                    setForm={setForm}
                    setHeader={setHeader}
                    values={values}
                  />
                )
              }
            >
              <Image
                src="/images/billet.png"
                alt="back"
                width={24}
                height={24}
              />
              <span>boleto</span>
            </S.Button>
          </div>
        </div>
      </div>
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={{
          cardNumber: '',
          expirationDate: '',
          cvv: '',
          cardName: '',
          cpf: ''
        }}
        validationSchema={Schema}
        onSubmit={(valuesInputs) => {
          // same shape as initial values
          console.log(valuesInputs, values)
          // setForm(
          //   <PaymentMethod
          //     setForm={setForm}
          //     setHeader={setHeader}
          //     values={values}
          //   />
          // )
        }}
      >
        {({ errors, handleChange, handleSubmit, values, setFieldValue }) => {
          return (
            <S.Form>
              <div className="input-group">
                <Input
                  placeholder="CPF do titular do cartão"
                  name="cpf"
                  value={values.cpf}
                  onChange={handleChange}
                />
                {errors.cpf ? (
                  <div className="form-erro">{errors.cpf}</div>
                ) : null}
              </div>
              <div className="input-group">
                <div className="row">
                  <div className="column-1">
                    <Input
                      placeholder="MM/AAAA"
                      name="expirationDate"
                      value={values.expirationDate}
                      onChange={handleChange}
                    />
                    {errors.expirationDate ? (
                      <div className="form-erro">{errors.expirationDate}</div>
                    ) : null}
                  </div>
                  <div className="column-2">
                    <Input
                      placeholder="CVV"
                      name="cvv"
                      value={values.cvv}
                      onChange={handleChange}
                    />
                    {errors.cvv ? (
                      <div className="form-erro">{errors.cvv}</div>
                    ) : null}
                  </div>
                </div>
              </div>

              <Button
                onClick={handleSubmit}
                type="button"
                color={secondaryColor}
                borderRadius={5}
              >
                doar
              </Button>
            </S.Form>
          )
        }}
      </Formik>
    </S.Container>
  )
}

export default BilletForm
