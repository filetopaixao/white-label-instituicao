import { useContext, useState } from 'react'
import { useRouter } from 'next/router'
import Image from 'next/image'
import { ThemeContext } from '../../../../../contexts/ThemeContext'
import { Formik } from 'formik'
import Input from 'components/atoms/Input'
import InputMask from 'react-input-mask'
import Button from 'components/atoms/Button'
import Checkbox from 'components/atoms/Checkbox'
import TextArea from 'components/atoms/TextArea'
import cookie from 'js-cookie'
import Schema from './schema'
import PaymentMethod from 'components/molecules/Form/Payment/PaymentMethod'

import * as S from './styles'

interface UserFormProps {
  setForm?: any
  setHeader?: any
}

const UserForm: React.FC<UserFormProps> = ({ setForm, setHeader }) => {
  const { primaryColor, secondaryColor } = useContext(ThemeContext)
  const router = useRouter()
  const loggedUser = cookie.get('loggedUser')
    ? JSON.parse(`${cookie.get('loggedUser')}`)
    : {}

  const [btn1, setBtn1] = useState(false)
  const [btn2, setBtn2] = useState(false)
  const [btn3, setBtn3] = useState(false)
  const [btn4, setBtn4] = useState(false)
  const [btn5, setBtn5] = useState(false)
  const [btn6, setBtn6] = useState(false)

  const [valueInput, setValueInput] = useState('')

  const [nameInput, setNameInput] = useState('')
  const [lastnameInput, setLastnameInput] = useState('')
  const [emailInput, setEmailInput] = useState('')

  const [isAnonymous, setIsAnonymous] = useState(false)
  const [message, setMessage] = useState('')

  const header = (
    <>
      <Image src="/images/lock.svg" alt="cadeado" width={20} height={20} />
      <h2>Doação segura</h2>
    </>
  )
  setHeader(header)

  return (
    <S.Container primaryColor={primaryColor} valueInput={valueInput}>
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={{
          valueDonation: '',
          name: loggedUser ? loggedUser.first_name : '',
          lastname: loggedUser ? loggedUser.last_name : '',
          email: loggedUser ? loggedUser.email : '',
          isAnonymous: false
        }}
        validationSchema={Schema}
        onSubmit={(values) => {
          // same shape as initial values
          console.log(loggedUser)
          if (loggedUser != {}) {
            console.log(values)
            setForm(
              <PaymentMethod
                setForm={setForm}
                setHeader={setHeader}
                values={values}
              />
            )
          } else {
            router.push('sign-in')
          }
        }}
      >
        {({ errors, handleChange, handleSubmit, values, setFieldValue }) => {
          const handleButton = (btn: number) => {
            switch (btn) {
              case 1:
                if (btn1) {
                  setBtn1(false)
                  setFieldValue('valueDonation', '')
                } else {
                  setBtn1(true)
                  setFieldValue('valueDonation', '25')
                }
                setBtn2(false)
                setBtn3(false)
                setBtn4(false)
                setBtn5(false)
                setBtn6(false)
                break
              case 2:
                if (btn2) {
                  setBtn2(false)
                  setFieldValue('valueDonation', '')
                } else {
                  setBtn2(true)
                  setFieldValue('valueDonation', '50')
                }
                setBtn1(false)
                setBtn3(false)
                setBtn4(false)
                setBtn5(false)
                setBtn6(false)
                break
              case 3:
                if (btn3) {
                  setBtn3(false)
                  setFieldValue('valueDonation', '')
                } else {
                  setBtn3(true)
                  setFieldValue('valueDonation', '75')
                }
                setBtn1(false)
                setBtn2(false)
                setBtn4(false)
                setBtn5(false)
                setBtn6(false)
                break
              case 4:
                if (btn4) {
                  setBtn4(false)
                  setFieldValue('valueDonation', '')
                } else {
                  setBtn4(true)
                  setFieldValue('valueDonation', '120')
                }
                setBtn1(false)
                setBtn2(false)
                setBtn3(false)
                setBtn5(false)
                setBtn6(false)
                break
              case 5:
                if (btn5) {
                  setBtn5(false)
                  setFieldValue('valueDonation', '')
                } else {
                  setBtn5(true)
                  setFieldValue('valueDonation', '250')
                }
                setBtn1(false)
                setBtn2(false)
                setBtn3(false)
                setBtn4(false)
                setBtn6(false)
                break
              case 6:
                if (btn6) {
                  setBtn6(false)
                  setFieldValue('valueDonation', '')
                } else {
                  setBtn6(true)
                  setFieldValue('valueDonation', '500')
                }
                setBtn1(false)
                setBtn2(false)
                setBtn3(false)
                setBtn4(false)
                setBtn5(false)
                break

              default:
                setBtn1(false)
                setBtn2(false)
                setBtn3(false)
                setBtn4(false)
                setBtn5(false)
                setBtn6(false)
            }
          }
          return (
            <div className="donation-form">
              <div className="buttons">
                <div className="button">
                  <S.Button
                    onClick={() => handleButton(6)}
                    selected={btn6}
                    primaryColor={primaryColor}
                    type="button"
                  >
                    R$ 500
                  </S.Button>
                </div>
                <div className="button">
                  <S.Button
                    onClick={() => handleButton(5)}
                    selected={btn5}
                    primaryColor={primaryColor}
                    type="button"
                  >
                    R$ 250
                  </S.Button>
                </div>
                <div className="button">
                  <S.Button
                    onClick={() => handleButton(4)}
                    selected={btn4}
                    primaryColor={primaryColor}
                    type="button"
                  >
                    R$ 120
                  </S.Button>
                </div>
                <div className="button">
                  <S.Button
                    onClick={() => handleButton(3)}
                    selected={btn3}
                    primaryColor={primaryColor}
                    type="button"
                  >
                    R$ 75
                  </S.Button>
                </div>
                <div className="button">
                  <S.Button
                    onClick={() => handleButton(2)}
                    selected={btn2}
                    primaryColor={primaryColor}
                    type="button"
                  >
                    R$ 50
                  </S.Button>
                </div>
                <div className="button">
                  <S.Button
                    onClick={() => handleButton(1)}
                    selected={btn1}
                    primaryColor={primaryColor}
                    type="button"
                  >
                    R$ 25
                  </S.Button>
                </div>
              </div>
              <div className="form-inputs">
                <div className="value-donation">
                  <S.ValueDonation
                    primaryColor={primaryColor}
                    valueInput={values.valueDonation}
                  >
                    R$
                    <input
                      id="value-donation-form"
                      type="number"
                      placeholder="Outro"
                      value={values.valueDonation}
                      name="valueDonation"
                      min={20}
                      onChange={(e) =>
                        setFieldValue('valueDonation', e.target.value)
                      }
                    />
                    BRL
                  </S.ValueDonation>
                  {errors.valueDonation ? (
                    <div className="form-erro">{errors.valueDonation}</div>
                  ) : null}
                </div>
                <div className="inputs">
                  <Input
                    placeholder="Nome"
                    name="name"
                    value={values.name}
                    onChange={handleChange}
                    disabled={loggedUser ? true : false}
                  />
                  {errors.name ? (
                    <div className="form-erro">{errors.name}</div>
                  ) : null}
                  <Input
                    placeholder="Sobrenome"
                    name="lastname"
                    value={values.lastname}
                    onChange={handleChange}
                    disabled={loggedUser ? true : false}
                  />
                  {errors.lastname ? (
                    <div className="form-erro">{errors.lastname}</div>
                  ) : null}
                  <Input
                    placeholder="Email"
                    name="email"
                    type="email"
                    value={values.email}
                    onChange={handleChange}
                    disabled={loggedUser ? true : false}
                  />
                  {errors.email ? (
                    <div className="form-erro">{errors.email}</div>
                  ) : null}
                  <div className="isAnnymous">
                    <Input
                      name="isAnonymous"
                      type="checkbox"
                      onChange={handleChange}
                      value={values.isAnonymous}
                    />
                    <span>Aparecer como Anômimo</span>
                  </div>
                  <div className="message">
                    <TextArea
                      value={message}
                      onChange={(e) => setMessage(e.target.value)}
                      placeholder="Deixe uma mensagem (Opcional)"
                    />
                  </div>
                  <Button
                    borderRadius={5}
                    onClick={handleSubmit}
                    type="button"
                    color={secondaryColor}
                  >
                    doar
                  </Button>
                </div>
              </div>
            </div>
          )
        }}
      </Formik>
    </S.Container>
  )
}

export default UserForm
