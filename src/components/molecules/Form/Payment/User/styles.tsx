import styled from 'styled-components'

interface StyledSidebarsProps {
  primaryColor?: string
  tertiaryColor?: string
  selected?: boolean
  valueInput?: string
}

export const Container = styled.div<StyledSidebarsProps>`
  .donation-form {
    padding: 10px 10%;

    .buttons {
      display: grid;
      grid-template-columns: 1fr 1fr 1fr;

      .button {
        padding: 5px;
      }
    }

    .form-inputs {
      .value-donation {
        margin-bottom: 28px;
      }

      .inputs {
        input {
          margin-bottom: 16px;

          border: 1px solid #e1e1e1;
        }

        button {
          margin-bottom: 10px;
        }

        .isAnnymous {
          display: flex;
          align-items: center;

          padding-bottom: 40px !important;
          input {
            width: 15px;
            margin-right: 10px;
            margin-bottom: 0px;
          }
          span {
            color: #5e5e5e;

            font-size: 16px;
          }
        }

        .message {
          padding-bottom: 20px;
        }
      }
    }
  }
  @media (max-width: 768px) {
    display: none;
  }
`

export const ValueDonation = styled.div<StyledSidebarsProps>`
  display: flex;
  align-items: center;

  height: 60px;

  padding: 15px;
  margin-top: 28px;

  border: ${(props) =>
    props.valueInput
      ? `2px solid ${props.primaryColor};`
      : `2px solid #E1E1E1;`};
  border-radius: 5px;

  color: #5e5e5e;

  input {
    width: 100%;
    padding: 0 20px;

    border: none;
    outline: none;

    color: ${(props) => props.primaryColor};

    font-size: ${(props) => (props.valueInput ? '36px' : '16px')};

    ::placeholder {
      display: flex;
      align-items: center;

      color: #acacac;

      font-size: 16px;
    }
  }
`

export const Button = styled.button<StyledSidebarsProps>`
  width: 100%;
  padding: 15px 10px;

  border: none;
  border-radius: 5px;
  outline: none;

  background-color: ${(props) =>
    props.selected ? props.primaryColor : '#fff'};
  color: ${(props) => (props.selected ? '#fff' : '#5e5e5e')};

  font-size: 14px;

  box-shadow: 0px 1px 6px #00000040;
  cursor: pointer;
`

const Default = () => false
export default Default
