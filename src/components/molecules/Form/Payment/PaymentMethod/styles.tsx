import styled from 'styled-components'

export const Container = styled.div`
  padding: 90px 10%;

  h2 {
    padding-bottom: 30px;

    color: #5e5e5e;

    font-size: 18px;
    font-weight: bold;
    text-align: center;
  }

  .row {
    display: flex;

    .column-1 {
      width: 50%;

      padding-right: 6px;
      button {
        width: 100%;
        height: 100px;

        border: none;
        outline: none;

        background: #ffffff 0% 0% no-repeat padding-box;
        box-shadow: 0px 1px 6px #00000040;

        cursor: pointer;
      }
    }
    .column-2 {
      width: 50%;

      padding-left: 6px;
      button {
        width: 100%;
        height: 100px;

        border: none;
        outline: none;

        background: #ffffff 0% 0% no-repeat padding-box;
        box-shadow: 0px 1px 6px #00000040;

        cursor: pointer;
      }
    }
  }
`
export const Header = styled.div`
  display: flex;

  width: 100%;

  img {
    cursor: pointer;
  }

  h2 {
    width: 100%;
    margin-left: -15px;

    text-align: center;
  }
`

const Default = () => false
export default Default
