import Image from 'next/image'
import UserForm from 'components/molecules/Form/Payment/User'
import CreditCardForm from 'components/molecules/Form/Payment/CreditCardForm'
import BilletForm from 'components/molecules/Form/Payment/BilletForm'
import * as S from './styles'

interface UserFormProps {
  setForm: any
  setHeader?: any
  values?: any
}

const PaymentMethod: React.FC<UserFormProps> = ({
  setForm,
  setHeader,
  values
}) => {
  const header = (
    <S.Header>
      <Image
        src="/images/arrow-left.svg"
        alt="back"
        width={20}
        height={20}
        onClick={() =>
          setForm(<UserForm setForm={setForm} setHeader={setHeader} />)
        }
      />
      <h2>
        Doar{' '}
        {parseFloat(values.valueDonation).toLocaleString('pt-br', {
          style: 'currency',
          currency: 'BRL'
        })}
      </h2>
    </S.Header>
  )
  setHeader(header)
  return (
    <S.Container>
      <h2>Escolha a forma de pagamento:</h2>
      <div className="row">
        <div className="column-1">
          <button
            onClick={() =>
              setForm(
                <CreditCardForm
                  setForm={setForm}
                  setHeader={setHeader}
                  values={values}
                />
              )
            }
          >
            <Image
              src="/images/credit-card.png"
              alt="back"
              width={70}
              height={70}
            />
          </button>
        </div>
        <div className="column-2">
          <button
            onClick={() =>
              setForm(
                <BilletForm
                  setForm={setForm}
                  setHeader={setHeader}
                  values={values}
                />
              )
            }
          >
            <Image src="/images/billet.png" alt="back" width={70} height={70} />
          </button>
        </div>
      </div>
    </S.Container>
  )
}

export default PaymentMethod
