import * as Yup from 'yup'

const Schema = Yup.object().shape({
  cardNumber: Yup.string().required('Campo obrigatório'),
  expirationDate: Yup.string().required('Campo obrigatório'),
  cvv: Yup.string().required('Campo obrigatório'),
  cardName: Yup.string().required('Campo obrigatório'),
  cpf: Yup.string().required('Campo obrigatório')
})

export default Schema
