import styled from 'styled-components'

interface StyledLoginProps {
  primaryColor?: string
  isLoadingLogin?: boolean
}

export const Form = styled.div<StyledLoginProps>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 412px;

  button {
    opacity: ${(props) => (props.isLoadingLogin ? '0.5' : '1')};
  }
`
export const Inputs = styled.div<StyledLoginProps>`
  width: 100%;
  padding-bottom: 20px;

  .person-type {
    display: flex;
    align-items: center;

    padding: 15px;

    background: #f2f2f2 0% 0% no-repeat padding-box;
    border: 1px solid #bdbdbd;
    border-radius: 5px;

    div {
      width: 50%;
      input {
        width: 20px;
      }
    }
  }

  .checkbox {
    display: flex;
    align-items: center;

    padding-top: 50px;

    input {
      width: 15px;

      margin-right: 10px;
    }

    span {
      color: #5e5e5e;
    }

    div {
      width: 15px;
      margin-right: 13px;
    }

    a {
      color: ${(props) => props.primaryColor};

      font-weight: bold;
      text-decoration: none;
    }
  }
  .row-inputs {
    display: flex;

    width: 100%;

    .column1 {
      width: 75%;
      padding-right: 10px;
    }
    .column2 {
      width: 25%;
      padding-left: 10px;
    }
  }
`

export const BtnFacebook = styled.button<StyledLoginProps>`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 100%;
  padding: 7px 15px;
  margin-bottom: 15px;

  border-radius: 5px;
  border: none;
  outline: none;

  color: #fff;
  background: #2971af 0% 0% no-repeat padding-box;

  font-family: 'Montserrat', sans-serif;
  text-transform: uppercase;
  cursor: pointer;

  span {
    margin-left: 23px;
  }
`

export const BtnGoogle = styled.button<StyledLoginProps>`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 100%;
  padding: 7px 15px;

  border-radius: 5px;
  border: none;
  outline: none;

  color: #fff;
  background: #f4366a 0% 0% no-repeat padding-box;

  font-family: 'Montserrat', sans-serif;
  text-transform: uppercase;
  cursor: pointer;

  span {
    margin-left: 23px;
  }
`
export const ErrorsContainer = styled.div`
  background-color: rgb(237, 67, 55);
  width: 100%;
  padding: 15px 20px;
  margin-bottom: 20px;
  border-radius: 10px;

  color: #fff;

  p {
    padding-bottom: 5px;
  }
`
