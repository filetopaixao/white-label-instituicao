import { useContext, useState, useEffect } from 'react'
import { ThemeContext } from '../../../../contexts/ThemeContext'
import { PagesContext } from '../../../../contexts/PagesContext'
import { useRouter } from 'next/router'
import { Formik, Field } from 'formik'
import Image from 'next/image'
import Label from 'components/atoms/Label'
import Input from 'components/atoms/Input'
import Button from 'components/atoms/Button'
import Schema from './schema'
import InputMask from 'react-input-mask'
import FacebookLogin from 'react-facebook-login-typed'
import { GoogleLogin } from 'react-google-login'
import UF from '../../../../utils/UF'
import countries from '../../../../utils/countries'
import axios from 'axios'
import { signUp } from '../../../../services/auth'

//import cookie from 'cookie-cutter'

import * as S from './styles'

const SignupForm: React.FC = () => {
  const { primaryColor, secondaryColor } = useContext(ThemeContext)
  const router = useRouter()
  const [isLoading, setIsLoading] = useState(false)
  const [socialPicture, setSocialPicture] = useState('')
  const [errorSignup, setErrorSignup] = useState('')

  const onlyNaturallPerson = true

  const { nameQuery, lastnameQuery, emailQuery } = useContext(PagesContext)
  console.log(nameQuery, lastnameQuery, emailQuery)

  const handleImage = async (url: string) => {
    console.log(url)
    const toDataURL = () =>
      fetch(url)
        .then((response) => response.blob())
        .then(
          (blob) =>
            new Promise((resolve, reject) => {
              const reader = new FileReader()
              reader.onloadend = () => resolve(reader.result)
              reader.onerror = reject
              reader.readAsDataURL(blob)
            })
        )

    function dataURLtoFile(dataurl: any, filename: any) {
      let arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n)
      while (n--) {
        u8arr[n] = bstr.charCodeAt(n)
      }
      return new File([u8arr], filename, { type: mime })
    }

    const image_id = await toDataURL().then(async (dataUrl) => {
      console.log('Here is Base64 Url', dataUrl)
      const fileData = dataURLtoFile(dataUrl, 'profilejpg')
      //console.log('Here is JavaScript File Object', fileData)

      const data = new FormData()
      data.append('image', fileData)
      const image_id = await axios({
        method: 'post',
        url: 'http://127.0.0.1:4000/uploader/single-image-upload',
        data
      })
      console.log('image_id', image_id.data.id)
      return image_id.data.id
    })
    console.log('return', image_id)
    return image_id
  }

  return (
    <>
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={{
          legal_person: 'natural_person',
          firstName: nameQuery ? nameQuery : '',
          lastName: lastnameQuery ? lastnameQuery : '',
          cpf: '',
          email: emailQuery ? emailQuery : '',
          phone: '',
          birthDate: '',
          gender: '',
          city: '',
          uf: '',
          country: 'Brasil',
          password: '',
          passwordConfirmation: '',
          terms: false,
          isNewEmail: true
        }}
        validationSchema={Schema}
        onSubmit={async (values) => {
          setIsLoading(true)
          setErrorSignup('')
          console.log(values)

          let image_id = await ''
          if (socialPicture) {
            image_id = await handleImage(socialPicture)
          } else {
            const photo = await fetch(
              `https://ui-avatars.com/api/?background=F46036&color=fff&name=${values.firstName.toLowerCase()}&length=1`
            )
            image_id = await handleImage(photo.url)
          }

          const {
            email,
            legal_person,
            password,
            passwordConfirmation,
            firstName,
            lastName,
            cpf,
            birthDate,
            phone,
            gender,
            city,
            uf
          } = await values

          await signUp({
            email,
            legal_person,
            password,
            password_confirmation: passwordConfirmation,
            image_id,
            natural_person_data: {
              first_name: firstName,
              last_name: lastName,
              cpf,
              birthday: birthDate,
              phone,
              gender
            },
            address_data: {
              city,
              state: uf,
              country: 'BR'
            }
          }).then((redirect) => {
            //router.push(redirect)
            if (redirect !== '/perfil') {
              if (redirect !== undefined) {
                setErrorSignup(redirect)
              }
            } else {
              router.push(redirect)
            }
            setIsLoading(false)
          })
        }}
      >
        {({ errors, handleChange, handleSubmit, values, setFieldValue }) => {
          const responseFacebook = (response: any) => {
            //console.log(response)
            setSocialPicture(response.picture.data.url)
            setFieldValue(
              'firstName',
              response.name.substr(0, response.name.indexOf(' '))
            )
            setFieldValue(
              'lastName',
              response.name.substr(response.name.indexOf(' ') + 1)
            )
            setFieldValue('email', response.email)
          }

          const responseGoogle = (response: any) => {
            console.log(response.profileObj.imageUrl)
            setSocialPicture(response.profileObj.imageUrl)
            setFieldValue('firstName', response.profileObj.givenName)
            setFieldValue('lastName', response.profileObj.familyName)
            setFieldValue('email', response.profileObj.email)
          }

          return (
            <>
              <S.Form isLoadingLogin={isLoading}>
                <FacebookLogin
                  appId="481128059564985"
                  autoLoad={false}
                  fields="name,email,picture.type(large)"
                  size="medium"
                  //onClick={componentClicked}
                  callback={responseFacebook}
                  render={(renderProps) => (
                    <S.BtnFacebook onClick={renderProps.onClick}>
                      <Image
                        src="/images/facebook.svg"
                        width={35}
                        height={35}
                        className="facebook-icon-white"
                      />
                      <span>continuar com o facebook</span>
                    </S.BtnFacebook>
                  )}
                />
                <GoogleLogin
                  clientId="812077401980-3h8q0mbdf7jd1me56qob191slt50qc84.apps.googleusercontent.com"
                  render={(renderProps) => (
                    <S.BtnGoogle
                      onClick={renderProps.onClick}
                      disabled={renderProps.disabled}
                    >
                      <Image
                        src="/images/google.svg"
                        width={35}
                        height={35}
                        className="facebook-icon-white"
                      />
                      <span>continuar com o google</span>
                    </S.BtnGoogle>
                  )}
                  buttonText="Login"
                  onSuccess={responseGoogle}
                  onFailure={responseGoogle}
                  cookiePolicy={'single_host_origin'}
                />
                <S.Inputs primaryColor={primaryColor}>
                  {onlyNaturallPerson ? null : (
                    <>
                      <Label>pessoa:</Label>
                      <div className="person-type">
                        <div>
                          <Field
                            type="radio"
                            name="legal_person"
                            id="fisica"
                            value="natural_person"
                          />
                          <label htmlFor="fisica">Física</label>
                        </div>
                        <div>
                          <Field
                            type="radio"
                            name="legal_person"
                            id="juridica"
                            value="juridical_person"
                          />
                          <label htmlFor="juridica">Jurídica</label>
                        </div>
                      </div>
                      {errors.legal_person ? (
                        <div className="form-erro">{errors.legal_person}</div>
                      ) : null}
                    </>
                  )}
                  <Label>Nome:</Label>
                  <Input
                    name="firstName"
                    value={values.firstName}
                    onChange={handleChange}
                  />
                  {errors.firstName ? (
                    <div className="form-erro">{errors.firstName}</div>
                  ) : null}

                  <Label>Sobrenome:</Label>
                  <Input
                    name="lastName"
                    value={values.lastName}
                    onChange={handleChange}
                  />
                  {errors.lastName ? (
                    <div className="form-erro">{errors.lastName}</div>
                  ) : null}

                  <Label>CPF:</Label>
                  <InputMask
                    mask="999.999.999-99"
                    value={values.cpf}
                    name="cpf"
                    onChange={handleChange}
                  />
                  {errors.cpf ? (
                    <div className="form-erro">{errors.cpf}</div>
                  ) : null}

                  <Label htmlFor="emailInput">e-mail</Label>
                  <Input
                    name="email"
                    id="emailInput"
                    type="email"
                    value={values.email}
                    onChange={handleChange}
                  />
                  {errors.email ? (
                    <div className="form-erro">{errors.email}</div>
                  ) : null}

                  <Label>telefone (WhatsApp):</Label>
                  <InputMask
                    mask="(99) 9999-99999"
                    name="phone"
                    value={values.phone}
                    onChange={handleChange}
                  />
                  {errors.phone ? (
                    <div className="form-erro">{errors.phone}</div>
                  ) : null}

                  <Label>data de nascimento:</Label>
                  <Input
                    name="birthDate"
                    type="date"
                    value={values.birthDate}
                    onChange={handleChange}
                  />
                  {errors.birthDate ? (
                    <div className="form-erro">{errors.birthDate}</div>
                  ) : null}

                  <Label>Gênero:</Label>
                  <Field as="select" name="gender">
                    <option value="" label="Selecione" />
                    <option value="male">Masculino</option>
                    <option value="female">Feminino</option>
                  </Field>
                  {errors.gender ? (
                    <div className="form-erro">{errors.gender}</div>
                  ) : null}

                  <div className="row-inputs">
                    <div className="column1">
                      <Label>Cidade:</Label>
                      <Input
                        name="city"
                        value={values.city}
                        onChange={handleChange}
                      />
                      {errors.city ? (
                        <div className="form-erro">{errors.city}</div>
                      ) : null}
                    </div>
                    <div className="column2">
                      <Label>UF:</Label>
                      <Field as="select" name="uf">
                        <option value="" disabled label="UF" />
                        {UF.map((uf) => (
                          <option value={uf.sigla}>{uf.sigla}</option>
                        ))}
                      </Field>
                      {errors.uf ? (
                        <div className="form-erro">{errors.uf}</div>
                      ) : null}
                    </div>
                  </div>

                  <Label>país:</Label>
                  <Field as="select" name="country">
                    <option value="" disabled label="País" />
                    {countries.map((country) => (
                      <option value={country.nome}>{country.nome}</option>
                    ))}
                  </Field>
                  {errors.country ? (
                    <div className="form-erro">{errors.country}</div>
                  ) : null}

                  <Label>senha</Label>
                  <Input
                    name="password"
                    type="password"
                    value={values.password}
                    onChange={handleChange}
                  />
                  {errors.password ? (
                    <div className="form-erro">{errors.password}</div>
                  ) : null}

                  <Label>confirmação de senha:</Label>
                  <Input
                    name="passwordConfirmation"
                    type="password"
                    value={values.passwordConfirmation}
                    onChange={handleChange}
                  />
                  {errors.passwordConfirmation ? (
                    <div className="form-erro">
                      {errors.passwordConfirmation}
                    </div>
                  ) : null}

                  <div className="checkbox">
                    <Input
                      name="terms"
                      type="checkbox"
                      onChange={handleChange}
                      value={values.terms}
                    />{' '}
                    <span>
                      Li e concordo com as <a href="#">Condições</a> e{' '}
                      <a href="#">Termos de Uso</a>
                    </span>
                  </div>
                  {errors.terms ? (
                    <div className="form-erro">{errors.terms}</div>
                  ) : null}
                </S.Inputs>
                {errorSignup ||
                  JSON.stringify(errors) !== JSON.stringify({}) ? (
                  <S.ErrorsContainer>
                    {errors.legal_person ? <p>{errors.legal_person}</p> : null}
                    {errors.firstName ? <p>{errors.firstName}</p> : null}
                    {errors.lastName ? <p>{errors.lastName}</p> : null}
                    {errors.cpf ? <p>{errors.cpf}</p> : null}
                    {errors.email ? <p>{errors.email}</p> : null}
                    {errors.phone ? <p>{errors.phone}</p> : null}
                    {errors.birthDate ? <p>{errors.birthDate}</p> : null}
                    {errors.gender ? <p>{errors.gender}</p> : null}
                    {errors.city ? <p>{errors.city}</p> : null}f
                    {errors.uf ? <p>{errors.uf}</p> : null}
                    {errors.country ? <p>{errors.country}</p> : null}
                    {errors.password ? <p>{errors.password}</p> : null}
                    {errors.passwordConfirmation ? (
                      <p>{errors.passwordConfirmation}</p>
                    ) : null}
                    {errors.terms ? <p>{errors.terms}</p> : null}
                    <p>{errorSignup}</p>
                  </S.ErrorsContainer>
                ) : null}

                <Button
                  borderRadius={5}
                  type="submit"
                  color={secondaryColor}
                  onClick={handleSubmit}
                >
                  {isLoading ? (
                    <>
                      <span>cadastrando...</span>
                      <Image
                        src="/images/loading-icon.svg"
                        width={30}
                        height={30}
                      />
                    </>
                  ) : (
                    <>
                      <span>cadastrar-se</span>
                    </>
                  )}
                </Button>
              </S.Form>
            </>
          )
        }}
      </Formik>
    </>
  )
}

export default SignupForm
