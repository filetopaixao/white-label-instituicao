import * as Yup from 'yup'

const Schema = Yup.object().shape({
  //legal_person: Yup.string().required('Tipo de pessoa é obrigatório'),
  firstName: Yup.string().required('Nome é obrigatório'),
  lastName: Yup.string().required('Sobrenome é obrigatório'),
  cpf: Yup.string()
    .transform((value) => value.replace(/[^\d]/g, ''))
    .min(11, 'CPF inválido')
    .required('Campo obrigatório'),
  email: Yup.string().email('Email inválido').required('Email é obrigatório'),
  phone: Yup.string()
    .transform((value) => value.replace(/[^\d]/g, ''))
    .min(11, 'Telefone inválido')
    .required('Telefone é obrigatório'),
  birthDate: Yup.string().required('Data de nascimento é obrigatório'),
  gender: Yup.string().required('Gênero é obrigatório'),
  city: Yup.string().required('Cidade é obrigatório'),
  uf: Yup.string().required('UF é obrigatório'),
  country: Yup.string().required('País é obrigatório'),
  password: Yup.string()
    .min(6, 'A senha deve ter pelo menos 6 caracteres.')
    .required('Senha é obrigatório'),
  passwordConfirmation: Yup.string().oneOf(
    [Yup.ref('password'), null],
    'As senhas não são idênticas'
  ),
  terms: Yup.bool().oneOf([true], 'É preciso aceitar os Termos')
})

export default Schema
