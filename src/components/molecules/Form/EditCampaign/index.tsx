import { useContext, useState } from 'react'
import { ThemeContext } from '../../../../contexts/ThemeContext'
import { useRouter } from 'next/router'
import { Formik, Field } from 'formik'
import Image from 'next/image'
import Label from 'components/atoms/Label'
import Button from 'components/atoms/Button'
import Modal from 'components/atoms/Modal'
import Schema from './schema'
import image1 from '../../../../../public/images/campaigns-images/1.png'
import image2 from '../../../../../public/images/campaigns-images/1.png'
import image3 from '../../../../../public/images/campaigns-images/1.png'
import image4 from '../../../../../public/images/campaigns-images/1.png'
import image5 from '../../../../../public/images/campaigns-images/1.png'
import image6 from '../../../../../public/images/campaigns-images/1.png'
import image7 from '../../../../../public/images/campaigns-images/1.png'
import image8 from '../../../../../public/images/campaigns-images/1.png'
import image9 from '../../../../../public/images/campaigns-images/1.png'
import image10 from '../../../../../public/images/campaigns-images/1.png'
import IntlCurrencyInput from 'react-intl-currency-input'
//const IntlCurrencyInput = require('react-intl-currency-input')

//import cookie from 'cookie-cutter'

import * as S from './styles'

const EditCampaignForm: React.FC = ({ campaignData }) => {
  const { primaryColor, secondaryColor } = useContext(ThemeContext)
  const router = useRouter()
  const [isLoading, setIsLoading] = useState(false)
  const [socialPicture, setSocialPicture] = useState('')
  const [errorSignup, setErrorSignup] = useState('')
  const [campaginImage, setCampaignImage] = useState('')
  const [isVisible, setIsVisible] = useState(false)

  const currencyConfig = {
    locale: 'pt-BR',
    formats: {
      number: {
        BRL: {
          style: 'currency',
          currency: 'BRL',
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }
      }
    }
  }

  const handleAvatar = (e: any) => {
    //setEditAvatar(e.target.files[0])
    const preview = URL.createObjectURL(e.target.files[0])
    setCampaignImage(preview)
    //console.log(preview)
  }

  return (
    <>
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={{
          campaignName: campaignData.name,
          campaignMeta: campaignData.goal_value,
          campaignClosingDate: campaignData.ending_date.split('T')[0],
          campaignImage: campaignData.image.url,
          campaignVideo: '',
          campaignDesc: campaignData.description,
          anonymous: campaignData.is_anonymous,
          terms: false
        }}
        validationSchema={Schema}
        onSubmit={async (values) => {
          //setIsLoading(true)
          setErrorSignup('')
          console.log(values)
        }}
      >
        {({ errors, handleChange, handleSubmit, values, setFieldValue }) => {
          return (
            <>
              <S.Form isLoadingLogin={isLoading}>
                <S.Inputs primaryColor={primaryColor}>
                  <Label>Nome:</Label>
                  <S.Input
                    name="campaignName"
                    value={values.campaignName}
                    onChange={handleChange}
                  />
                  {errors.campaignName ? (
                    <div className="form-erro">{errors.campaignName}</div>
                  ) : null}

                  <Label>Meta:</Label>
                  <IntlCurrencyInput
                    currency="BRL"
                    name="campaignMeta"
                    config={currencyConfig}
                    onChange={(e) =>
                      setFieldValue('campaignMeta', e.target.value)
                    }
                    value={values.campaignMeta}
                  />

                  {errors.campaignMeta ? (
                    <div className="form-erro">{errors.campaignMeta}</div>
                  ) : null}

                  <Label>Data de encerramento:</Label>
                  <S.Input
                    name="campaignClosingDate"
                    type="date"
                    value={values.campaignClosingDate}
                    onChange={handleChange}
                  />
                  {errors.campaignClosingDate ? (
                    <div className="form-erro">
                      {errors.campaignClosingDate}
                    </div>
                  ) : null}

                  <Label>Imagem:</Label>
                  <S.CampaignImage campaginImage={campaginImage ? campaginImage : values.campaignImage} />

                  <div className="row-inputs">
                    <div className="column1">
                      <label htmlFor="image-campaign-upload">
                        <p className="p1">carregar</p>
                        <p className="p2">uma foto sua</p>
                      </label>
                      <input
                        type="file"
                        id="image-campaign-upload"
                        accept="image/png, image/jpeg"
                        onChange={(e: any) => {
                          handleAvatar(e)
                          setFieldValue('campaignImage', e.target.files[0])
                        }}
                      />
                    </div>
                    <div className="column2">
                      <label htmlFor="image-campaign-upload-2">
                        <p className="p1">escolher</p>
                        <p className="p2">uma de nossas fotos</p>
                      </label>
                      <input
                        type="button"
                        id="image-campaign-upload-2"
                        onClick={() => setIsVisible(!isVisible)}
                      />
                    </div>
                  </div>

                  <Label>Link video:</Label>
                  <S.Input
                    name="campaignVideo"
                    value={values.campaignVideo}
                    onChange={handleChange}
                  />
                  {errors.campaignVideo ? (
                    <div className="form-erro">{errors.campaignVideo}</div>
                  ) : null}

                  <Label>Descrição:</Label>
                  <Field
                    as="textarea"
                    id="campaignDesc"
                    onChange={handleChange}
                    value={values.campaignDesc}
                  />
                  {errors.campaignDesc ? (
                    <div className="form-erro">{errors.campaignDesc}</div>
                  ) : null}

                  <div className="checkbox">
                    <S.Input
                      name="anonymous"
                      type="checkbox"
                      onChange={handleChange}
                      value={values.anonymous}
                    />{' '}
                    <span>Aparecer como Anômimo</span>
                  </div>

                  <div className="checkbox">
                    <S.Input
                      name="terms"
                      type="checkbox"
                      onChange={handleChange}
                      value={values.terms}
                    />{' '}
                    <span>
                      Me responsabilizo pela imagem <br /> e descrição utilizada
                      nesta campanha. <br />
                      <a href="#">Termos de Responsabilidade</a>
                    </span>
                  </div>
                  {errors.terms ? (
                    <div className="form-erro">{errors.terms}</div>
                  ) : null}
                </S.Inputs>
                {errorSignup ||
                  JSON.stringify(errors) !== JSON.stringify({}) ? (
                  <S.ErrorsContainer>
                    {errors.campaignName ? <p>{errors.campaignName}</p> : null}
                    {errors.campaignMeta ? <p>{errors.campaignMeta}</p> : null}
                    {errors.campaignClosingDate ? (
                      <p>{errors.campaignClosingDate}</p>
                    ) : null}
                    {errors.campaignImage ? (
                      <p>{errors.campaignImage}</p>
                    ) : null}
                    {errors.campaignVideo ? (
                      <p>{errors.campaignVideo}</p>
                    ) : null}
                    {errors.terms ? <p>{errors.terms}</p> : null}
                    {errors.campaignDesc ? <p>{errors.campaignDesc}</p> : null}
                    <p>{errorSignup}</p>
                  </S.ErrorsContainer>
                ) : null}

                <Button
                  borderRadius={5}
                  type="submit"
                  color={secondaryColor}
                  onClick={handleSubmit}
                >
                  {isLoading ? (
                    <>
                      <span>salvando...</span>
                      <Image
                        src="/images/loading-icon.svg"
                        width={30}
                        height={30}
                      />
                    </>
                  ) : (
                    <>
                      <span>concluir campanha</span>
                    </>
                  )}
                </Button>
                <Modal
                  id="modal-campaign-image"
                  isVisible={isVisible}
                  variant="sm"
                  onClose={() => setIsVisible(!isVisible)}
                  title="Escolha sua imagem"
                  btnCloser
                >
                  <S.Row>
                    <div className="column1">
                      <img
                        src={image1}
                        onClick={() => {
                          setCampaignImage(image1)
                          setIsVisible(!isVisible)
                        }}
                      />
                    </div>
                    <div className="column2">
                      <img
                        src={image2}
                        onClick={() => {
                          setCampaignImage(image2)
                          setIsVisible(!isVisible)
                        }}
                      />
                    </div>
                  </S.Row>
                  <S.Row>
                    <div className="column1">
                      <img
                        src={image3}
                        onClick={() => {
                          setCampaignImage(image3)
                          setIsVisible(!isVisible)
                        }}
                      />
                    </div>
                    <div className="column2">
                      <img
                        src={image4}
                        onClick={() => {
                          setCampaignImage(image4)
                          setIsVisible(!isVisible)
                        }}
                      />
                    </div>
                  </S.Row>
                  <S.Row>
                    <div className="column1">
                      <img
                        src={image5}
                        onClick={() => {
                          setCampaignImage(image5)
                          setIsVisible(!isVisible)
                        }}
                      />
                    </div>
                    <div className="column2">
                      <img
                        src={image6}
                        onClick={() => {
                          setCampaignImage(image6)
                          setIsVisible(!isVisible)
                        }}
                      />
                    </div>
                  </S.Row>
                  <S.Row>
                    <div className="column1">
                      <img
                        src={image7}
                        onClick={() => {
                          setCampaignImage(image7)
                          setIsVisible(!isVisible)
                        }}
                      />
                    </div>
                    <div className="column2">
                      <img
                        src={image8}
                        onClick={() => {
                          setCampaignImage(image8)
                          setIsVisible(!isVisible)
                        }}
                      />
                    </div>
                  </S.Row>
                  <S.Row>
                    <div className="column1">
                      <img
                        src={image9}
                        onClick={() => {
                          setCampaignImage(image9)
                          setIsVisible(!isVisible)
                        }}
                      />
                    </div>
                    <div className="column2">
                      <img
                        src={image10}
                        onClick={() => {
                          setCampaignImage(image10)
                          setIsVisible(!isVisible)
                        }}
                      />
                    </div>
                  </S.Row>
                </Modal>
              </S.Form>
            </>
          )
        }}
      </Formik>
    </>
  )
}

export default EditCampaignForm
