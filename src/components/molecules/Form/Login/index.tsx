import { useContext, useState } from 'react'
import { ThemeContext } from '../../../../contexts/ThemeContext'
import { PagesContext } from '../../../../contexts/PagesContext'
import { useRouter } from 'next/router'
import { Formik } from 'formik'
import Image from 'next/image'
import Label from 'components/atoms/Label'
import Input from 'components/atoms/Input'
import Button from 'components/atoms/Button'
import FacebookLogin from 'react-facebook-login-typed'
import { GoogleLogin } from 'react-google-login'
import Schema from './schema'

import { authentication, socialAuthentication } from '../../../../services/auth'

import * as S from './styles'

const LoginForm: React.FC = () => {
  const { secondaryColor } = useContext(ThemeContext)
  const { redirectUrl } = useContext(PagesContext)

  const router = useRouter()
  const [isLoadingLogin, setIsLoadingLogin] = useState(false)
  const [errorSignin, setErrorSignin] = useState('')

  // const handleLoginSocial = async (email: string) => {
  //   console.log(router.query)
  //   setIsLoadingLogin(true)
  //   const response = await socialAuthentication({ email })
  //   if (redirectUrl) {
  //     router.push(redirectUrl)
  //   } else {
  //     if (response === '/campanhas/1') {
  //       router.push(response)
  //     } else {
  //       setErrorSignin(response)
  //     }
  //   }
  //   setIsLoadingLogin(false)
  // }

  return (
    <Formik
      validateOnChange={false}
      validateOnBlur={false}
      initialValues={{
        email: '',
        password: '',
        keepConnected: false
      }}
      validationSchema={Schema}
      onSubmit={async (values) => {
        setIsLoadingLogin(true)
        setErrorSignin('')
        const { email, password, keepConnected } = values
        authentication({
          email,
          password,
          keepConnected
        })
          .then((response) => {
            if (router.query.redirect) {
              router.push(router.query.redirect)
            } else {
              if (response === '/campanhas/1') {
                router.push(response)
              } else {
                setErrorSignin(response)
                setIsLoadingLogin(false)
              }
            }
          })
          .catch((e) => {
            setIsLoadingLogin(false)
          })
      }}
    >
      {({ errors, handleChange, handleSubmit, values }) => {
        const responseFacebook = (response: any) => {
          handleLoginSocial(response.email)
        }

        const responseGoogle = (response: any) => {
          handleLoginSocial(response.profileObj.email)
        }

        return (
          <S.Form isLoadingLogin={isLoadingLogin}>
            <FacebookLogin
              appId="481128059564985"
              autoLoad={false}
              fields="name,email,picture.type(large)"
              size="medium"
              //onClick={componentClicked}
              callback={responseFacebook}
              render={(renderProps) => (
                <S.BtnFacebook onClick={renderProps.onClick}>
                  <Image
                    src="/images/facebook.svg"
                    width={35}
                    height={35}
                    className="facebook-icon-white"
                  />
                  <span>continuar com o facebook</span>
                </S.BtnFacebook>
              )}
            />
            <GoogleLogin
              clientId="812077401980-3h8q0mbdf7jd1me56qob191slt50qc84.apps.googleusercontent.com"
              render={(renderProps) => (
                <S.BtnGoogle onClick={renderProps.onClick}>
                  <Image
                    src="/images/google.svg"
                    width={35}
                    height={35}
                    className="facebook-icon-white"
                  />
                  <span>continuar com o google</span>
                </S.BtnGoogle>
              )}
              buttonText="Login"
              onSuccess={responseGoogle}
              onFailure={responseGoogle}
              cookiePolicy={'single_host_origin'}
            />
            <S.Inputs>
              <Label htmlFor="email">e-mail</Label>
              <Input
                name="email"
                placeholder="Email"
                id="email"
                type="email"
                value={values.email}
                onChange={handleChange}
              />
              {errors.email ? (
                <div className="form-erro">{errors.email}</div>
              ) : null}
              <Label htmlFor="password">senha</Label>
              <Input
                name="password"
                placeholder="Senha"
                id="password"
                type="password"
                value={values.password}
                onChange={handleChange}
              />
              {errors.password ? (
                <div className="form-erro">{errors.password}</div>
              ) : null}
              <div className="checkbox">
                <Input
                  name="keepConnected"
                  type="checkbox"
                  onChange={handleChange}
                  value={values.keepConnected}
                />{' '}
                <span>Permanecer conectado</span>
              </div>
            </S.Inputs>

            {errorSignin || JSON.stringify(errors) !== JSON.stringify({}) ? (
              <S.ErrorsContainer>
                {errors.email ? <p>{errors.email}</p> : null}
                {errors.password ? <p>{errors.password}</p> : null}
                <p>{errorSignin}</p>
              </S.ErrorsContainer>
            ) : null}

            <Button
              borderRadius={5}
              type="submit"
              color={secondaryColor}
              onClick={handleSubmit}
            >
              {isLoadingLogin ? (
                <>
                  <span>logando...</span>
                  <Image
                    src="/images/loading-icon.svg"
                    width={30}
                    height={30}
                  />
                </>
              ) : (
                <>
                  <span>logar</span>
                </>
              )}
            </Button>
          </S.Form>
        )
      }}
    </Formik>
  )
}

export default LoginForm
