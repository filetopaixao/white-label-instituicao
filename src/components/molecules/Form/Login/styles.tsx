import styled from 'styled-components'

interface StyledLoginProps {
  isLoadingLogin: boolean
}

export const Form = styled.div<StyledLoginProps>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 412px;

  button {
    opacity: ${(props) => (props.isLoadingLogin ? '0.5' : '1')};
  }
`
export const Inputs = styled.div`
  width: 100%;
  padding-bottom: 20px;

  .checkbox {
    display: flex;
    align-items: center;

    padding-top: 50px;

    input {
      width: 15px;

      margin-right: 10px;
    }

    span {
      color: #5e5e5e;
    }

    div {
      width: 15px;
      margin-right: 13px;
    }
  }
`

export const BtnFacebook = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 100%;
  padding: 7px 15px;
  margin-bottom: 15px;

  border-radius: 5px;
  border: none;
  outline: none;

  color: #fff;
  background: #2971af 0% 0% no-repeat padding-box;

  font-family: 'Montserrat', sans-serif;
  text-transform: uppercase;
  cursor: pointer;

  span {
    margin-left: 23px;
  }
`

export const BtnGoogle = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 100%;
  padding: 7px 15px;

  border-radius: 5px;
  border: none;
  outline: none;

  color: #fff;
  background: #f4366a 0% 0% no-repeat padding-box;

  font-family: 'Montserrat', sans-serif;
  text-transform: uppercase;
  cursor: pointer;

  span {
    margin-left: 23px;
  }
`

export const ErrorsContainer = styled.div`
  background-color: rgb(237, 67, 55);
  width: 100%;
  padding: 15px 20px;
  margin-bottom: 20px;
  border-radius: 10px;

  color: #fff;

  p {
    padding-bottom: 5px;
  }
`
