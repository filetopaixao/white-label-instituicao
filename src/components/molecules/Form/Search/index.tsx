import { useState } from 'react'
import SearchIcon from '../../../../../public/images/search.svg'
import * as S from './styles'

const Search: React.FC = () => {
  const [inputText, setInputText] = useState('')
  return (
    <S.Container>
      <S.Input
        value={inputText}
        onChange={(e) => setInputText(e.target.value)}
        type="text"
        placeholder="Digite o ID ou título"
      />
      <S.Button type="submit" onClick={() => console.log(inputText)}>
        <img src={SearchIcon} />
      </S.Button>
    </S.Container>
  )
}
export default Search
