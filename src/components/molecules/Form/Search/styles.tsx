import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;

  height: 50px;
  width: 280px;
  margin: 0 20px;
  padding-left: 20px;

  background: #f7f7f7;
  border-radius: 30px;
  border: 1px solid #acacac;
`

export const Input = styled.input`
  width: 85%;
  color: #acacac;
  background: transparent;
  border: none;
  outline: none;

  font-size: 17px;

  ::placeholder {
    color: #acacac;
  }
`
export const Button = styled.button`
  height: 75%;
  width: 15%;
  margin-right: 16px;
  padding-left: 6px;

  background: transparent;
  border: none;
  outline: none;
  border-left: 1px solid #acacac;

  font-size: 17px;

  cursor: pointer;

  img {
    width: 20px;
    height: 20px;
  }
`
