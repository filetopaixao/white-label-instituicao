import * as S from './styles'

interface MenuProps {
    handleDropdown: () => void
    collapseTarget: string
}

const CollapseButton: React.FC<MenuProps> = ({ handleDropdown, collapseTarget }) => {
    return (
        <>
            <S.DropdownItemMobile >
                <button
                    onClick={() => handleDropdown(collapseTarget)}
                >
                    <div className="arrow-icon" />
                </button>
            </S.DropdownItemMobile>
        </>
    )
}

export default CollapseButton
