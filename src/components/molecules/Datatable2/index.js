import React from 'react';
import { render } from 'react-dom';
import { slideDown, slideUp } from './anim';

import * as S from './styles'


function formatDate(str) {
    return str.substr(0, 10);
}

function capitalize(str) {
    return str.split(' ').map(s => {
        return s.charAt(0).toUpperCase() + s.substr(1);
    }).join(' ');
}


class UserTableRow extends React.Component {
    state = { expanded: false }

    toggleExpander = (e) => {
        if (e.target.type === 'checkbox') return;

        if (!this.state.expanded) {
            this.setState(
                { expanded: true },
                () => {
                    if (this.refs.expanderBody) {
                        slideDown(this.refs.expanderBody);
                    }
                }
            );
        } else {
            slideUp(this.refs.expanderBody, {
                onComplete: () => { this.setState({ expanded: false }); }
            });
        }
    }

    render() {
        const { user } = this.props;
        return [
            <tr key="main" onClick={this.toggleExpander}>
                {this.props.columnsMobile && this.props.columnsMobile.map((header) => {
                    return (
                        <>
                            <td>{header.Cell ? header.Cell(user[header.accessor]) : user[header.accessor]}</td>
                        </>
                    )
                })}
            </tr>,
            this.state.expanded && (
                <tr className="expandable" key="tr-expander">
                    <td className="uk-background-muted" colSpan={6}>
                        <div ref="expanderBody" className="inner uk-grid">
                            <div className="uk-width-3-4">
                                {this.props.collapseMobile && this.props.collapseMobile.map(column => {
                                    return <S.CollapseContent><p>{column.Header} <span>{column.Cell ? column.Cell(user[column.accessor]) : user[column.accessor]}</span></p></S.CollapseContent>
                                })}
                            </div>
                        </div>
                    </td>
                </tr>
            )
        ];
    }
}



class Datatable2 extends React.Component {
    state = { users: null }

    componentDidMount() {
        console.log(this.props.columnsMobile)
        fetch('https://randomuser.me/api/1.1/?results=15')
            .then(response => response.json())
            .then(data => { this.setState({ users: data.results }) });
    }

    render() {
        const { users } = this.state;
        const isLoading = users === null;
        return (
            <main>
                <S.Container>
                    <div className="uk-overflow-auto">
                        <table className="uk-table uk-table-hover uk-table-middle uk-table-divider">
                            <thead>
                                <S.ThHeader>
                                    {this.props.columnsMobile && this.props.columnsMobile.map(column => <th>{column.Header}</th>)}
                                </S.ThHeader>
                            </thead>
                            <tbody>
                                {isLoading
                                    ? <tr><td colSpan={6} className="uk-text-center"><em className="uk-text-muted">Loading...</em></td></tr>
                                    : this.props.data && this.props.data.map((data, index) =>
                                        <UserTableRow key={index} index={index + 1} user={data} columnsMobile={this.props.columnsMobile} collapseMobile={this.props.collapseMobile} />
                                    )
                                }
                            </tbody>
                        </table>
                    </div>
                </S.Container>
            </main>
        );
    }
}

export default Datatable2