import styled from 'styled-components'

import Arrow from '../../../../public/images/arrow.svg'

interface StyledCardCampaignProps {
}

export const Container = styled.div`
  display: flex;

  width: 100%;
  padding: 20px;

  table{
    border-collapse: collapse;
    width: 100%;

    tr {
      width: 100%;
    }

    td, th {
      border-bottom: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }
  }

  @media (max-width: 768px) {
  }
`

export const ThHeader = styled.tr`

  color: #5E5E5E;
  
  font-weight: bold;
  text-transform: uppercase;

  @media (max-width: 768px) {
  }
`

export const CollapseContent = styled.div`
  margin-bottom: 30px;

  color: #5E5E5E;
  
  font-weight: bold;
  text-transform: uppercase;

  span{
    font-weight: normal;
  }
`