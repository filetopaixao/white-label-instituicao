import styled from 'styled-components'

export const Container = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  padding: 10px 10%;
  .title {
    padding-bottom: 10px;

    color: #5e5e5e;

    font-size: 50px;
    @media (max-width: 768px) {
      text-align: center;
    }
  }

  .subtitle {
    padding-bottom: 64px;

    color: #5e5e5e;

    font-size: 24px;
    @media (max-width: 768px) {
      text-align: center;
    }
  }

  .new-row {
    display: flex;

    width: 100%;
    @media (max-width: 768px) {
      flex-direction: column;
    }
  }
  @media (max-width: 768px) {
    padding: 0 35px;
  }
`
