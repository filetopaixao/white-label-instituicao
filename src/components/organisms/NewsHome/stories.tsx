import { Story, Meta } from '@storybook/react/types-6-0'
import NewsHome from '.'

export default {
  title: 'NewsHome',
  component: NewsHome
} as Meta

const news = {
  data: [
    {
      image: {
        id: 1,
        url:
          'https://conectabucket.s3.us-east-2.amazonaws.com/1p.ia9vt1njho-Ubutntu-on-Windows-10-logo-banner.jpg'
      },
      id: 3,
      title: 'Que notícia',
      description: 'Uma descrição de uma notícia',
      button_text: 'any_button_text',
      button_redirect_url: 'any_url.com'
    },
    {
      image: {
        id: 1,
        url:
          'https://conectabucket.s3.us-east-2.amazonaws.com/1p.ia9vt1njho-Ubutntu-on-Windows-10-logo-banner.jpg'
      },
      id: 4,
      title: 'Última notícia',
      description: 'Uma descrição de uma notícia',
      button_text: 'any_button_text',
      button_redirect_url: 'any_url.com'
    },
    {
      image: {
        id: 1,
        url:
          'https://conectabucket.s3.us-east-2.amazonaws.com/1p.ia9vt1njho-Ubutntu-on-Windows-10-logo-banner.jpg'
      },
      id: 2,
      title: 'Notícia nova',
      description: 'Uma descrição de uma notícia',
      button_text: 'any_button_text',
      button_redirect_url: 'any_url.com'
    },
    {
      image: {
        id: 1,
        url:
          'https://conectabucket.s3.us-east-2.amazonaws.com/1p.ia9vt1njho-Ubutntu-on-Windows-10-logo-banner.jpg'
      },
      id: 1,
      title: 'Outra notícia nova',
      description: 'Uma descrição de uma notícia',
      button_text: 'any_button_text',
      button_redirect_url: 'any_url.com'
    }
  ]
}

export const Basic: Story = () => <NewsHome news={news} />
