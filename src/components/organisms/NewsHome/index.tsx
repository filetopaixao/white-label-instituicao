import HeartIcon from 'components/atoms/HeartIcon'
import NewsWithButton from 'components/molecules/News/NewsWithButton'
import NewsWithoutButton from 'components/molecules/News/NewsWithoutButton'

import * as S from './styles'

interface Image {
  id?: number
  url?: string
}

interface New {
  id: number
  title: string
  description: string
  button_text: string
  button_redirect_url: string
  image: Image
}

interface Data {
  data: New[]
}

interface NewsHomeProps {
  news: Data
}

const NewsHome: React.FC<NewsHomeProps> = ({ news }) => {
  return (
    <S.Container>
      <HeartIcon />
      <h1 className="title">Novidades do Mês</h1>
      <h2 className="subtitle">Saiba o que está rolando por aqui</h2>
      <div className="new-row">
        <NewsWithButton
          image={news.data[0] ? news.data[0].image.url : ''}
          title={news.data[0] ? news.data[0].title : ''}
          description={news.data[0] ? news.data[0].description : ''}
          button_text={news.data[0] ? news.data[0].button_text : ''}
        />
        <NewsWithoutButton
          image={news.data[1] ? news.data[1].image.url : ''}
          title={news.data[1] ? news.data[1].title : ''}
          description={news.data[1] ? news.data[1].description : ''}
          button_text={news.data[1] ? news.data[1].button_text : ''}
        />
      </div>
      <div className="new-row">
        <NewsWithoutButton
          image={news.data[2] ? news.data[2].image.url : ''}
          title={news.data[2] ? news.data[2].title : ''}
          description={news.data[2] ? news.data[2].description : ''}
          button_text={news.data[2] ? news.data[2].button_text : ''}
        />
        <NewsWithButton
          image={news.data[3] ? news.data[3].image.url : ''}
          title={news.data[3] ? news.data[3].title : ''}
          description={news.data[3] ? news.data[3].description : ''}
          button_text={news.data[3] ? news.data[3].button_text : ''}
        />
      </div>
    </S.Container>
  )
}

export default NewsHome
