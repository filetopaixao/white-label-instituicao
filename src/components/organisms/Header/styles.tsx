import styled from 'styled-components'
import Menu from '../../../../public/images/menu.svg'
import Close from '../../../../public/images/close.svg'
import Arrow from '../../../../public/images/arrow.svg'

interface StyledHeaderProps {
  secondaryColor?: string
  showMenu?: boolean
  avatarProfile?: string
}

export const Container = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;

  min-height: 110px;
  padding-left: 30px;

  background: #ffffff;
  box-shadow: 0px 3px 6px #00000029;

  @media (max-width: 768px) {
    display: none;
  }
`
export const ContainerMenu = styled.div`
  display: flex;
  justify-content: center;

  padding-left: 70px;
`
export const ButtonCreateCampaign = styled.button<StyledHeaderProps>`
  display: flex;
  align-items: center;
  justify-content: center;
  flex: 1;

  max-width: 250px;
  height: 110px;
  padding: 0 40px;

  color: #ffffff;
  background: ${(props) => props.secondaryColor};
  border: 0;
  outline: 0;

  font-size: 16px;
  text-transform: uppercase;

  cursor: pointer;
`
export const ContainerMobile = styled.header`
  position: fixed;
  display: none;
  align-items: center;
  justify-content: center;

  width: 100%;
  min-height: 40px;
  padding: 15px;

  background: #ffffff;
  box-shadow: 0px 3px 6px #00000029;

  z-index: 999999;

  .menu-profile {
    position: absolute;
    right: 15px;
    display: flex;
    justify-content: flex-end;

    width: 100%;
  }

  @media (max-width: 768px) {
    display: flex;
  }
`

export const CollapseIcon = styled.button<StyledHeaderProps>`
  position: absolute;
  left: 15px;

  width: 25px;
  height: 20px;

  mask-image: ${(props) =>
    props.showMenu ? `url(${Close});` : `url(${Menu});`};
  -webkit-mask-image: ${(props) =>
    props.showMenu ? `url(${Close});` : `url(${Menu});`};
  mask-repeat: no-repeat;
  mask-size: ${(props) => (props.showMenu ? `18px;` : `25px;`)};
  background-color: #5e5e5e;

  cursor: pointer;
`

export const Profile = styled.button<StyledHeaderProps>`
  position: absolute;
  right: 15px;
  display: flex;
  align-items: center;

  background-color: transparent;
  border: none;
  outline: none;

  .avatar-profile {
    width: 40px;
    height: 40px;

    border-radius: 50%;

    background-image: ${(props) => `url(${props.avatarProfile})`};
    background-size: cover;
    background-repeat: no-repeat;
    background-position-x: center;
  }

  .arrow-icon {
    float: right;

    width: 11px;
    height: 6px;
    margin-left: 5px;

    mask-image: url(${Arrow});
    mask-size: 11px;
    -webkit-mask-image: url(${Arrow});
    mask-repeat: no-repeat;
    background-color: #5e5e5e;

    transform: rotate(180deg);
  }

  cursor: pointer;
`
