import { useState, useContext, useEffect } from 'react'
import { MenuProvider } from '../../../contexts/MenuContext'
import { ThemeContext } from '../../../contexts/ThemeContext'
import Logo from 'components/atoms/Logo'
import Menu from 'components/molecules/Menu'
import MenuProfile from 'components/molecules/MenuProfile'
import Search from 'components/molecules/Form/Search'
import NextNprogress from 'nextjs-progressbar'
import cookie from 'js-cookie'
import { useRouter } from 'next/router'

import * as S from './styles'

const Header: React.FC = () => {
  const { primaryColor, secondaryColor } = useContext(ThemeContext)
  const [showMenu, setShowMenu] = useState(false)
  const [showMenuProfile, setShowMenuProfile] = useState(false)
  const [isLogged, setIsLogged] = useState(false)
  const router = useRouter()

  const loggedUser = cookie.get('loggedUser')
    ? JSON.parse(`${cookie.get('loggedUser')}`)
    : {}

  useEffect(() => {
    if (cookie.get('loggedUser')) {
      setIsLogged(true)
    } else {
      setIsLogged(false)
    }
  }, [cookie.get('loggedUser')])

  const handleCollapseMenu = () => {
    setShowMenuProfile(false)
    setShowMenu(!showMenu)
  }

  const handleCollapseProfile = () => {
    setShowMenu(false)
    setShowMenuProfile(!showMenuProfile)
  }

  return (
    <MenuProvider>
      <NextNprogress
        color={primaryColor}
        startPosition={0.3}
        stopDelayMs={200}
        height={3}
        options={{ showSpinner: false }}
      />
      <S.Container>
        <Logo width={189} height={67} />
        <S.ContainerMenu>
          <Menu isMobile={false} showMenu={true} />
        </S.ContainerMenu>
        <Search />
        {isLogged && <MenuProfile isMobile={false} showMenu={true} />}
        <S.ButtonCreateCampaign
          secondaryColor={secondaryColor}
          onClick={() => router.push('/criar-campanha')}
        >
          criar campanha
        </S.ButtonCreateCampaign>
      </S.Container>
      <S.ContainerMobile>
        <S.CollapseIcon
          onClick={() => handleCollapseMenu()}
          showMenu={showMenu}
        />
        <Logo width={95} height={34} />
        <Menu isMobile showMenu={showMenu} setShowMenu={setShowMenu} />
        {isLogged && (
          <S.Profile
            onClick={() => handleCollapseProfile()}
            showMenu={showMenuProfile}
            avatarProfile={loggedUser.image_profile}
          >
            <div className="avatar-profile" />
            <div className="arrow-icon" />
          </S.Profile>
        )}
        <MenuProfile
          isMobile
          showMenu={showMenuProfile}
          setShowMenu={setShowMenuProfile}
        />
      </S.ContainerMobile>
    </MenuProvider>
  )
}

export default Header
