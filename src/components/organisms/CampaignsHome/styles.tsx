import styled from 'styled-components'

import ArrowDown from '../../../../public/images/arrow-down.svg'

interface StyledCampaignsHomeProps {
  primaryColor: string
}

export const Container = styled.div<StyledCampaignsHomeProps>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  padding: 10px 10%;
  @media (max-width: 768px) {
    padding: 10px 36px;
  }

  a {
    text-decoration: none;
  }

  .all-campaigns {
    display: flex;
    align-items: center;

    padding-bottom: 65px;

    color: ${(props) => props.primaryColor};

    font-size: 17px;
    font-weight: 600;

    text-decoration: none;
    text-transform: uppercase;

    div {
      width: 11px;
      height: 11px;
      margin-left: 6px;

      mask-image: url(${ArrowDown});
      mask-size: 11px;
      -webkit-mask-image: url(${ArrowDown});
      mask-repeat: no-repeat;
      background-color: ${(props) => props.primaryColor};
    }
  }
`

export const Title = styled.h1`
  padding-bottom: 10px;

  color: #5e5e5e;

  font-size: 50px;
  text-align: center;
  @media (max-width: 768px) {
    padding-bottom: 20px;

    font-size: 25pt;
  }
`

export const Subtitle = styled.h2`
  padding-bottom: 64px;

  color: #5e5e5e;

  font-size: 24px;
  text-align: center;
  @media (max-width: 768px) {
    font-size: 13pt;
  }
`

export const CampaignsContainer = styled.div`
  width: 100%;
`
