import { useContext } from 'react'
import { ThemeContext } from '../../../contexts/ThemeContext'
import HeartIcon from 'components/atoms/HeartIcon'
import CardCampaign from 'components/molecules/CardCampaign'
import Carousel from 'components/atoms/Carousel'

import * as S from './styles'

interface Image {
  id: number
  url: string
}

interface Campaign {
  id: number
  name: string
  user_id: number
  goal_value: string
  amount_collected: string
  ending_date: string
  category_id: number
  image_id: number
  description: string
  is_anonymous: boolean
  status: string
  image: Image
}

interface Data {
  data: Campaign[]
}

interface CampaignsHomeProps {
  campaigns: Data
}

const CampaignsHome: React.FC<CampaignsHomeProps> = ({ campaigns }) => {
  const { primaryColor } = useContext(ThemeContext)
  return (
    <section>
      <HeartIcon />
      <S.Container primaryColor={primaryColor}>
        <S.Title>Campanhas em destaque</S.Title>
        <S.Subtitle>
          “Gratidão a todos que nos ajudam a escrever essa história”
        </S.Subtitle>
        <S.CampaignsContainer>
          <Carousel len={campaigns.data.length}>
            {campaigns.data.map((campaign, key) => (
              <CardCampaign
                key={key}
                id={campaign.id}
                image={campaign.image.url}
                name={campaign.name}
                description={campaign.description}
                goal_value={campaign.goal_value}
                amount_collected={campaign.amount_collected}
              />
            ))}
          </Carousel>
        </S.CampaignsContainer>
        <a className="all-campaigns" href="/campanhas/1">
          ver todas <div />
        </a>
      </S.Container>
    </section>
  )
}

export default CampaignsHome
