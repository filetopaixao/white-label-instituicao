import { Story, Meta } from '@storybook/react/types-6-0'
import SectionHomeCampaigns from '.'

export default {
  title: 'SectionHomeCampaigns',
  component: SectionHomeCampaigns
} as Meta

const campaigns = {
  data: [
    {
      id: 1,
      name: 'campanha do teste',
      user_id: 3,
      goal_value: '950.00',
      amount_collected: '0.00',
      ending_date: '2021-11-13T00:00:00.000Z',
      category_id: 1,
      image_id: 1,
      description: 'uma campanha do teste',
      is_anonymous: false,
      status: 'active',
      image: {
        id: 1,
        url:
          'https://conectabucket.s3.us-east-2.amazonaws.com/1p.ia9vt1njho-Ubutntu-on-Windows-10-logo-banner.jpg'
      }
    }
  ]
}

export const Basic: Story = () => <SectionHomeCampaigns campaigns={campaigns} />
