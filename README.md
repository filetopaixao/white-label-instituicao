This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run:
yarn create next-app -e https://github.com/filetopaixao/NextJS-boilerplate.git

## CSS organization

Propriedades do layout (position, float, clear, display)
Propriedades de modelagem (width, height, margin, padding)
Propriedades visuais (color, background, border, box-shadow)
Propriedades tipográficas (font-size, font-family, text-align, text-transform)
Outras propriedades (cursor, overflow, z-index)
