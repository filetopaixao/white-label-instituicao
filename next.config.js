// eslint-disable-next-line @typescript-eslint/no-var-requires
// const withPWA = require('next-pwa')
// const isProd = process.env.NODE_ENV === 'production'

// module.exports = {
//   distDir: 'build',
// }

// module.exports = withPWA({
//   pwa: {
//     dest: 'public',
//     disable: !isProd
//   }
// })

module.exports = {
  distDir: 'nextjs',
  env: {
    FIREBASE_PROJECT_ID: 'conecta-white-label'
  },
  experimental: {
    sprFlushToDisk: false
  }
}

module.exports = {
  typescript: {
    // !! WARN !!
    // Dangerously allow production builds to successfully complete even if
    // your project has type errors.
    // !! WARN !!
    ignoreBuildErrors: true
  }
}

const withImages = require('next-images')
module.exports = withImages({
  esModule: true
})
